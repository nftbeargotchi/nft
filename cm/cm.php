<?php
/**
 * @package ContentManager
 * @author Samuele Diella <samuele.diella@gmail.com>
 * @copyright Copyright (c) 2004-2010, Samuele Diella
 * @license http://opensource.org/licenses/gpl-3.0.html
 * @link http://www.formsphpframework.com
 */

/**
 * @package ContentManager
 * @author Samuele Diella <samuele.diella@gmail.com>
 * @copyright Copyright (c) 2004-2010, Samuele Diella
 * @license http://opensource.org/licenses/gpl-3.0.html
 * @link http://www.formsphpframework.com
 */
class cm extends ffCommon
{
    public static $singleton 			= null;
    public static $env                = array();

    protected static $_events = null;
    
    const LAYOUT_PRIORITY_TOPLEVEL		= 1; // special, only one
    const LAYOUT_PRIORITY_HIGH			= 2;
    const LAYOUT_PRIORITY_NORMAL			= 3;
    const LAYOUT_PRIORITY_LOW			= 4;
    const LAYOUT_PRIORITY_FINAL			= 5; // special, only one
    const LAYOUT_PRIORITY_DEFAULT		= cm::LAYOUT_PRIORITY_NORMAL;

    public $config                 = array();
    public $content_root			= null;
    public $path_info 				= null;
    public $real_path_info			= null;
    public $query_string 			= null;
    public $script_name			= null;
    public $is_php					= null;
    public $is_resource			= null;
    public $module					= null;
    public $module_path			= null;
        
    public $process_next_rule		= false;
    public $processed_rule			= null;
    public $processed_rule_attrs = null;
    
    //var $ff_settings_loaded		= array();
    //var $ff_settings			= null;

    public $layout_vars			= null;

    public $default_mime			= "text/html";
    public $default_charset		= "UTF-8";

    /**
     *
     * @var cmRouter
     */
    public $router 				= null;
    /**
     *
     * @var cmRouter
     */
    public $cache_router 			= null;
    public $cache_force_enable		= null;
    public $cache_force_expire		= null;
    public $cache_force_max_age	= null;
    
    /**
     *
     * @var ffMemCache
     */
    public $cache					= null;

    //var $applets_components 	= array();
    public $loaded_applets 		= array();
    public $modules				= array();
    
    /**
     *
     * @var ffPage_base
     */
    public $oPage					= null;
    /**
     *
     * @var ffTemplate
     */
    public $tpl_content 			= null;
    
    public $json_response			= array(
            "success" => true
        );
    
    private function __construct()
    {
        if (isset($_REQUEST["__query__"])) {
            ffDB_Sql::$_profile = true;
        }
            
        $this->router = cmRouter::getInstance();
    }
    
    private function __clone()
    {
    }
    
    /**
     *
     * @return cm
     */
    public static function getInstance()
    {
        if (self::$singleton === null) {
            self::$singleton = new cm();
        }

        return self::$singleton;
    }



    /**
    * EVENTS OVERRIDING
    */
    
    public static function _addEvent($event_name, $func_name, $priority = null, $index = 0, $break_when = null, $break_value = null, $additional_data = null)
    {
        self::initEvents();
        return self::$_events->addEvent($event_name, $func_name, $priority, $index, $break_when, $break_value, $additional_data);
    }

    public static function _doEvent($event_name, $event_params = array())
    {
        self::initEvents();
        return self::$_events->doEvent($event_name, $event_params);
    }
    
    private static function initEvents()
    {
        if (self::$_events === null) {
            self::$_events = new ffEvents();
        }
    }

    public function doEvent($event_name, $event_params = array())
    {
        return self::_doEvent($event_name, $event_params);
    }
    
    public function addEvent($event_name, $func_name, $priority = null, $index = 0, $break_when = null, $break_value = null, $additional_data = null)
    {
        return self::_addEvent($event_name, $func_name, $priority, $index, $break_when, $break_value, $additional_data);
    }

    /**
     * Env
     */
    public static function env($name, $value = null)
    {
        if ($value !== null) {
            self::$env[$name] = $value;
        }

        return self::$env[$name];
    }

    public function load_env($envs)
    {
        foreach ($envs as $key => $env) {
            self::$env[$key] = $env["value"];
        }
    }
    public function load_env_by_xml(SimpleXMLElement $xml)
    {
        if (isset($xml) && count($xml->children())) {
            foreach ($xml->children() as $key => $properties) {
                $attrs = $properties->attributes();
                $value = (string) $attrs["value"];
                switch ($value) {
                    case "false":
                        $value = false;
                        break;
                    case "true":
                        $value = true;
                        break;
                    default:
                }

                self::env($key, $value);
            }
        }
    }

    private function loadConfig($file, $type = null)
    {
        if (is_file($file)) {
            $fs = Filemanager::getInstance("xml");
            $config = $fs->read($file);

            if (is_array($config["env"]) && count($config["env"])) {
                foreach ($config["env"] as $key => $value) {
                    $config["env"][$key] = (
                        $value["@attributes"]
                        ? $value["@attributes"]
                        : $value
                    );
                }
            }
            if ($config["env"]) {
                $this->load_env($config["env"]);
            }
            if ($type && !isset($this->config[$type])) {
                $this->config[$type] = array();
            }
            if ($type) {
                $ref = &$this->config[$type];
            } else {
                $ref = &$this->config;
            }

            if (is_array($config) && count($config)) {
                foreach ($config as $name => $params) {
                    if ($name) {
                        $ref[$name] = array_replace((array)$ref[$name], $params);
                    }
                }
            }
        }
    }

    private function loadORM($entry)
    {
        if (@is_file(CM_MODULES_ROOT . "/" . $entry . "/ds/common.php")) {
            require CM_MODULES_ROOT . "/" . $entry . "/ds/common.php";
        }
        if (@is_dir(CM_MODULES_ROOT . "/" . $entry . "/ds/sources")) {
            $itGroup = new DirectoryIterator(CM_MODULES_ROOT . "/" . $entry . "/ds/sources");
            foreach ($itGroup as $fiGroup) {
                if ($fiGroup->isDot() || $fiGroup->isDir()) {
                    continue;
                }

                require($fiGroup->getPathname());
            }
        }
    }

    /**
    * Main Process Fucntion (use only once per session)
    *
    */
    public function process()
    {
        if (CM_ENABLE_MEM_CACHING) {
            $this->cache = ffCache::getInstance();
            if (isset($_REQUEST["__CLEARCACHE__"])) {
                $this->cache->clear();
            }
        }

        $this->path_info 		= $_SERVER['PATH_INFO'];
        $this->query_string 	= $_SERVER['QUERY_STRING'];
        
        // #0: verifica configurazione

        // #1: normalizzazione dell'url

        $this->path_info = ffCommon_url_normalize($this->path_info);
        $this->path_info = ffCommon_url_stripslashes($this->path_info);

        if (!strlen($this->path_info) || $this->path_info == "/") {  //todo: da verificare se serve. Con la cache in home, la cache si sfonda
            $this->path_info = "/index";
            if (CM_URL_NORMALIZE) {
                $_SERVER['PATH_INFO'] = $this->path_info;
            }
        }
        
        // STATIC CACHE
        if (CM_PAGECACHE && !$this->isXHR() && !isset($_REQUEST["__nocache__"]) && !isset($_REQUEST["__debug__"]) && !defined("CM_DONT_RUN") && strpos($this->path_info, "sitemap.xml") === false) {
            define("ALLOW_PAGECACHE", true);
            
            $cache_dir = CM_PAGECACHE_DIR;
            if (CM_PAGECACHE_BYDOMAIN) {
                $cache_domain_prefix = $_SERVER["HTTP_HOST"];
                if (CM_PAGECACHE_BYDOMAIN_STRIPWWW && strpos($cache_domain_prefix, "www.") === 0) {
                    $cache_domain_prefix = substr($cache_domain_prefix, 4);
                }
                $cache_dir .= "/" . $cache_domain_prefix;
            }
            
            if (CM_PAGECACHE_GROUPHASH) {
                $hash = sha1($this->path_info);
                $parts = str_split($hash, 2);
                $cache_dir .= "/" . implode("/", $parts);
            }
            
            if (file_exists($cache_dir)) {
                if (CM_PAGECACHE_ASYNC && array_key_exists("__GENCACHE__", $_REQUEST)) {
                    if (CM_PAGECACHE_GROUPDIRS) {
                        $itGroup = new DirectoryIterator($cache_dir);
                        foreach ($itGroup as $fiGroup) {
                            if ($fiGroup->isDot()) {
                                continue;
                            }

                            $filePath = $fiGroup->getPathname();
                            $file = $filePath . $this->path_info . "/" . $_SERVER["HTTP_IF_NONE_MATCH"];
                            cm_filecache_empty_dir($file);
                        }
                    } else {
                        $file = $cache_dir . $this->path_info . "/" . $_SERVER["HTTP_IF_NONE_MATCH"];
                        cm_filecache_empty_dir($file);
                    }
                } else {
                    $now = time();

                    if (isset($_SERVER["HTTP_IF_NONE_MATCH"])) {
                        if (CM_PAGECACHE_GROUPDIRS) {
                            $itGroup = new DirectoryIterator($cache_dir);
                            foreach ($itGroup as $fiGroup) {
                                if ($fiGroup->isDot()) {
                                    continue;
                                }

                                $filePath = $fiGroup->getPathname();
                                $file = $filePath . $this->path_info . "/" . $_SERVER["HTTP_IF_NONE_MATCH"];
                                if (false !== ($fctime = @filectime($file))) {
                                    if (
                                            (
                                                !CM_PAGECACHE_LAST_VALID
                                                || $now < CM_PAGECACHE_LAST_VALID
                                                || $fctime >= CM_PAGECACHE_LAST_VALID
                                            ) && (
                                                CM_PAGECACHE_ASYNC ||
                                                filemtime($file) > $now
                                            )
                                    ) {
                                        $cache_valid = true;
                                    } else {
                                        @unlink($file);
                                    }
                                }
                            }
                        } else {
                            $file = $cache_dir . $this->path_info . "/" . $_SERVER["HTTP_IF_NONE_MATCH"];
                            /*$fctime = @filectime($file);
                            $a = $now < CM_PAGECACHE_LAST_VALID;
                            $b = $fctime >= CM_PAGECACHE_LAST_VALID;
                            $c = filemtime($file) > $now;
                            $d = !CM_PAGECACHE_LAST_VALID;
                            $e = CM_PAGECACHE_ASYNC;*/
                            if (false !== ($fctime = @filectime($file))) {
                                if (
                                        (
                                            !CM_PAGECACHE_LAST_VALID
                                            || $now < CM_PAGECACHE_LAST_VALID
                                            || $fctime >= CM_PAGECACHE_LAST_VALID
                                        ) && (
                                            CM_PAGECACHE_ASYNC ||
                                            filemtime($file) > $now
                                        )
                                ) {
                                    $cache_valid = true;
                                } else {
                                    @unlink($file);
                                }
                            }
                        }
                        if ($cache_valid) {
                            http_response_code(304);
                            exit;
                        }
                    }

                    // find right file
                    if (null !== ($find_cache_file = cm_filecache_find($cache_dir, $this->path_info, CM_PAGECACHE_GROUPDIRS, CM_PAGECACHE_USE_STRONG_CACHE, CM_PAGECACHE_LAST_VALID, CM_PAGECACHE_SCALEDOWN, $now))
                        ) {
                        if (!ffHTTP_encoding_isset("gzip")) {
                            if ($find_cache_file["uncompressed"] !== null) {
                                $cache_file = $find_cache_file["uncompressed"];
                            }
                        } else {
                            if ($find_cache_file["compressed"] !== null) {
                                $cache_file = $find_cache_file["compressed"];
                            } elseif (CM_PAGECACHE_SCALEDOWN && $find_cache_file["uncompressed"] !== null) {
                                $cache_file = $find_cache_file["uncompressed"];
                            }
                        }
                    }

                    if ($cache_file !== null) {
                        if ($cache_file["compressed"] !== false) {
                            header("Content-Encoding: " . $cache_file["compressed"]);
                        }

                        $mime_type = ffMedia::getMimeTypeByExtension($cache_file["file_parts"][1], "text/html");
                        if ($mime_type == "text/html") {
                            $mime_type .= "; charset=UTF-8";
                        }
                        header("Content-type: " . $mime_type);

                        // send regenerated info about cache
                        if (CM_PAGECACHE_USE_STRONG_CACHE) {
                            $exp_gmt = gmdate("D, d M Y H:i:s", $cache_file["fmtime"]) . " GMT";
                            header("Expires: $exp_gmt");
                        } else {
                            $max_age = $cache_file["file_parts"][2];
                            if ($cache_file["fmtime"] - $cache_file["fctime"] < $max_age) {
                                $max_age = $cache_file["fmtime"] - $cache_file["fctime"];
                            }

                            header("Cache-Control: public, max-age=" . $max_age);
                        }

                        header("ETag: " . $cache_file["filename"]);

                        readfile($cache_file["file"]);
                        exit;
                    }
                }
            }
            // nessuna cache trovata
            $this->cache_router = cmRouter::getInstance("__cm_cache__");
        } else {
            define("ALLOW_PAGECACHE", false);
        }

        $this->doEvent("on_before_init", array($this));

        if (FF_ORM_ENABLE) {
            if (@is_file(FF_DISK_PATH . "/ds/common.php")) {
                require FF_DISK_PATH . "/ds/common.php";
            }
            
            if (@is_dir(FF_DISK_PATH . "/ds/sources")) {
                $itGroup = new DirectoryIterator(FF_DISK_PATH . "/ds/sources");
                foreach ($itGroup as $fiGroup) {
                    if ($fiGroup->isDot() || $fiGroup->isDir()) {
                        continue;
                    }

                    require($fiGroup->getPathname());
                }
            }
        }

        // #2: inizializzazione classi

        $ff = ffGlobals::getInstance("ff");
        if (!is_object($ff) || !is_object($ff->events) || !(method_exists($ff->events, "addEvent"))) {
            ffErrorHandler::raise("Errore Critico (Rebecca)", E_USER_ERROR, get_included_files(), get_defined_vars());
        }
        $ff->events->addEvent("onRedirect", "cm::onRedirect");
        if (CM_ENABLE_MEM_CACHING) {
            $router_loaded = $this->router->loadMem();
        }

        if (!$router_loaded) {
            $this->router->loadFile(cm_confCascadeFind(__DIR__ . "/conf", "/cm", "routing_table.xml"));

            if (is_file(__PRJ_DIR__ . "/conf/routing_table.xml")) {
                $this->router->loadFile(__PRJ_DIR__. "/conf/routing_table.xml");
            }
        }

        if (ALLOW_PAGECACHE) {
            if (CM_ENABLE_MEM_CACHING) {
                $cache_router_loaded = $this->cache_router->loadMem();
            }

            if (!$cache_router_loaded) {
                $this->cache_router->loadFile(cm_confCascadeFind(__DIR__ . "/conf", "/cm", "cache_routing_table.xml"));

                if (is_file(__PRJ_DIR__ . "/conf/cache_routing_table.xml")) {
                    $this->cache_router->loadFile(__PRJ_DIR__ . "/conf/cache_routing_table.xml");
                }
            }
        }

        $this->doEvent("on_after_init", array($this));

        // #3: precaricamento moduli

        $this->loadConfig(__DIR__ . "/conf/config.xml");
        $this->loadConfig(FF_DISK_PATH . "/conf/config.xml");

        $modules = new DirectoryIterator(CM_MODULES_ROOT);
        foreach ($modules as $module) {
            if ($module->isDot()) {
                continue;
            }
            $entry = $module->getBasename();

            if (!isset($this->modules[$entry])) {
                $this->modules[$entry] = array();
            }

            if (!isset($this->modules[$entry]["events"])) {
                $this->modules[$entry]["events"] = new ffEvents();
            }

            $this->loadConfig(CM_MODULES_ROOT . "/" . $entry . "/conf/config.xml");
            $this->loadConfig(FF_DISK_PATH . "/conf/modules/" . $entry . "/config.xml");
            //todo: da togliere in futuro
            if (@is_file(__PRJ_DIR__ . "/conf/modules/" . $entry . "/config." . FF_PHP_EXT)) {
                require __PRJ_DIR__ . "/conf/modules/" . $entry . "/config." . FF_PHP_EXT;
            }

            //todo: da togliere in futuro
            if (@is_file(CM_MODULES_ROOT . "/" . $entry . "/conf/config." . FF_PHP_EXT)) {
                require CM_MODULES_ROOT . "/" . $entry . "/conf/config." . FF_PHP_EXT;
            }


            if (is_file(CM_MODULES_ROOT . "/" . $entry . "/autoload." . FF_PHP_EXT)) {
                require CM_MODULES_ROOT . "/" . $entry . "/autoload." . FF_PHP_EXT;
            }

            if (!$router_loaded) {
                //todo: da eliminare is_file nel futuro
                if (is_file(FF_DISK_PATH . "/conf/modules/" . $entry . "/routing_table.xml")) {
                    $this->router->loadFile(FF_DISK_PATH . "/conf/modules/" . $entry . "/routing_table.xml");
                } else {
                    $this->router->loadFile(CM_MODULES_ROOT . "/" . $entry . "/conf/routing_table.xml");
                }
                if (ALLOW_PAGECACHE) {
                    //todo: da eliminare is_file nel futuro
                    if (is_file(FF_DISK_PATH . "/conf/modules/" . $entry . "/cache_routing_table.xml")) {
                        $this->router->loadFile(FF_DISK_PATH . "/conf/modules/" . $entry . "/cache_routing_table.xml");
                    } else {
                        $this->router->loadFile(CM_MODULES_ROOT . "/" . $entry . "/conf/cache_routing_table.xml");
                    }
                }
            }

            if (is_file(CM_MODULES_ROOT . "/" . $entry . "/common." . FF_PHP_EXT)) {
                require CM_MODULES_ROOT . "/" . $entry . "/common." . FF_PHP_EXT;
            }

            if (FF_ORM_ENABLE) {
                $this->loadORM($entry);
            }

            $res = $this->doEvent("on_load_module", array($this, $entry));
        }


        /*
                echo "cm_config";
                print_r($this->config);
                echo "cm";
                print_r(cm::$env);
        
        
                echo "cms";
                print_r(cms::$env);
                echo "auth";
                print_r(Auth::$env);
                echo "profiles";
                print_r(Auth::$profiles);
                echo "packages";
                print_r(Auth::$packages);
        
                print_r($_SESSION);*/

        /*
        $d = dir(CM_MODULES_ROOT);
        while (false !== ($entry = $d->read()))
        {
            if ($entry == "." || $entry == ".." || substr($entry, 0, 1) == ".")
                continue;

            if (!isset($this->modules[$entry]))
                $this->modules[$entry] = array();

            if (!isset($this->modules[$entry]["events"]))
                $this->modules[$entry]["events"] = new ffEvents();

            if (@is_file(FF_DISK_PATH . "/conf/modules/" . $entry . "/config." . FF_PHP_EXT))
                require FF_DISK_PATH . "/conf/modules/" . $entry . "/config." . FF_PHP_EXT;

            if (@is_file(CM_MODULES_ROOT . "/" . $entry . "/conf/config." . FF_PHP_EXT))
                require CM_MODULES_ROOT . "/" . $entry . "/conf/config." . FF_PHP_EXT;

            if (@is_file(CM_MODULES_ROOT . "/" . $entry . "/autoload." . FF_PHP_EXT))
                require CM_MODULES_ROOT . "/" . $entry . "/autoload." . FF_PHP_EXT;

            if (!$router_loaded && $routing_file = cm_confCascadeFind(CM_MODULES_ROOT . "/" . $entry . "/conf", "/modules/" . $entry, "routing_table.xml"))
                $this->router->loadFile($routing_file);
            if (ALLOW_PAGECACHE && !$router_loaded && $routing_file = cm_confCascadeFind(CM_MODULES_ROOT . "/" . $entry . "/conf", "/modules/" . $entry, "cache_routing_table.xml"))
                $this->cache_router->loadFile($routing_file);

        }
        $d->close();*/

        if (CM_ENABLE_MEM_CACHING && !$router_loaded) {
            $this->router->orderRules();
            $this->cache->set("cm/router/rules", $this->router->rules);
            $this->cache->set("cm/router/named_rules", $this->router->named_rules);
        }
        if (CM_ENABLE_MEM_CACHING && ALLOW_PAGECACHE && !$cache_router_loaded) {
            $this->cache_router->orderRules();
            $this->cache->set("cm/cache_router/rules", $this->cache_router->rules);
            $this->cache->set("cm/cache_router/named_rules", $this->cache_router->named_rules);
        }
        /*
        foreach ($this->modules as $key => $value)
        {
            if (@is_file(CM_MODULES_ROOT . "/" . $key . "/common." . FF_PHP_EXT))
                require CM_MODULES_ROOT . "/" . $key . "/common." . FF_PHP_EXT;
        }
        reset($this->modules);*/
        /*
                foreach ($this->modules as $key => $value) //todo: se fatto ad oggetti e da togliere
                {
                    if (FF_ORM_ENABLE)
                    {
                        if (@is_file(CM_MODULES_ROOT . "/" . $key . "/ds/common.php"))
                            require CM_MODULES_ROOT . "/" . $key . "/ds/common.php";
        
                        if (@is_dir(CM_MODULES_ROOT . "/" . $key . "/ds/sources"))
                        {
                            $itGroup = new DirectoryIterator(CM_MODULES_ROOT . "/" . $key . "/ds/sources");
                            foreach($itGroup as $fiGroup)
                            {
                                if($fiGroup->isDot() || $fiGroup->isDir())
                                    continue;
        
                                require($fiGroup->getPathname());
                            }
                        }
                    }
        
                    $res = $this->doEvent("on_load_module", array($this, $key));
                }
                reset($this->modules);*/

        $res = $this->doEvent("on_modules_loaded", array($this));

        if (defined("CM_DONT_RUN")) {
            return;
        }

        $res = $this->doEvent("on_before_cm", array($this));
        $rc = end($res);
        if (is_array($rc) && array_key_exists("path_info", $rc)) {
            $layout_path_info = $rc["path_info"];
        } else {
            $layout_path_info = $this->path_info;
        }

        // #4: Inizializzazione Layout
        if (!defined("CM_DONT_RUN_LAYOUT") && strpos($this->query_string, "__nolayout__") === false) {
            $this->layout_vars = $this->getLayoutByPath($layout_path_info);
            if ($this->isXHR() && isset($_REQUEST["XHR_THEME"])) {
                $this->layout_vars["theme"] = $_REQUEST["XHR_THEME"];
            }
        }

        if (isset($this->layout_vars["main_theme"]) && strlen($this->layout_vars["main_theme"])) {
            define("CM_LOADED_THEME", $this->layout_vars["main_theme"]);
        } else {
            define("CM_LOADED_THEME", CM_DEFAULT_THEME);
        }

        //define("FF_THEME_ONLY_INIT", true);
        ffCommon_theme_init(CM_LOADED_THEME);

        foreach ($this->modules as $key => $value) {
            if (@is_file(CM_MODULES_ROOT . "/" . $key . "/themes/" . CM_LOADED_THEME . "/ff/common." . FF_PHP_EXT)) {
                require CM_MODULES_ROOT . "/" . $key . "/themes/" . CM_LOADED_THEME . "/ff/common." . FF_PHP_EXT;
            }
        }
        reset($this->modules);

        if (!isset($this->layout_vars["theme"]) || !strlen($this->layout_vars["theme"])) {
            $this->layout_vars["theme"] = cm_getMainTheme();
        }

        $this->doEvent("on_before_page_process", array($this));

        $this->oPage = ffPage::factory(FF_DISK_PATH, FF_SITE_PATH, $this->oPage->page_path, $this->layout_vars["theme"]);
        $this->oPage->addEvent("on_page_process", "cm::oPage_on_page_process");
        //$this->oPage->addEvent("on_after_process_components", "cm::oPage_on_after_process_components", ffEvent::PRIORITY_HIGH);
        $this->oPage->addEvent("on_page_processed", "cm::oPage_on_page_processed", ffEvent::PRIORITY_HIGH);

        if (CM_IGNORE_THEME_DEFAULTS || $this->layout_vars["ignore_defaults"]) {
            $this->oPage->page_css = array();
            $this->oPage->page_js = array();
            $this->oPage->page_meta = array();
        }
        
        if (strlen($this->layout_vars["title"])) {
            $this->oPage->title = str_replace("[CM_LOCAL_APP_NAME]", cm_getAppName(), $this->layout_vars["title"]);
        } else {
            $this->oPage->title = cm_getAppName();
        }
        
        if (strlen($this->layout_vars["class_body"])) {
            $this->oPage->class_body = $this->layout_vars["class_body"];
        }
            
        $this->oPage->use_own_form = !$this->layout_vars["exclude_form"];
        $this->oPage->use_own_js = !$this->layout_vars["exclude_ff_js"];
        $this->oPage->compact_js = $this->layout_vars["compact_js"];
        $this->oPage->compact_css = $this->layout_vars["compact_css"];
        $this->oPage->compress = $this->layout_vars["enable_gzip"];

        if (is_array($this->layout_vars["cdn"]["css"]) && count($this->layout_vars["cdn"]["css"])) {
            $this->oPage->override_css = array_merge($this->oPage->override_css, $this->layout_vars["cdn"]["css"]);
        }
        if (is_array($this->layout_vars["cdn"]["js"]) && count($this->layout_vars["cdn"]["js"])) {
            $this->oPage->override_js = array_merge($this->oPage->override_js, $this->layout_vars["cdn"]["js"]);
        }
        
        //$this->oPage->addEvent("on_tpl_load", "cm::oPage_on_process_parts", ffEvent::PRIORITY_HIGH);
        //$this->oPage->addEvent("on_tpl_layer_loaded", "cm::oPage_on_process_parts", ffEvent::PRIORITY_HIGH);

        if (!isset($_REQUEST["__nolayout__"])) {
            $this->doEvent("on_layout_init", array($this->oPage, $this->layout_vars));

            if ($this->layout_vars["page"] != "default" && $this->layout_vars["page"] !== null) {
                $this->oPage->template_file = "ffPage_" . $this->layout_vars["page"] . ".html";
            }
                
            if (strlen($this->layout_vars["layer"])) {
                $this->oPage->layer = $this->layout_vars["layer"];
            }
                                                               
            if (is_array($this->layout_vars["sect"]) && count($this->layout_vars["sect"])) {
                foreach ($this->layout_vars["sect"] as $key => $value) {
                    $this->oPage->addSection($key);
                    $this->oPage->sections[$key]["name"] = $value;
                }
                reset($this->layout_vars["sect"]);
            }

            if (is_array($this->layout_vars["css"]) && count($this->layout_vars["css"])) {
                foreach ($this->layout_vars["css"] as $key => $value) {
                    $this->oPage->tplAddCss($key, $value["file"], $value["path"], "stylesheet", "text/css", true, false, null, $value["exclude_compact"], $value["priority"]);
                }
                reset($this->layout_vars["css"]);
            }

            if (is_array($this->layout_vars["js"]) && count($this->layout_vars["js"])) {
                foreach ($this->layout_vars["js"] as $key => $value) {
                    $this->oPage->tplAddJs($key, $value["file"], $value["path"], true, false, null, false, $value["priority"]);
                }
                reset($this->layout_vars["js"]);
            }
            if (is_array($this->layout_vars["meta"]) && count($this->layout_vars["meta"])) {
                foreach ($this->layout_vars["meta"] as $key => $value) {
                    $this->oPage->tplAddMeta($key, $value["content"], true, $value["type"]);
                }
                reset($this->layout_vars["meta"]);
            }
        }
        

        $this->doEvent("on_before_routing", array($this));

        // #5: elaborazione richiesta
        //ffErrorHandler::raise("DEBUG", E_USER_ERROR, $this, get_defined_vars());

        // caricamento dei config / common
        $include_script_path_parts = explode("/", $this->path_info);
        $include_script_path_tmp = FF_DISK_PATH . "/conf/contents";
        $include_script_path_count = 0;
        while ($include_script_path_count < count($include_script_path_parts) && $include_script_path_tmp .= $include_script_path_parts[$include_script_path_count] . "/") {
            if (@is_file($include_script_path_tmp . "config." . FF_PHP_EXT)) {
                require $include_script_path_tmp . "config." . FF_PHP_EXT;
            }
            if (@is_file($include_script_path_tmp . "config_" . $this->oPage->getTheme() . "." . FF_PHP_EXT)) {
                require $include_script_path_tmp . "config_" . $this->oPage->getTheme() . "." . FF_PHP_EXT;
            }
            $include_script_path_count++;
        }

        $include_script_path_parts = explode("/", $this->path_info);
        $include_script_path_tmp = FF_DISK_PATH . "/conf/contents";
        $include_script_path_count = 0;
        while ($include_script_path_count < count($include_script_path_parts) && $include_script_path_tmp .= $include_script_path_parts[$include_script_path_count] . "/") {
            if (@is_file($include_script_path_tmp . "common." . FF_PHP_EXT)) {
                require $include_script_path_tmp . "common." . FF_PHP_EXT;
            }
            if (@is_file($include_script_path_tmp . "common_" . $this->oPage->getTheme() . "." . FF_PHP_EXT)) {
                require $include_script_path_tmp . "common_" . $this->oPage->getTheme() . "." . FF_PHP_EXT;
            }
            $include_script_path_count++;
        }

        unset($include_script_path_parts, $include_script_path_tmp, $include_script_path_count);

        if (CM_ENABLE_MEM_CACHING && CM_ENABLE_PATH_CACHE) {
            $this->router->matched_rules = $this->cache->get($this->path_info, "/cm/router/matches");
        }

        if (!$this->router->matched_rules) {
            $this->router->process($this->path_info, $this->query_string, $_SERVER["HTTP_HOST"]);
            if (CM_ENABLE_MEM_CACHING && CM_ENABLE_PATH_CACHE) {
                $this->cache->set($this->path_info, $this->router->matched_rules, "/cm/router/matches");
            }
        }

        if (!is_array($this->router->matched_rules) && !count($this->router->matched_rules)) {
            ffErrorHandler::raise("CM: no available routes!", E_USER_ERROR, $this, get_defined_vars());
        }

        //ffErrorHandler::raise("DEBUG", E_USER_ERROR, $this, get_defined_vars());

        foreach ($this->router->matched_rules as $key => $match) {
            $this->process_next_rule = null;
            $this->real_path_info = null;
            $this->script_name = null;
            $this->is_php = null;
            $this->is_resource = null;
            $this->module = null;

            $this->processed_rule = $match;
            
            $match_attrs = $match["rule"]->__attributes;
            
            $this->processed_rule_attrs = $match_attrs;

            if (isset($match["rule"]->destination->header)) {
                if (isset($match["rule"]->destination->location)) {
                    $location = str_replace("[SITE_PATH]", FF_SITE_PATH, (string)$match["rule"]->destination->location);
                }
                
                /*
                 * Da sistemare, non funziona con stesso path_info su hostname diverso (probabilmente da togliere)
                if(strlen($location) && strpos($location, str_replace("/index", "/", $this->path_info)) !== 0 && str_replace("/index", "/", $this->path_info) != "/")
                {
                    continue;
                }*/

                if (isset($match["rule"]->useragent)) {
                    $skip_rule = true;
                    if (isset($match["rule"]->useragent->browser) && strlen($match["rule"]->useragent->browser)) {
                        $arrBrowser = explode(",", strtolower($match["rule"]->useragent->browser));
                        $actualBrowser = $this->getBrowser();
                        if (array_search(strtolower($actualBrowser["name"]), $arrBrowser) !== false) {
                            if (isset($match["rule"]->useragent->version) && strlen($match["rule"]->useragent->version)) {
                                $arrBrowserVersion = explode(",", $match["rule"]->useragent->version);
                                if (array_search($actualBrowser["majorver"], $arrBrowserVersion) !== false) {
                                    $skip_rule = false;
                                }
                            } else {
                                $skip_rule = false;
                            }
                        }
                    }
                    if ($skip_rule) {
                        continue;
                    }
                }

                http_response_code((int)$match["rule"]->destination->header);
                if (strlen($location)) {
                    ffRedirect($location);
                }
                exit;
            } elseif (isset($match["rule"]->destination->file)) {
                $file = FF_DISK_PATH . "/" . trim((string)$match["rule"]->destination->file, "/");
                if (!is_file($file)) {
                    ffErrorHandler::raise("FILE NOT FOUND", E_USER_ERROR, $this, get_defined_vars());
                }
                

                ffMedia::sendHeaders($file);
                readfile($file);
                exit;
            }
            
            if (isset($match["rule"]->destination->module)) {
                $this->module = (string)$match["rule"]->destination->module;
                $this->module_path = CM_MODULES_ROOT . "/" . $this->module;
                $doc_root = CM_MODULES_ROOT . "/" . $this->module . "/contents";
            } else {
                $doc_root = FF_DISK_PATH;
            }
            
            $this->content_root = $doc_root;

            $url = (string)$match["rule"]->destination->url;
            
            for ($i = 0; $i < 10; $i++) {
                $url = str_replace('$' . $i, $match["params"][$i][0], $url);
            }
            
            /*if (isset($match["rule"]->params) && isset($match["rule"]->params->param) && count($match["rule"]->params->param))
            {
                foreach ($match["rule"]->params->param as $key => $value)
                {
                    if (CM_ENABLE_MEM_CACHING)
                        $attrs = $value->__attributes;
                    else
                        $attrs = $value->attributes();

                    $value = (string)$attrs["value"];
                    for ($i = 0; $i < 10; $i++)
                    {
                        $value = str_replace('$' . $i, $match["params"][$i][0], $value);
                    }

                    if (strlen($value))
                    {
                        $_REQUEST[(string)$attrs["name"]] = $value;
                    }

                }
            }*/
            
            $url = str_replace("[MAIN_THEME]", FF_THEME_DIR . "/" . cm_getMainTheme(), $url);
            $url = str_replace("[THEME]", FF_THEME_DIR . "/" . $this->oPage->getTheme(), $url);
            
            // rileva il file giusto da caricare procedendo con test a ritroso
            $tmp_path = $url;
            $tmp_url = $url;

            do {
                if ($tmp_path == "" || $tmp_path == "/") {
                    $tmp_path = "/index";
                    $tmp_url = "/index" . $tmp_url;
                }
                if (strpos($tmp_path, "." . FF_PHP_EXT . "/") === false) {
                    $tmp_ext = pathinfo($tmp_path, PATHINFO_EXTENSION);
                    if (@is_file($doc_root . $tmp_path . "." . FF_PHP_EXT) || (@is_file($doc_root . $tmp_path) && $tmp_ext == FF_PHP_EXT)) {
                        $this->real_path_info = substr($tmp_url, strlen($tmp_path));
                        if (strlen($tmp_ext)) {
                            $this->script_name = $tmp_path;
                        } else {
                            $this->script_name = $tmp_path . "." . FF_PHP_EXT;
                        }
                        $this->is_php = true;
                        $this->is_resource = false;
                        break;
                    } elseif (@is_file($doc_root . $tmp_path . ".html") || (@is_file($doc_root . $tmp_path) && $tmp_ext == "html")) {
                        $this->real_path_info = substr($tmp_url, strlen($tmp_path));
                        if (strlen($tmp_ext)) {
                            $this->script_name = $tmp_path;
                        } else {
                            $this->script_name = $tmp_path . ".html";
                        }
                        $this->is_php = false;
                        $this->is_resource = false;
                        break;
                    } elseif (@is_file($doc_root . $tmp_path . "/index." . FF_PHP_EXT)) {
                        $this->real_path_info = substr($tmp_url, strlen($tmp_path));
                        $this->script_name = $tmp_path . "/index." . FF_PHP_EXT;
                        $this->is_php = true;
                        $this->is_resource = false;
                        break;
                    } elseif (@is_file($doc_root . $tmp_path . "/index.html")) {
                        $this->real_path_info = substr($tmp_url, strlen($tmp_path));
                        $this->script_name = $tmp_path . "/index.html";
                        $this->is_php = false;
                        $this->is_resource = false;
                        break;
                    } elseif (@is_file($doc_root . $tmp_path)) {
                        $this->real_path_info = substr($tmp_url, strlen($tmp_path));
                        $this->script_name = $tmp_path;
                        $this->is_php = false;
                        $this->is_resource = true;
                        break;
                    }
                }
                if ($tmp_path == "/index") {
                    break;
                }

                if ($tmp_path != "/index") {
                    if (substr($tmp_path, -1) == "/") {
                        $tmp_path = substr($tmp_path, 0, -1);
                    } else {
                        $tmp_path = ffCommon_dirname($tmp_path);
                    }
                }
            } while (true);

            if (strpos($this->content_root . $this->script_name, FF_DISK_PATH . "/index.") === 0) {
                continue;
            }

            if ((!isset($match["rule"]->accept_path_info) || (string)$match["rule"]->accept_path_info == "false") && strlen($this->real_path_info)) {
                continue;
            }

            // se ha trovato qualcosa da eseguire, lo esegue
            if (strlen($this->script_name)) {
                $path_parts = explode("/", $this->path_info);
                $script_parts = explode("/", $this->script_name);
                
                if (
                    end($path_parts) . ".html" == end($script_parts)
                    || end($path_parts) . "." . FF_PHP_EXT == end($script_parts)
                ) {
                    $this->oPage->page_path = ffCommon_dirname($this->path_info);
                } else {
                    if (strlen($this->real_path_info)) {
                        $this->oPage->page_path = substr($this->path_info, 0, strlen($this->real_path_info) * -1);
                    } else {
                        $this->oPage->page_path = $this->path_info;
                    }
                }
                
                if ($this->is_php) {
                    if (file_exists($this->content_root . $this->script_name)) {
                        $this->callScript($this->content_root . $this->script_name);
                    } elseif (file_exists($this->content_root . $this->script_name . ".php")) {
                        $this->callScript($this->content_root . $this->script_name . ".php");
                    }
                        
                    if ($this->module !== null) {
                        $tmp_mod_parts = explode("/", $this->script_name);
                        array_pop($tmp_mod_parts);
                        $tmp_mod_path = implode("/", $tmp_mod_parts);
                        if (is_file(FF_THEME_DISK_PATH . "/" . cm_getMainTheme() . "/modules/" . $this->module . $tmp_mod_path . "/config.php")) {
                            require FF_THEME_DISK_PATH . "/" . cm_getMainTheme() . "/modules/" . $this->module . $tmp_mod_path . "/config.php";
                        }
                        if (is_file(FF_THEME_DISK_PATH . "/" . cm_getMainTheme() . "/modules/" . $this->module . $tmp_mod_path . "/common.php")) {
                            require FF_THEME_DISK_PATH . "/" . cm_getMainTheme() . "/modules/" . $this->module . $tmp_mod_path . "/common.php";
                        }
                        if ($this->oPage->getTheme() !== cm_getMainTheme()) {
                            if (is_file($this->oPage->getThemeDir() . "/modules/" . $this->module . $tmp_mod_path . "/config.php")) {
                                require $this->oPage->getThemeDir() . "/modules/" . $this->module . $tmp_mod_path . "/config.php";
                            }
                            if (is_file($this->oPage->getThemeDir() . "/modules/" . $this->module . $tmp_mod_path . "/common.php")) {
                                require $this->oPage->getThemeDir() . "/modules/" . $this->module . $tmp_mod_path . "/common.php";
                            }
                        }
                    }
                } else {
                    if ($this->is_resource) {
                        ffMedia::sendHeaders($this->content_root . $this->script_name);
                        readfile($this->content_root . $this->script_name);
                        exit;
                    } else {
                        $this->tpl_content = ffTemplate::factory($this->content_root);
                        $this->tpl_content->load_file($this->script_name, "main");
                        $this->tpl_content->set_var("site_path", FF_SITE_PATH);
                        $this->tpl_content->set_var("theme", $this->oPage->theme);
                        $this->tpl_content->set_var("ret_url", $_REQUEST["ret_url"]);
                        $this->tpl_content->set_var("encoded_ret_url", rawurlencode($_REQUEST["ret_url"]));
                        $this->tpl_content->set_var("encoded_this_url", rawurlencode($_SERVER["REQUEST_URI"]));
                        //$this->preloadApplets($this->tpl_content);

                        $this->doEvent("cm_onParseFixed", array(&$this));
                    }
                }

                $this->load_ffSettingsByPath(); // TODO: ?? appurare perchè non si trova fuori dal ciclo e non viene richiamato una sola volta

                if (isset($this->processed_rule["rule"]->blocking) && (string)$this->processed_rule["rule"]->blocking != "false") {
                    exit;
                }

                if ($this->process_next_rule === null && isset($this->processed_rule["rule"]->process_next) && (string)$this->processed_rule["rule"]->process_next != "false") {
                    $this->process_next_rule = true;
                }
                    
                if (!$this->process_next_rule) {
                    break;
                }
            }
        }
        reset($this->router->matched_rules);

        if (
                strlen($this->real_path_info) &&
                (
                    !isset($match["rule"]->accept_path_info) ||
                    (isset($match["rule"]->accept_path_info) && (string)$match["rule"]->accept_path_info == "false")
                )
            ) {
            $this->responseCode(404);
        }
        
        // LOAD SETTINGS BY COMPONENT
        if (is_dir(FF_DISK_PATH . "/conf/ffsettings/components")) {
            foreach ($this->oPage->components as $key => $value) {
                if (@is_file(FF_DISK_PATH . "/conf/ffsettings/components/" . $key . ".xml")) {
                    $this->load_ffSettings(FF_DISK_PATH . "/conf/ffsettings/components/" . $key . ".xml");
                }
            }
            reset($this->oPage->components);
        }

        $include_script_path_tmp = FF_DISK_PATH . "/conf/contents" . $this->path_info . "/";
        if (@is_file($include_script_path_tmp . "custom." . FF_PHP_EXT)) {
            require $include_script_path_tmp . "custom." . FF_PHP_EXT;
        }
        if (@is_file($include_script_path_tmp . "custom_" . $this->oPage->getTheme() . "." . FF_PHP_EXT)) {
            require $include_script_path_tmp . "custom_" . $this->oPage->getTheme() . "." . FF_PHP_EXT;
        }
        unset($include_script_path_tmp);

        $this->doEvent("on_before_process", array($this));
        
        $this->oPage->process_params();

        if (!ffErrorHandler::$hide && (isset($_REQUEST["__debug__"]) || isset($_REQUEST["__query__"]))) {
            $this->oPage->compress = false;
        }

        if (ALLOW_PAGECACHE && $this->cache_force_enable !== false && http_response_code() == 200 && !@file_exists($cache_dir . "/disk_fail") && (!CM_PAGECACHE_ASYNC || (CM_PAGECACHE_ASYNC && array_key_exists("__GENCACHE__", $_REQUEST)))) {
            $enable_cache = false;
            $cache_disk_fail = false;
            $max_age = CM_PAGECACHE_DEFAULT_MAXAGE;
            $expires = null;

            if ($this->cache_force_max_age === null || $this->cache_force_expire === null) {
                if (CM_ENABLE_MEM_CACHING && CM_CACHE_ROUTER_MATCH) {
                    $this->cache_router->matched_rules = $this->cache->get($this->path_info, "/cm/cache_router/matches");
                }

                if (!$this->cache_router->matched_rules) {
                    $this->cache_router->process($this->path_info, $this->query_string, $_SERVER["HTTP_HOST"]);
                    if (CM_ENABLE_MEM_CACHING && CM_CACHE_ROUTER_MATCH) {
                        $this->cache->set($this->path_info, $this->cache_router->matched_rules, "/cm/cache_router/matches");
                    }
                }

                if (is_array($this->cache_router->matched_rules) && count($this->cache_router->matched_rules)) {
                    foreach ($this->cache_router->matched_rules as $key => $match) {
                        $process_next = false;
                        if (isset($match["rule"]->control)) {
                            if (strtolower($match["rule"]->control) == "disabled") {
                                $enable_cache = false;
                            } elseif (strtolower($match["rule"]->control) == "enabled") {
                                $enable_cache = true;
                            }
                        }

                        if (isset($match["rule"]->expires)) {
                            $expires = intval($match["rule"]->expires);
                        }
                        if (isset($match["rule"]->max_age)) {
                            $max_age = intval($match["rule"]->max_age);
                        }

                        if (isset($match["rule"]->process_next)) {
                            $process_next = true;
                        }
                        if (!$process_next) {
                            break;
                        }
                    }
                    reset($this->cache_router->matched_rules);
                }
                
                if ($this->cache_force_expire !== null) {
                    $expires = $this->cache_force_expire;
                }
                if ($this->cache_force_max_age !== null) {
                    $max_age = $this->cache_force_max_age;
                }
            }
            
            if (!is_numeric($max_age) || $max_age < 0) {
                $max_age = CM_PAGECACHE_DEFAULT_MAXAGE;
            }
            if (!is_numeric($expires) || $expires < 0) {
                $expires = $max_age;
            }
            
            $this->doEvent("on_cache", array(&$this, &$enable_cache, &$max_age, &$expires));

            if ($this->cache_force_enable || $enable_cache) {
                $buffer = $this->oPage->process(false);

                // detect output mime-type and extension for file
                $mime_type = null;
                $hsent = false;
                $extension = null;
                $charset = null;
                
                $hlist = headers_list();
                foreach ($hlist as $key => $value) {
                    $rc = preg_match("/\s*([^:\s]+)\s*:\s*([^;\s]+)(;\s*([^=]+)\s*=(.+))?/", $value, $matches);
                    if ($rc && strtolower($matches[1]) == "content-type") {
                        $mime_type = $matches[2];
                        $hsent = true;
                        
                        if ($matches[4] == "charset") {
                            $charset = $matches[5];
                        }
                    }
                }
                
                if ($mime_type === null && strlen($this->path_info)) {
                    $extension = (($extension = pathinfo($this->path_info, PATHINFO_EXTENSION)) === "" ? null : $extension);
                    if ($extension !== null) {
                        $mime_type = ffMedia::getMimeTypeByExtension($extension, null);
                    }
                    if ($mime_type === null) {
                        $extension = null;
                    }
                }

                if ($mime_type === null && class_exists("finfo")) {
                    $finfo = new finfo(FILEINFO_MIME_TYPE);
                    $mime_type = $finfo->buffer($buffer, FILEINFO_MIME_TYPE);
                }

                if ($mime_type !== null && $extension === null) {
                    $extension = ffMedia::getExtensionByMimeType($mime_type);
                    if ($extension === null) {
                        $mime_type = null;
                    }
                }

                if ($mime_type === null) {
                    $mime_type = "text/html";
                    $extension = "html";
                }
                
                if ($mime_type == "text/html" && $charset === null) {
                    $mime_type .= "; charset=UTF-8";
                    $hsent = false;
                }

                if (!$hsent) {
                    header("Content-type: " . $mime_type);
                }
                
                // make & save cache
                $now = time();
                
                $id = uniqid();
                $etag = $id . "." . $extension;
                $file = $id . "." . $extension;

                if (!CM_PAGECACHE_USE_STRONG_CACHE) {
                    $file .= "." . $max_age;
                    $etag .= "." . $max_age;
                }
                
                $compression = false;
                if ($this->oPage->compress && ffHTTP_encoding_isset("gzip")) {
                    $compression = true;
                }
                
                $rc_cache = true;

                // when making main dir fail, don't do anything at all
                if ($rc_cache && !is_dir($cache_dir)) {
                    $rc_cache = @mkdir($cache_dir, 0777, true);
                }
                
                // write it uncompressed
                if ($rc_cache && $find_cache_file["uncompressed"] === null && (!$compression || CM_PAGECACHE_WRITEALL)) {
                    if (CM_PAGECACHE_GROUPDIRS) {
                        $cache_group_dir = 0;
                        $rc_cache = cm_filecache_groupwrite(CM_PAGECACHE_DIR, $cache_dir, $this->path_info, $file, $buffer, ($now + $expires), CM_PAGECACHE_MAXGROUPDIRS, $cache_group_dir, $cache_disk_fail);
                    } else {
                        $rc_cache = cm_filecache_write($cache_dir . $this->path_info, $file, $buffer, ($now + $expires));
                    }
                    
                    /*do
                    {
                        $cache_group_dir++;

                        if (!is_dir($cache_dir . "/" . $cache_group_dir))
                            $cache_new_groupdir = true;
                        else
                            $cache_new_groupdir = false;

                        if (!is_dir($cache_dir . "/" . $cache_group_dir . $cm->path_info))
                            $rc_cache = @mkdir($cache_dir . "/" . $cache_group_dir . $cm->path_info, 0777, true);
                        if ($rc_cache)
                        {
                            $rc_cache = @file_put_contents($cache_dir . "/" . $cache_group_dir . $cm->path_info . "/" . $file, $buffer, LOCK_EX);
                            if (!$rc_cache && $cache_new_groupdir)
                                $cache_disk_fail = true;
                        }
                        else if ($cache_new_groupdir)
                            $cache_disk_fail = true;

                    } while (!$rc_cache && !$cache_disk_fail && $cache_group_dir < CM_PAGECACHE_MAXGROUPDIRS);

                    if ($rc_cache)
                    {
                        $rc_cache = @touch($cache_dir . "/" . $cache_group_dir . $cm->path_info . "/" . $file, $now + $expires);
                        if (!$rc_cache)
                            @unlink($cache_dir . "/" . $cache_group_dir . $cm->path_info . "/" . $file);
                    }
                    else if ($cache_group_dir == CM_PAGECACHE_MAXGROUPDIRS)
                    {
                        @touch($cache_dir . "/maxgroup_limit_reached");
                        if ($cache_dir != CM_PAGECACHE_DIR)
                            @touch(CM_PAGECACHE_DIR . "/maxgroup_limit_reached");
                    }*/
                }

                // manage compressions
                if ($rc_cache && ($compression || ($find_cache_file["compressed"] === null && CM_PAGECACHE_WRITEALL))) {
                    $ret = ffTemplate::http_compress($buffer, false, "gzip");
                    if ($rc_cache && $find_cache_file["compressed"] === null) {
                        if (CM_PAGECACHE_GROUPDIRS) {
                            if ($cache_group_dir > 0) {
                                $cache_group_dir--;
                            } // reuse uncompressed' one
                            else {
                                $cache_group_dir = 0;
                            }
                            
                            $rc_cache = cm_filecache_groupwrite(CM_PAGECACHE_DIR, $cache_dir, $this->path_info, $file . "." . $ret["method"], $ret["data"], ($now + $expires), CM_PAGECACHE_MAXGROUPDIRS, $cache_group_dir, $cache_disk_fail);
                        } else {
                            $rc_cache = cm_filecache_write($cache_dir . $this->path_info, $file . "." . $ret["method"], $ret["data"], ($now + $expires));
                        }
                        
                        /*
                        do
                        {
                            $cache_group_dir++;

                            if (!is_dir($cache_dir . "/" . $cache_group_dir))
                                $cache_new_groupdir = true;
                            else
                                $cache_new_groupdir = false;

                            if (!is_dir($cache_dir . "/" . $cache_group_dir . $cm->path_info))
                                $rc_cache = @mkdir($cache_dir . "/" . $cache_group_dir . $cm->path_info, 0777, true);
                            if ($rc_cache)
                            {
                                $rc_cache = @file_put_contents($cache_dir . "/" . $cache_group_dir . $cm->path_info . "/" . $file . "." . $ret["method"], $ret["data"], LOCK_EX);
                                if (!$rc_cache && $cache_new_groupdir)
                                    $cache_disk_fail = true;
                            }
                            else if ($cache_new_groupdir)
                                $cache_disk_fail = true;

                        } while (!$rc_cache && !$cache_disk_fail && $cache_group_dir < CM_PAGECACHE_MAXGROUPDIRS);

                        if ($rc_cache)
                        {
                            $rc_cache = @touch($cache_dir . "/" . $cache_group_dir . $cm->path_info . "/" . $file . "." . $ret["method"], $now + $expires);
                            if (!$rc_cache)
                                @unlink($cache_dir . "/" . $cache_group_dir . $cm->path_info . "/" . $file . "." . $ret["method"]);
                        }
                        else if ($cache_group_dir == CM_PAGECACHE_MAXGROUPDIRS)
                        {
                            @touch($cache_dir . "/maxgroup_limit_reached");
                            if ($cache_dir != CM_PAGECACHE_DIR)
                                @touch(CM_PAGECACHE_DIR . "/maxgroup_limit_reached");
                        }*/
                    }
                    
                    if ($compression) {
                        $buffer = $ret["data"];
                        
                        if ($rc_cache) {
                            $file .= ".gzip"; // just to align, not really used
                            $etag .= ".gzip";
                        }
                    }
                }
                
                if ($rc_cache) {
                    $this->doEvent("on_cache_write", array(&$this, $now, $compression));

                    if (CM_PAGECACHE_USE_STRONG_CACHE) {
                        $exp_gmt = gmdate("D, d M Y H:i:s", $now + $expires) . " GMT";
                        header("Expires: $exp_gmt");
                    } else {
                        //$mod_gmt = gmdate("D, d M Y H:i:s", $now) . " GMT";
                        header("Cache-Control: public, max-age=" . $max_age); // public: to let firefox cache over https
                    }
                    
                    header("ETag: " . $etag);
                }

                if ($compression !== false) {
                    header("Content-encoding: gzip");
                }

                echo $buffer;
                exit;
            }
        }
        
        $buffer = null;

        if (CM_MIME_FORCE && !$this->isXHR()) {
            $mime = null;
            $charset = null;
        
            $hsent = ffHTTP_getHeader();
            if ($hsent !== false) {
                $mime = $hsent["value"];
                if (array_key_exists("opt_name", $hsent) && $hsent["opt_name"] == "charset") {
                    $charset = $hsent["opt_value"];
                }

                if (!strlen($mime)) {
                    $mime = null;
                }
                if (!strlen($charset)) {
                    $charset = null;
                }
            }

            if ($mime === null && CM_MIME_FINFO /*&& class_exists("finfo")*/) {
                $buffer = $this->oPage->process(false);
                $finfo = new finfo(FILEINFO_MIME_TYPE);
                $mime_type = $finfo->buffer($buffer, FILEINFO_MIME_TYPE);

                if (!strlen($mime_type)) {
                    $mime_type = null;
                }
            }

            if ($mime === null) {
                $mime = $this->default_mime;
            }

            if ($charset === null) {
                $charset = $this->default_charset;
            }

            $header = "Content-type: " . $mime_type . "; charset=" . $charset;
            header($header);
        }

        if ($buffer === null) {
            $this->oPage->process();
        } else {
            echo $buffer;
        }

        if (isset($_REQUEST["__query__"])) {
            ffErrorHandler::raise("QUERY", E_USER_ERROR, null, ffDB_Sql::$_objProfile);
        }

        if (isset($_REQUEST["__debug__"])) {
            ffErrorHandler::raise("DEBUG CM Process End", E_USER_ERROR, $this, get_defined_vars());
        }
    }

    public static function oPage_on_page_process()
    {
        $cm = cm::getInstance();

        if ($cm->tpl_content !== null) {
            $cm->oPage->addContent($cm->tpl_content);
        }

        $cm->preloadApplets($cm->oPage->tpl[0]);
        $cm->preloadApplets($cm->oPage->tpl_layer[0]);

        foreach ($cm->oPage->sections as $key => $value) {
            $cm->preloadApplets($cm->oPage->sections[$key]["tpl"]);
        }

        foreach ($cm->oPage->components as $key => $value) {
            $cm->preloadApplets($cm->oPage->components[$key]->tpl[0]);
        }

        foreach ($cm->oPage->contents as $key => $content) {
            if ($content["data"]) {
                $ref =& $content["data"];
            } else {
                $ref =& $content;
            }
            if (is_object($ref) && get_class($ref) == "ffTemplate") {
                $cm->preloadApplets($ref);
            } elseif (is_string($ref)) {
                $cm->preloadAppletsContent($ref);
            }
        }
    }
    public static function oPage_on_page_processed($oPage)
    {
        $cm = cm::getInstance();

        if (is_array($cm->loaded_applets) && count($cm->loaded_applets)) {
            $oPage->output_buffer["html"] = $cm->parseAppletsContent($oPage->output_buffer["html"]);
        }
    }

    /*static function oPage_on_process_parts($oPage, $tpl)
    {
        if (is_array($tpl))
            $tpl = $tpl[0];

        cm::getInstance()->preloadApplets($tpl);
    }

    static function oPage_on_after_process_components($oPage)
    {
        $cm = cm::getInstance();

        $cm->parseApplets($oPage->tpl);
        $cm->parseApplets($oPage->tpl_layer);

        //ffErrorHandler::raise("asd", E_USER_ERROR, null, get_defined_vars());

        foreach ($cm->oPage->sections as $key => $value)
        {
            $cm->parseApplets($cm->oPage->sections[$key]["tpl"]);
        }
        reset($cm->oPage->sections);

        if ($cm->tpl_content !== null)
        {
            $cm->parseApplets(array($cm->tpl_content));

            $oPage->addContent($cm->tpl_content->rpparse("main", false), null, $cm->tpl_content->sTemplate);
        }
    }*/
    
    public static function onRedirect($destination, $http_response_code, $add_params, $response = array())
    {
        $cm = cm::getInstance();
        if ($cm->isXHR()) {
            $out = array_merge($response, $cm->json_response);
            $out["url"] = $destination;
            $out["close"] = false;
            
            cm::jsonParse($out);
            exit;
        }
    }

    public function preloadApplets($tpl)
    {
        if ($tpl) {
            if (is_object($tpl) && get_class($tpl) == "ffTemplate") {
                $applets = $tpl->getDApplets();

                if (is_array($applets) && count($applets)) {
                    foreach ($applets as $appletid => $applet) {
                        if (!isset($this->loaded_applets[$appletid])) {
                            $this->includeApplet($applet["name"], $applet["params"], $appletid);
                        }
                    }
                }
            } else {
                ffErrorHandler::raise("Preload Applets: Missing ffTemplate", E_USER_ERROR, null, get_defined_vars());
            }
        }
        /*
                if (is_array($tpl->DVars) && count($tpl->DVars))
                {
                    foreach ($tpl->DVars as $key => $ignore)
                    {
                        if ($tmp = preg_match('/\[([\w\:\=\|\-]+)\]/U', $key, $matches))
                        {
                            $applet_parts = explode(":", $matches[1]);
        
                            $applet = $applet_parts[0];
        
                            $params = array();
                            $order_params = array();
                            if (count($applet_parts) > 1)
                            {
                                for ($i = 1; $i < count($applet_parts); $i++)
                                {
                                    $params_parts = explode("=", $applet_parts[$i]);
                                    if (count($params_parts) == 2)
                                    {
                                        $params[$params_parts[0]] = $params_parts[1];
                                        $order_params[] = array("index" => $params_parts[0], "counter" => count($params) - 1, "value" => $params_parts[1]);
                                    }
                                }
                            }
        
                            $rc = usort($order_params, "ffCommon_IndexOrder");
                            if (!$rc)
                                ffErrorHandler::raise("UNABLE TO SORT", E_USER_ERROR, null, get_defined_vars());
        
                            $ordered_params = "";
                            foreach ($order_params as $subkey => $subvalue)
                            {
                                $ordered_params .= ":" . $subvalue["index"] . "=" . $subvalue["value"];
                            }
        
                            $appletid = "[" . $applet . $ordered_params . "]";
        
                            foreach ($tpl->DBlocks as $subkey => $subvalue)
                            {
                                $tpl->DBlocks[$subkey] = str_replace("{" . $key . "}", "{" . $appletid . "}", $tpl->DBlocks[$subkey]);
                            }
                            reset($tpl->DBlocks);
        
                            unset($tpl->DVars[$key]);// = $appletid;
                            $tpl->DVars[$appletid] = "changed by PreloadApplets";
        
                            if (!isset($this->loaded_applets[$appletid]))
                                $this->includeApplet($applet, $params, $appletid);
                        }
                    }
                    reset($tpl->DVars);
                }*/
    }

    public function preloadAppletsContent($content)
    {
        $regexp = '/\{\[(.+)\]\}/U';

        $tmp = preg_match_all($regexp, $content, $matches);
        if (is_array($matches[1]) && count($matches[1])) {
            foreach ($matches[1] as $key => $value) {
                $applet_parts = explode(":", $value);

                $applet = $applet_parts[0];

                $params = array();
                $order_params = array();
                if (count($applet_parts) > 1) {
                    for ($i = 1; $i < count($applet_parts); $i++) {
                        $params_parts = explode("=", $applet_parts[$i]);
                        if (count($params_parts) == 2) {
                            $params[$params_parts[0]] = $params_parts[1];
                            $order_params[] = array("index" => $params_parts[0], "counter" => count($params) - 1, "value" => $params_parts[1]);
                        }
                    }
                }

                $rc = usort($order_params, "ffCommon_IndexOrder");
                if (!$rc) {
                    ffErrorHandler::raise("UNABLE TO SORT", E_USER_ERROR, null, get_defined_vars());
                }

                $ordered_params = "";
                foreach ($order_params as $subkey => $subvalue) {
                    $ordered_params .= ":" . $subvalue["index"] . "=" . $subvalue["value"];
                }

                $appletid = "[" . $applet . $ordered_params . "]";

                $content = str_replace("{[" . $value . "]}", "{" . $appletid . "}", $content);

                if (!isset($this->loaded_applets[$appletid])) {
                    $this->includeApplet($applet, $params, $appletid);
                }
            }
            reset($matches);
        }

        return $content;
    }

    public function parseApplets($tpl)
    {
        if (is_array($tpl)) {
            $tpl = $tpl[0];
        }

        $this->preloadApplets($tpl);
        $tpl->DBlocks[$tpl->root_element] = $this->parseAppletsContent($tpl->DBlocks[$tpl->root_element]);
    }

    public function parseAppletsContent($content)
    {
        if (is_array($this->loaded_applets) && count($this->loaded_applets)) {
            foreach ($this->loaded_applets as $key => $value) {
                if (is_array($value["buffer"])) {
                    $html = $value["buffer"]["html"];
                    if ($value["buffer"]["js"]) {
                        $this->oPage->tplAddJs($key, array(
                            "embed" => $value["buffer"]["js"]
                        ));
                    }
                    if ($value["buffer"]["css"]) {
                        $this->oPage->tplAddCss($key, array(
                            "embed" => $value["buffer"]["css"]
                        ));
                    }
                } else {
                    $html = $value["buffer"];
                }


                $content = str_replace("{" . $key . "}", $html, $content);
            }
        }
        return $content;
    }

    public function includeApplet($appletname, $applet_params, $appletid)
    {
        $out_buffer = "";

        $res = $this->doEvent("on_before_include_applet", array(&$this, $appletname, $applet_params, $appletid));
        $applet_file = end($res);
        if (!$applet_file) {
            //echo "$appletname, $applet_params, $appletid<br />";
            $this->loaded_applets[$appletid]["params"] = $applet_params;

            $appletname_parts = explode("|", $appletname);
            if (count($appletname_parts) == 2) {
                $this->loaded_applets[$appletid]["module"] = $appletname_parts[0];
                $this->loaded_applets[$appletid]["name"] = $appletname_parts[1];
                $applet_file = CM_MODULES_ROOT . "/" . $appletname_parts[0] . "/applets/" . $appletname_parts[1] . "/index." . FF_PHP_EXT;
            } else {
                $this->loaded_applets[$appletid]["name"] = $appletname;
                $applet_file = FF_DISK_PATH . "/applets/" . $appletname . "/index." . FF_PHP_EXT;
            }
        }

        $cm = $this;
        if (file_exists($applet_file)) {
            include $applet_file;
        } else {
            ffErrorHandler::raise("APPLET NON TROVATA", E_USER_ERROR, $this, get_defined_vars());
        }

        $this->loaded_applets[$appletid]["buffer"] = $out_buffer;
        
        $this->oPage->process_params();
        return $out_buffer;
    }

    public function addApplet($file, $name = null, $params = null, $module = null, $id = null)
    {
        $out_buffer = "";
        if (!$name) {
            $name = ffGetFilename($file);
        }

        if (!$id) {
            $ordered_params = "";
            if (is_array($params) && count($params)) {
                foreach ($params as $key => $value) {
                    $ordered_params .= ":" . $key . "=" . $value;
                }
            }

            $id = "[" . $name . $ordered_params . "]";
        }
        if (!$this->loaded_applets[$id]) {
            $this->loaded_applets[$id]["params"] = $params;
            $this->loaded_applets[$id]["module"] = $module;
            $this->loaded_applets[$id]["name"] = $name;
            if (file_exists($file)) {
                include $file;
            } else {
                ffErrorHandler::raise("APPLET NON TROVATA", E_USER_ERROR, $this, get_defined_vars());
            }

            $this->loaded_applets[$id]["buffer"] = $out_buffer;

            $this->oPage->process_params();
        }
        return $this->loaded_applets[$id]["buffer"];
    }
    public function callScript($file)
    {
        $ff = ffGlobals::getInstance("ff");
        $cm = $this;

        require $file;
        
        $this->doEvent("on_callScript", array(&$this, $this->script_name));
    }
    
    public function load_ffSettingsByPath($path_info = null)
    {
        if ($path_info === null) {
            $path_info = $this->path_info;
        }
        
        $file = rtrim(FF_DISK_PATH . "/conf/contents" . $path_info, "/") . "/ff_settings.xml";
        if (!is_file($file)) {
            return false;
        }
        
        $this->load_ffSettings($file);
    }

    public function load_ffSettings($file)
    {
        //if (isset($this->ff_settings_loaded[$file]))
        //	return true;

        //$this->ff_settings_loaded[$file] = true;

        $xml = new SimpleXMLElement("file://" . $file, null, true);

        if (isset($xml->ffPage) && count($xml->ffPage->children())) {
            foreach ($xml->ffPage->children() as $key => $value) {
                switch ($key) {
                    case "option":
                        $attrs	= $value->attributes();
                        $name	= (string)$attrs["name"];
                        $mode	= (string)$attrs["mode"];
                        if (isset($this->oPage->$name)) {
                            if ($mode) {
                                $this->ff_settings_merge($this->oPage->$name, $this->ff_settings_process_value($value));
                            } else {
                                $this->oPage->$name = $this->ff_settings_process_value($value);
                            }
                        } else {
                            ffErrorHandler::raise("Wrong ffPage option", E_USER_ERROR, $this, get_defined_vars());
                        }
                        break;
                    
                    case "event":
                        $attrs	= $value->attributes();

                        $event	= $this->ff_settings_process_event($attrs, $value);
                        $this->oPage->addEvent($event["event_name"], $event["func_name"], $event["priority"], $event["index"], $event["break_when"], $event["break_value"], $event["additional_data"]);
                        break;

                    case "ffGrid":
                    case "ffRecord":
                    case "ffDetails":
                        $attrs	= $value->attributes();
                        $id		= (string)$attrs["id"];
                        if (!isset($this->oPage->components[$id])) {
                            continue;
                        }
                            
                        foreach ($value as $subkey => $subvalue) {
                            switch ($subkey) {
                                case "event":
                                    $attrs	= $subvalue->attributes();

                                    $event	= $this->ff_settings_process_event($attrs, $subvalue);
                                    $this->oPage->components[$id]->addEvent($event["event_name"], $event["func_name"], $event["priority"], $event["index"], $event["break_when"], $event["break_value"], $event["additional_data"]);
                                    break;

                                case "option":
                                    $attrs	= $subvalue->attributes();
                                    $name	= (string)$attrs["name"];
                                    $mode	= (string)$attrs["mode"];
                                    if ($mode) {
                                        $this->ff_settings_merge($this->oPage->components[$id]->$name, $this->ff_settings_process_value($subvalue));
                                    } else {
                                        $this->oPage->components[$id]->$name = $this->ff_settings_process_value($subvalue);
                                    }
                                    break;
                                
                                case "ffField":
                                case "ffButton":
                                    $this->ff_settings_process_element($subkey, $key, $id, $subvalue);
                                    break;
                            }
                        }
                        break;
                    
                    case "ffField":
                    case "ffButton":
                        $this->ff_settings_process_element($key, "ffPage", null, $value);
                        break;
                }
            }
        }
        
        return true;
    }
    
    public function ff_settings_process_element($type, $container_type, $container_id, $element)
    {
        $attrs	= $element->attributes();
        $field_id	= (string)$attrs["id"];
        $field_type	= (string)$attrs["type"];

        $field = null;
        if ($type === "ffButton") {
            switch ($container_type) {
                case "ffPage":
                    $field = $this->oPage->buttons[$field_id];
                    break;
                
                case "ffGrid":
                    if (!array_key_exists($container_id, $this->oPage->components)) {
                        //ffErrorHandler::raise ("ffSettings - Unknown container", E_USER_ERROR, $this, get_defined_vars());
                        return;
                    }
                
                    switch ($field_type) {
                        case "grid":
                            $field = $this->oPage->components[$container_id]->grid_buttons[$field_id];
                            break;
                        case "action":
                            $field = $this->oPage->components[$container_id]->action_buttons[$field_id];
                            break;
                        case "action_header":
                            $field = $this->oPage->components[$container_id]->action_buttons_header[$field_id];
                            break;
                        case "search":
                            $field = $this->oPage->components[$container_id]->search_buttons[$field_id];
                            break;
                    }
                    break;
                case "ffRecord":
                    if (!array_key_exists($container_id, $this->oPage->components)) {
                        //ffErrorHandler::raise ("ffSettings - Unknown container", E_USER_ERROR, $this, get_defined_vars());
                        return;
                    }
                
                    switch ($field_type) {
                        case "action":
                        default:
                            $field = $this->oPage->components[$container_id]->action_buttons[$field_id];
                            break;
                    }
                    break;
                case "ffDetails":
                    if (!array_key_exists($container_id, $this->oPage->components)) {
                        //ffErrorHandler::raise ("ffSettings - Unknown container", E_USER_ERROR, $this, get_defined_vars());
                        return;
                    }
                
                    switch ($field_type) {
                        case "content":
                        default:
                            $field = $this->oPage->components[$container_id]->detail_buttons[$field_id];
                            break;
                    }
                    break;
            }
        } elseif ($type === "ffField") {
            if ($container_type == "ffPage") {
                $field = $this->oPage->fields[$field_id];
            } else {
                if (!array_key_exists($container_id, $this->oPage->components)) {
                    //ffErrorHandler::raise ("ffSettings - Unknown container", E_USER_ERROR, $this, get_defined_vars());
                    return;
                }
                
                switch ($field_type) {
                    case "display":
                        $field = $this->oPage->components[$container_id]->grid_fields[$field_id];
                        break;

                    case "search":
                        $field = $this->oPage->components[$container_id]->search_fields[$field_id];
                        break;

                    case "key":
                        $field = $this->oPage->components[$container_id]->key_fields[$field_id];
                        break;

                    case "form":
                        $field = $this->oPage->components[$container_id]->form_fields[$field_id];
                        break;
                }
            }
        } else {
            ffErrorHandler::raise("ffSettings - UNHANDLED ELEMENT", E_USER_ERROR, $this, get_defined_vars());
        }
        
        if (!$field) {
            //ffErrorHandler::raise ("ffSettings - Unknown element", E_USER_ERROR, $this, get_defined_vars());
            return;
        }

        foreach ($element->children() as $field_key => $field_value) {
            switch ($field_key) {
                case "event":
                    $attrs	= $field_value->attributes();

                    $event	= $this->ff_settings_process_event($attrs, $field_value);
                    $field->addEvent($event["event_name"], $event["func_name"], $event["priority"], $event["index"], $event["break_when"], $event["break_value"], $event["additional_data"]);
                    break;

                case "option":
                    $attrs	= $field_value->attributes();
                    $name = (string)$attrs["name"];
                    $mode = (string)$attrs["mode"];
                    if ($mode) {
                        $this->ff_settings_merge($field->$name, $this->ff_settings_process_value($field_value));
                    } else {
                        $field->$name = $this->ff_settings_process_value($field_value);
                    }
                    break;
                    
                case "array":
                    $arr_key = (string)$field_value->arr_key;
                    
                    if (count($field_value->arr_value->children())) { // suboptions
                        foreach ($field_value->arr_value->children() as $key_opt => $item_opt) {
                            if ($key_opt == "option") {
                                $attrs	= $item_opt->attributes();
                                $name = (string)$attrs["name"];
                                $mode = (string)$attrs["mode"];
                                if ($mode) {
                                    $this->ff_settings_merge($field[$arr_key]->$name, $this->ff_settings_process_value($item_opt));
                                } else {
                                    $field[$arr_key]->$name = $this->ff_settings_process_value($item_opt);
                                }
                            } else {
                                ffErrorHandler::raise("Wrong ff_settings.xml format", E_USER_ERROR, $this, get_defined_vars());
                            }
                        }
                    } else {
                        $field[$arr_key] = $this->ff_settings_process_value($field_value->arr_value);
                    }
                    break;

                default:
                    ffErrorHandler::raise("Wrong ff_settings.xml format", E_USER_ERROR, $this, get_defined_vars());
            }
        }
    }

    public function ff_settings_merge(&$ori, $new)
    {
        if (is_string($new)) {
            $ori .= $new;
            return;
        }
        
        if (!is_array($new)) {
            $ori = $new;
            return;
        }

        foreach ($new as $key => $value) {
            if (!isset($ori[$key])) {
                $ori[$key] = $value;
            } else {
                $this->ff_settings_merge($ori[$key], $new[$key]);
            } // $this->ff_settings_merge(&$ori[$key], $new[$key]);
        }
        reset($ori);
    }

    public function ff_settings_process_event($attrs, $element)
    {
        $event_name			= (string)$attrs["event_name"];
        $func_name			= (string)$attrs["func_name"];

        $priority			= (string)$attrs["priority"];
        switch ($priority) {
            case "PRIORITY_TOPLEVEL":
                $priority = ffEvent::PRIORITY_TOPLEVEL;
                break;

            case "PRIORITY_HIGH":
                $priority = ffEvent::PRIORITY_HIGH;
                break;

            case "PRIORITY_NORMAL":
                $priority = ffEvent::PRIORITY_NORMAL;
                break;

            case "PRIORITY_LOW":
                $priority = ffEvent::PRIORITY_LOW;
                break;

            case "PRIORITY_FINAL":
                $priority = ffEvent::PRIORITY_FINAL;
                break;

            default:
                $priority = null;
        }

        $index	= (int)$attrs["index"];
        if (!strlen($index)) {
            $index = 0;
        }

        $break_when			= (string)$attrs["break_when"];
        switch ($break_when) {
            case "BREAK_NEVER":
                $break_when = ffEvent::BREAK_NEVER;
                break;

            case "BREAK_EQUAL":
                $break_when = ffEvent::BREAK_EQUAL;
                break;

            case "BREAK_NOT_EQUAL":
                $break_when = ffEvent::BREAK_NOT_EQUAL;
                break;

            case "BREAK_CALLBACK":
                $break_when = ffEvent::BREAK_CALLBACK;
                break;

            default:
                $break_when = null;
        }

        if ($element->break_value) {
            $break_value = $this->ff_settings_process_value($element->break_value);
        } else {
            $break_value = null;
        }

        if ($element->additional_data) {
            $additional_data = $this->ff_settings_process_value($element->additional_data);
        } else {
            $additional_data = null;
        }

        return array(
                "event_name"			=> $event_name
                , "func_name"			=> $func_name
                , "priority"			=> $priority
                , "index"				=> $index
                , "break_when"			=> $break_when
                , "break_value"			=> $break_value
                , "additional_data"		=> $additional_data
            );
    }

    public function ff_settings_process_value($element)
    {
        $value = null;
        
        if (count($element->children())) {
            $value = array();
            foreach ($element->children() as $key => $item) {
                if ($key == "array") {
                    $value[(string)$item->arr_key] = $this->ff_settings_process_value($item->arr_value);
                } elseif ($key == "pair") {
                    $value[] = array($this->ff_settings_process_value($item->value[0]), $this->ff_settings_process_value($item->value[1]));
                } elseif ($key == "value") {
                    $value[] = $this->ff_settings_process_value($item);
                } else {
                    ffErrorHandler::raise("Wrong ff_settings.xml format", E_USER_ERROR, $this, get_defined_vars());
                }
            }
        } else {
            $attrs = $element->attributes();

            if (!isset($attrs["value"])) {
                $value = (string)$element;
                if (!strlen($value)) {
                    $value		= null;
                }
            } else {
                $value		= (string)$attrs["value"];
            }

            $type = (string)$attrs["type"];

            if (!isset($attrs["data_type"])) {
                $data_type	= "Text";
            } else {
                $data_type	= (string)$attrs["data_type"];
            }
                
            if (!isset($attrs["locale"])) {
                $locale		= FF_SYSTEM_LOCALE;
            } elseif ((string)$attrs["locale"] == "FF_LOCALE") {
                $locale		= FF_LOCALE;
            } else {
                $locale		= (string)$attrs["locale"];
            }

            switch ($data_type) {
                case "Boolean":
                    if ($value == "true") {
                        $value = true;
                    } else {
                        $value = false;
                    }
                    break;

                case "Number":
                    $value = (double)$value;
                    break;

                default:
                    break;
            }

            if ($type == "ffData") {
                $value = new ffData($value, $data_type, $locale);
            }
        }
        return $value;
    }

    public function getLayoutByPath($layout_path)
    {
        if (CM_ENABLE_MEM_CACHING && CM_ENABLE_PATH_CACHE) {
            $layout_vars = $this->cache->get($layout_path, "/cm/layout");
            if ($layout_vars) {
                return $layout_vars;
            }
        }

        $db = ffDB_Sql::factory();
        
        $layout_vars = array();
        $layout_vars["main_theme"] = null;
        $layout_vars["theme"] = null;
        $layout_vars["page"] = null;
        $layout_vars["layer"] = null;
        $layout_vars["title"] = null;
        $layout_vars["class_body"] = null;
        $layout_vars["sect"] = array();
        $layout_vars["css"] = array();
        $layout_vars["js"] = array();
        $layout_vars["meta"] = array();
        $layout_vars["cdn"] = array();
        $layout_vars["ignore_defaults"] = false;
        $layout_vars["ignore_defaults_main"] = false;
        $layout_vars["exclude_ff_js"] = null;
        $layout_vars["exclude_form"] = null;
        $layout_vars["enable_gzip"] = false;
        $layout_vars["compact_js"] = false;
        $layout_vars["compact_css"] = false;
        // $layout_vars["framework_css"] = null;
        // $layout_vars["font_icon"] = null;
        
        $tmp = $layout_path;
        $paths = "";
        $i = 0;
        do {
            if ($i > 0 && $tmp != "/") {
                $paths .= " OR path = '" . $db->toSql(new ffData($tmp . "/"), null, false) . "'";
            }

            if (strlen($paths)) {
                $paths .= " OR ";
            }

            $paths .= "path = '" . $db->toSql(new ffData($tmp), null, false) . "'";
            $i++;
        } while ($tmp != "/" && $tmp = ffCommon_dirname($tmp));

        if (defined("CM_MULTIDOMAIN_ROUTING") && CM_MULTIDOMAIN_ROUTING) {
            $sSQL = "SELECT tbl_src.* 
                    FROM
                    (
                        SELECT
                            " . CM_TABLE_PREFIX . "layout.*
                            , IF(" . CM_TABLE_PREFIX . "layout.domains = ''
                                , 0
                                , 1
                            ) AS sort_domains
                        FROM
                            " . CM_TABLE_PREFIX . "layout
                        WHERE 1
                            " . (
                strlen($paths)
                                ? " AND (" . $paths . ") "
                                : ""
                            ) . "
                            AND (" . CM_TABLE_PREFIX . "layout.domains = ''
                                OR FIND_IN_SET(" . $db->toSql($_SERVER["HTTP_HOST"]) . ", " . CM_TABLE_PREFIX . "layout.domains)
                            )
                        ORDER BY
                            sort_domains DESC, path ASC
                    ) AS tbl_src
                    GROUP BY path
                    ORDER BY tbl_src.path ASC";
        } else {
            $sSQL = "SELECT
                        " . CM_TABLE_PREFIX . "layout.*
                    FROM
                        " . CM_TABLE_PREFIX . "layout
                    WHERE 1
                        " . (
                strlen($paths)
                            ? " AND (" . $paths . ") "
                            : ""
                        ) . "
                    ORDER BY path ASC";
        }

        $db->query($sSQL);
        if ($db->nextRecord()) {
            $db2 = ffDB_Sql::factory();
            do {
                if ($db->getField("path", "Text", true) == $layout_path || $db->getField("path", "Text", true) == str_replace("/index", "/", $layout_path)) {
                    $bMatchPath = true;
                } else {
                    $bMatchPath = false;
                }

                if (!$db->getField("enable_cascading", "Text", true) && !$bMatchPath) {
                    continue;
                }
                
                if ($db->getField("reset_cascading", "Text", true)) {
                    $layout_vars = array();
                    $layout_vars["main_theme"] = null;
                    $layout_vars["theme"] = null;
                    $layout_vars["page"] = null;
                    $layout_vars["layer"] = null;
                    $layout_vars["title"] = null;
                    $layout_vars["class_body"] = null;
                    $layout_vars["sect"] = array();
                    $layout_vars["sect_theme"] = array();
                    $layout_vars["css"] = array();
                    $layout_vars["js"] = array();
                    $layout_vars["meta"] = array();
                    $layout_vars["cdn"] = array();
                    $layout_vars["ignore_defaults"] = false;
                    $layout_vars["ignore_defaults_main"] = false;
                    $layout_vars["exclude_ff_js"] = null;
                    $layout_vars["exclude_form"] = null;
                    $layout_vars["enable_gzip"] = false;
                    $layout_vars["compact_js"] = false;
                    $layout_vars["compact_css"] = false;
                    // $layout_vars["framework_css"] = null;
                   // $layout_vars["font_icon"] = null;
                }

                if ($db->getField("ignore_defaults", "Number", true, false)) {
                    $layout_vars["ignore_defaults"] = true;
                }

                if ($db->getField("ignore_defaults_main", "Number", true, false)) {
                    $layout_vars["ignore_defaults_main"] = true;
                }

                if (strlen($db->getField("exclude_ff_js", "Number", true, false))) {
                    $layout_vars["exclude_ff_js"] = $db->getField("exclude_ff_js", "Number", true);
                }

                if (strlen($db->getField("exclude_form", "Number", true, false))) {
                    $layout_vars["exclude_form"] = $db->getField("exclude_form", "Number", true);
                }

                if (strlen($db->getField("enable_gzip", "Number", true, false))) {
                    $layout_vars["enable_gzip"] = $db->getField("enable_gzip", "Number", true);
                }

                if (strlen($db->getField("compact_js", "Number", true, false))) {
                    $layout_vars["compact_js"] = $db->getField("compact_js", "Number", true);
                }

                if (strlen($db->getField("compact_css", "Number", true, false))) {
                    $layout_vars["compact_css"] = $db->getField("compact_css", "Number", true);
                }

                if (strlen($db->getField("main_theme", "Text", true))) {
                    $layout_vars["main_theme"] = $db->getField("main_theme", "Text", true);
                }

                if (strlen($db->getField("theme", "Text", true))) {
                    $layout_vars["theme"] = $db->getField("theme", "Text", true);
                }

                if (strlen($db->getField("page", "Text", true))) {
                    $layout_vars["page"] = $db->getField("page", "Text", true);
                }

                if (strlen($db->getField("layer", "Text", true))) {
                    $layout_vars["layer"] = $db->getField("layer", "Text", true);
                }

                if (strlen($db->getField("title", "Text", true))) {
                    $layout_vars["title"] = $db->getField("title", "Text", true);
                }

                if (strlen($db->getField("class_body", "Text", true))) {
                    $layout_vars["class_body"] = $db->getField("class_body", "Text", true);
                }

                /*if (strlen($db->getField("framework_css", "Text", true, false)))
                    $layout_vars["framework_css"] = $db->getField("framework_css", "Text", true);

                if (strlen($db->getField("font_icon", "Text", true, false)))
                    $layout_vars["font_icon"] = $db->getField("font_icon", "Text", true);*/

                $sSQL = "SELECT * FROM " . CM_TABLE_PREFIX . "layout_sect WHERE ID_layout = " . $db2->toSql($db->getField("ID")) . " ORDER BY ID";
                
                $db2->query($sSQL);
                if ($db2->nextRecord()) {
                    do {
                        if (!$db2->getField("cascading", "Text", true) && !$bMatchPath) {
                            continue;
                        }

                        $layout_vars["sect"][$db2->getField("name", "Text", true)] = $db2->getField("value", "Text", true);
                        $layout_vars["sect_theme"][$db2->getField("name", "Text", true)] = $db2->getField("theme_include", "Text", true);
                    } while ($db2->nextRecord());
                }

                $sSQL = "SELECT * FROM " . CM_TABLE_PREFIX . "layout_css WHERE ID_layout = " . $db2->toSql($db->getField("ID")) . " ORDER BY ID";
                $db2->query($sSQL);
                if ($db2->nextRecord()) {
                    do {
                        if (!$db2->getField("cascading", "Text", true) && !$bMatchPath) {
                            continue;
                        }

                        if (!strlen($db2->getField("priority", "Text", true))) {
                            $priority = "top";
                        } else {
                            $priority = $db2->getField("priority", "Text", true);
                        }

                        $layout_vars["css"][$db2->getField("name", "Text", true)]["path"] = ($db2->getField("path", "Text", true) ? $db2->getField("path", "Text", true) : null);
                        $layout_vars["css"][$db2->getField("name", "Text", true)]["file"] = $db2->getField("file", "Text", true);
                        $layout_vars["css"][$db2->getField("name", "Text", true)]["exclude_compact"] =  $db2->getField("exclude_compact", "Text", true);
                        $layout_vars["css"][$db2->getField("name", "Text", true)]["priority"] = $priority;
                    } while ($db2->nextRecord());
                }

                $sSQL = "SELECT * FROM " . CM_TABLE_PREFIX . "layout_js WHERE ID_layout = " . $db2->toSql($db->getField("ID", "Number")) . " ORDER BY ID";
                $db2->query($sSQL);
                if ($db2->nextRecord()) {
                    do {
                        if (!$db2->getField("cascading", "Text", true) && !$bMatchPath) {
                            continue;
                        }

                        if (!strlen($db2->getField("priority", "Text", true))) {
                            $priority = "top";
                        } else {
                            $priority = $db2->getField("priority", "Text", true);
                        }

                        if (strlen($db2->getField("plugin_path", "Text", true))) {
                            if (file_exists(FF_DISK_PATH . $db2->getField("plugin_path", "Text", true))) {
                                $layout_vars["js"][basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true)))]["path"] = ffCommon_dirname($db2->getField("plugin_path", "Text", true));
                                $layout_vars["js"][basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true)))]["file"] = basename($db2->getField("plugin_path", "Text", true));
                                $layout_vars["js"][basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true)))]["exclude_compact"] = $db2->getField("exclude_compact", "Text", true);
                                $layout_vars["js"][basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true)))]["priority"] = $priority;
                            }
                            if (strlen($db2->getField("js_path", "Text", true))) {
                                $layout_vars["js"][basename($db2->getField("js_path", "Text", true))]["path"] = "/themes/" . $layout_vars["theme"] . "/javascript" . ffCommon_dirname($db2->getField("js_path", "Text", true));
                                $layout_vars["js"][basename($db2->getField("js_path", "Text", true))]["file"] = basename($db2->getField("js_path", "Text", true));
                                $layout_vars["js"][basename($db2->getField("js_path", "Text", true))]["exclude_compact"] = $db2->getField("exclude_compact", "Text", true);
                                $layout_vars["js"][basename($db2->getField("js_path", "Text", true))]["priority"] = $priority;
                            } else {
                                if (file_exists(FF_DISK_PATH . "/themes/" . $layout_vars["theme"] . "/javascript/" . basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true))) . ".observe.js")) {
                                    $layout_vars["js"][basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true))) . ".observe"]["path"] = "/themes/" . $layout_vars["theme"] . "/javascript";
                                    $layout_vars["js"][basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true))) . ".observe"]["file"] = basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true))) . ".observe.js";
                                    $layout_vars["js"][basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true))) . ".observe"]["exclude_compact"] = $db2->getField("exclude_compact", "Text", true);
                                    $layout_vars["js"][basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true))) . ".observe"]["priority"] = $priority;
                                } elseif (file_exists(FF_DISK_PATH . ffCommon_dirname($db2->getField("plugin_path", "Text", true)) . "/" . basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true))) . ".observe.js")) {
                                    $layout_vars["js"][basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true))) . ".observe"]["path"] = ffCommon_dirname($db2->getField("plugin_path", "Text", true));
                                    $layout_vars["js"][basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true))) . ".observe"]["file"] = basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true))) . ".observe.js";
                                    $layout_vars["js"][basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true))) . ".observe"]["exclude_compact"] = $db2->getField("exclude_compact", "Text", true);
                                    $layout_vars["js"][basename(ffCommon_dirname($db2->getField("plugin_path", "Text", true))) . ".observe"]["priority"] = $priority;
                                }
                            }
                        } elseif (strlen($db2->getField("js_path", "Text", true))) {
                            $layout_vars["js"][basename($db2->getField("js_path", "Text", true))]["path"] = "/themes/" . $layout_vars["theme"] . "/javascript" . ffCommon_dirname($db2->getField("js_path", "Text", true));
                            $layout_vars["js"][basename($db2->getField("js_path", "Text", true))]["file"] = basename($db2->getField("js_path", "Text", true));
                            $layout_vars["js"][basename($db2->getField("js_path", "Text", true))]["theme"] = $db2->getField("theme_include", "Text", true);
                            $layout_vars["js"][basename($db2->getField("js_path", "Text", true))]["exclude_compact"] = $db2->getField("exclude_compact", "Text", true);
                            $layout_vars["js"][basename($db2->getField("js_path", "Text", true))]["priority"] = $priority;
                        } elseif (strlen($db2->getField("file", "Text", true))) {
                            $layout_vars["js"][$db2->getField("name", "Text", true)]["path"]	= ($db2->getField("path", "Text", true) ? $db2->getField("path", "Text", true) : null);
                            $layout_vars["js"][$db2->getField("name", "Text", true)]["file"]	= $db2->getField("file", "Text", true);
                            $layout_vars["js"][$db2->getField("name", "Text", true)]["theme"]	= $db2->getField("theme_include", "Text", true);
                            $layout_vars["js"][$db2->getField("name", "Text", true)]["exclude_compact"] = $db2->getField("exclude_compact", "Text", true);
                            $layout_vars["js"][$db2->getField("name", "Text", true)]["priority"] = $priority;
                        }
                    } while ($db2->nextRecord());
                }

                $sSQL = "SELECT * FROM " . CM_TABLE_PREFIX . "layout_meta WHERE ID_layout = " . $db2->toSql($db->getField("ID")) . " ORDER BY ID";
                $db2->query($sSQL);
                if ($db2->nextRecord()) {
                    do {
                        if (!$db2->getField("cascading", "Text", true) && !$bMatchPath) {
                            continue;
                        }

                        $layout_vars["meta"][$db2->getField("name", "Text", true)]["type"] = (
                            strlen($db2->getField("type", "Text", true))
                                                                                                ? $db2->getField("type", "Text", true)
                                                                                                : "name"
                                                                                            );
                        $layout_vars["meta"][$db2->getField("name", "Text", true)]["content"] = $db2->getField("content", "Text", true);
                    } while ($db2->nextRecord());
                }

                $sSQL = "SELECT * FROM " . CM_TABLE_PREFIX . "layout_cdn WHERE ID_layout = " . $db2->toSql($db->getField("ID")) . " ORDER BY ID";
                $db2->query($sSQL);
                if ($db2->nextRecord()) {
                    do {
                        if (!$db2->getField("status", "Text", true) && !$bMatchPath) {
                            continue;
                        }

                        $layout_vars["cdn"][$db2->getField("type", "Text", true)][$db2->getField("name", "Text", true)] = $db2->getField("url", "Text", true);
                    } while ($db2->nextRecord());
                }
            } while ($db->nextRecord());
        }

        if (CM_ENABLE_MEM_CACHING && CM_ENABLE_PATH_CACHE) {
            $this->cache->set($layout_path, $layout_vars, "/cm/layout");
        }

        return $layout_vars;
    }

    public function isXHR()
    {
        if ($_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest") {
            return true;
        } else {
            return false;
        }
    }
    
    public static function getJSONP()
    {
        if (isset($_REQUEST["XHR_JSONP"])) {
            return $_REQUEST["XHR_JSONP"];
        } else {
            return false;
        }
    }

    public function jsonAddResponse($data)
    {
        $this->json_response = array_replace_recursive($this->json_response, $data);
    }
    
    public static function jsonParse($arData, $out = true, $add_newline = false, $standard_encode = false, $standard_opts = 0, $skip_event = false)
    {
        if (!$skip_event) {
            cm::_doEvent("jsonParse", array(&$arData, &$out, &$add_newline, &$standard_encode, &$standard_opts));
        }
                
        if ($jsonp = cm::getJSONP()) {
            $jsonp_pre = $jsonp . "(";
            $jsonp_post = ")";
            
            if ($out) {
                header("Content-type: application/javascript; charset=utf-8");
            }
        } elseif ($out) {
            header("Content-type: application/json; charset=utf-8");
        }
        
        if ($standard_encode) {
            if ($out) {
                echo $jsonp_pre . json_encode($arData, $standard_opts) . $jsonp_post;
            } else {
                return $jsonp_pre . json_encode($arData, $standard_opts) . $jsonp_post;
            }
        } else {
            if ($out) {
                echo $jsonp_pre . ffCommon_jsonenc($arData, false, $add_newline) . $jsonp_post;
            } else {
                return $jsonp_pre . ffCommon_jsonenc($arData, false, $add_newline) . $jsonp_post;
            }
        }
    }
    
    public function getBrowser()
    {
        $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $bname = 'Unknown';
        $platform = 'Unknown';
        $version= "";

        //First get the platform?
        if (preg_match('/linux/i', $u_agent)) {
            $platform = 'linux';
        } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $u_agent)) {
            $platform = 'windows';
        }
       
        // Next get the name of the useragent yes seperately and for good reason
        if (preg_match('/iPad/i', $u_agent)) {
            $bname = 'Ipad';
            $ub = "Ipad";
        } elseif (preg_match('/iPhone/i', $u_agent)) {
            $bname = 'iPhone';
            $ub = "iPhone";
        } elseif (preg_match('/iPod/i', $u_agent)) {
            $bname = 'Ipod';
            $ub = "Ipod";
        } elseif (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
            $bname = 'Internet Explorer';
            $ub = "MSIE";
        } elseif (preg_match('/Firefox/i', $u_agent)) {
            $bname = 'Mozilla Firefox';
            $ub = "Firefox";
        } elseif (preg_match('/Chrome/i', $u_agent)) {
            $bname = 'Google Chrome';
            $ub = "Chrome";
        } elseif (preg_match('/Safari/i', $u_agent)) {
            $bname = 'Apple Safari';
            $ub = "Safari";
        } elseif (preg_match('/Opera/i', $u_agent)) {
            $bname = 'Opera';
            $ub = "Opera";
        } elseif (preg_match('/Netscape/i', $u_agent)) {
            $bname = 'Netscape';
            $ub = "Netscape";
        }

        
        // finally get the correct version number
        $known = array('Version', $ub, 'other');
        $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $u_agent, $matches)) {
            // we have no matching number just continue
        }
       
        // see how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            //we will have two since we are not using 'other' argument yet
            //see if version is before or after the name
            if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
                $version= $matches['version'][0];
            } else {
                $version= $matches['version'][1];
            }
        } else {
            $version= $matches['version'][0];
        }
       
        // check if we have a number
        if ($version==null || $version=="") {
            $version="?";
        }
       
        return array(
            'userAgent' 	=> $u_agent
            , 'extendname'  => $bname
            , 'name'		=> $ub
            , 'majorver'   	=> (strpos($version, ".") === false ? $version : substr($version, 0, strpos($version, ".")))
            , 'lowerver'   	=> (strpos($version, ".") === false ? $version : substr($version, strpos($version, ".") + 1))
            , 'platform'  	=> $platform
            , 'pattern'    	=> $pattern
        );
    }
    
    public function responseCode($code)
    {
        $res = $this->doEvent("on_responseCode", array($this, $code));
        $rc = end($res);
        if ($rc === null) {
            $tpl = null;
            
            if (is_file(FF_DISK_PATH . "/conf/cm/extras/" . $code . ".html")) {
                $tpl = ffTemplate::factory(FF_DISK_PATH . "/conf/cm/extras");
            } elseif (is_file(FF_DISK_PATH . "/cm/extras/" . $code . ".html")) {
                $tpl = ffTemplate::factory(FF_DISK_PATH . "/cm/extras");
            }
            
            if ($tpl !== null) {
                $tpl->load_file($code . ".html", "main");
                $tpl->pparse("main", false);
            }
            
            http_response_code($code);
            exit;
        }
    }
}
