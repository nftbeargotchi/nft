<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"nivoslider" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.nivoslider",
							"file" => "jquery.nivoslider.js",
							"css_loads" => array(
								".style" => array(
									"path" => FF_THEME_DIR . "/library/plugins/jquery.nivoslider",
									"file" => "nivo-slider.css"
								)
							)
						)
					)
				)
			)
		)
	)
);
