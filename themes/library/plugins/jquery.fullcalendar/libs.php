<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"flexslider" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.fullcalendar",
							"file" => "fullcalendar.js",
							"css_loads" => array(
								".style" => array(
									"path" => FF_THEME_DIR . "/library/plugins/jquery.fullcalendar",
									"file" => "fullcalendar.css"
								)
							)
						)
					)
				)
			)
		)
	)
);
