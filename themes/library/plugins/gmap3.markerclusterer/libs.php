<?php
return array(
	"google" => array(
		"all" => array(
			"js_defs" => array(
				"maps" => array(
					"js_defs" => array(
						"markerclusterer" => array(
							"path" => FF_THEME_DIR . "/library/plugins/gmap3.markerclusterer",
							"file" => "markerclusterer.js"
							, "js_deps" => array(
								"jquery" => null
							)
						)
					)
				)
			)
		)
	)
);
