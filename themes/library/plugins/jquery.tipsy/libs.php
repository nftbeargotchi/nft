<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"tipsy" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.tipsy",
							"file" => "jquery.tipsy.js",
							"css_loads" => array(
								".style" => array(
									"path" => FF_THEME_DIR . "/library/plugins/jquery.tipsy",
									"file" => "tipsy.css"
								)
							)
						)
					)
				)
			)
		)
	)
);
