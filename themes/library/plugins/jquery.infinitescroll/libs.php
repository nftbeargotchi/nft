<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"infinitescroll" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.infinitescroll",
							"file" => "jquery.infinitescroll.js"
							, "js_defs" => array(
								"observe" => array(
									"path" => FF_THEME_DIR . "/library/plugins/jquery.infinitescroll"
									, "file" => "jquery.infinitescroll.observe.js"
								)
							)
						)
					)
				)
			)
		)
	)
);
