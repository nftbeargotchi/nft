<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"coverscroll" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.coverscroll",
							"file" => "jquery.coverscroll.js"
						)
					)
				)
			)
		)
	)
);
