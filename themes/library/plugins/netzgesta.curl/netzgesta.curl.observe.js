jQuery(document).ready(function() {
	jQuery("img").addClass("curl isize20");
});

/*
* Initialisation class "curl"
* vary the size of the curl by adding isize followed by the desired size in percent:
  Curl size class "isize" - min=0 max=MinImageDimension default=33
* vary the paperback color of the curl by adding icolor followed by the color:
  Curl color class "icolor" - min=000000 max=ffffff default=0
* vary the shadow by adding ishadow followed by the opacity in percent:
  Curl shadow class "ishadow" - min=0 max=100 default=66
* vary the clipping by adding invert:
  Curl clipping class "invert"
*/