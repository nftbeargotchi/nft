<?php
return array(
	"jquery" => array(
		"all" => array(
			"js_defs" => array(
				"plugins" => array(
					"empty" => true,
					"js_defs" => array(
						"image-picker" => array(
							"path" => FF_THEME_DIR . "/library/plugins/jquery.image-picker",
							"file" => "jquery.image-picker.min.js",
							"css_loads" => array(
								".style" => array(
									"path" => FF_THEME_DIR . "/library/plugins/jquery.image-picker",
									"file" => "jquery.image-picker.css",
								)
							)
						)
					)
				)
			)
		)
	)
);
