<?php

class ffWidget_leavepage extends ffCommon
{

    // ---------------------------------------------------------------
    //  PRIVATE VARS (used by code, don't touch or may be explode! :-)

    public $template_file 	 = "ffWidget.html";

    public $class			= "ffWidget_leavepage";

    public $widget_deps	= array();
    public $js_deps = array(
                              "jquery" 			=> null
                            /*, "jquery.ui" 		=> null*/
                        );
    public $css_deps 		= array(
                              /*"jquery.ui"		=> array(
                                      "file" => "jquery.ui.all.css"
                                    , "path" => null
                                )*/
                        );
    // PRIVATE VARS
    public $oPage			= null;
    public $source_path	= null;
    public $style_path		= null;

    public $tpl 			= null;

    public $processed_id	= array();

    public function __construct(ffPage_base $oPage = null, $source_path = null, $style_path = null)
    {
        $this->get_defaults();

        $this->oPage = array(&$oPage);
        
        $this->tpl[0] = ffTemplate::factory(ffCommon_dirname(__FILE__));
        $this->tpl[0]->load_file($this->template_file, "main");

        if ($source_path !== null) {
            $this->source_path = $source_path;
        } elseif ($oPage !== null) {
            $this->source_path = $oPage->getThemePath();
        }

        $this->tpl[0]->set_var("source_path", $this->source_path);

        if ($style_path !== null) {
            $this->tpl[0]->set_var("style_path", $style_path);
        } elseif ($oPage !== null) {
            $this->tpl[0]->set_var("style_path", $oPage->getThemePath());
        }
    }

    public function process($id, &$data, ffPage_base &$oPage)
    {
        $this->tpl[0]->set_var("site_path", $oPage->site_path);
        $this->tpl[0]->set_var("theme", $oPage->getTheme());

        $this->tpl[0]->set_var("id", $id);

        $this->tpl[0]->parse("SectBinding", true);
        return $this->tpl[0]->rpparse("SectIstance", false);
    }

    public function process_headers()
    {
        return $this->tpl[0]->rpparse("SectHeaders", false);
    }

    public function process_footers()
    {
        return $this->tpl[0]->rpparse("SectFooters", false);
    }
}
