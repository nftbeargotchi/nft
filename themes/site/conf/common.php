<?php
function cms_showfiles_on_warning($user_path, $referer, $mode)
{
    if (strpos($user_path, "/medici-online/") !== false) {
        $res["filepath"] = FF_DISK_PATH . FF_THEME_DIR . "/site/images/noavatar.svg";
    } elseif (strpos($user_path, "/utente") !== false) {
        $res["filepath"] = FF_DISK_PATH . FF_THEME_DIR . "/site/images/avatar/pm-user-03.svg";
    }
    if ($res["filepath"]) {
        $res["base_path"] = FF_DISK_PATH . FF_THEME_DIR; //la base. Se nn lo dici parte da uploads
        $res["mode"] = $mode; //e il tipo di resizing che vuoi se lo vuoi
    }
    return $res;
}
