<?php
/**
 * Path
 */
define("FF_DISK_PATH"                               , '');
define("FF_SITE_PATH"                               , '');
define("SITE_UPDIR"                                 , '/uploads');
define("DISK_UPDIR"                                 , '');

/**
 * Session
 */
define("SESSION_SAVE_PATH"                          , '/tmp');
define("SESSION_NAME"                               , 'PHPSESS_c173aaa9');
define("MOD_SECURITY_SESSION_PERMANENT"             , true);

/**
 * Database
 */
define("DB_CHARACTER_SET"                           , 'utf8');
define("DB_COLLATION"                               , 'utf8_unicode_ci');

/**
 * Database Mysql
 */
define("FF_DATABASE_HOST"                           , '');
define("FF_DATABASE_NAME"                           , '');
define("FF_DATABASE_USER"                           , '');
define("FF_DATABASE_PASSWORD"                       , "");

/**
 * Database Mongo
 */ 
define("MONGO_DATABASE_HOST"                        , '');
define("MONGO_DATABASE_NAME"                        , '');
define("MONGO_DATABASE_USER"                        , '');
define("MONGO_DATABASE_PASSWORD"                    , "");

/**
 * Database Mysql
 */
define("ANAGRAPH_DATABASE_HOST"                           , '');
define("ANAGRAPH_DATABASE_NAME"                           , '');
define("ANAGRAPH_DATABASE_USER"                           , '');
define("ANAGRAPH_DATABASE_PASSWORD"                       , "");

define("ANAGRAPH_DOCTOR_DATABASE_HOST"                           , '');
define("ANAGRAPH_DOCTOR_DATABASE_NAME"                           , '');
define("ANAGRAPH_DOCTOR_DATABASE_USER"                           , '');
define("ANAGRAPH_DOCTOR_DATABASE_PASSWORD"                       , "");

/**
 * Database Mongo
 */
define("ANAGRAPH_MONGO_DATABASE_HOST"                        , '');
define("ANAGRAPH_MONGO_DATABASE_NAME"                        , '');
define("ANAGRAPH_MONGO_DATABASE_USER"                        , '');
define("ANAGRAPH_MONGO_DATABASE_PASSWORD"                    , "");


/**
 * Trace
 */
define("TRACE_TABLE_NAME"                           , 'trace');
define("TRACE_NOTIFY_TABLE_NAME"                    , 'trace_notify');
define("TRACE_ONESIGNAL_APP_ID"                     , '');
define("TRACE_ONESIGNAL_API_KEY"                    , '');

/**
 * Trace Database Mysql
 */
define("TRACE_DATABASE_HOST"                        , '');
define("TRACE_DATABASE_NAME"                        , '');
define("TRACE_DATABASE_USER"                        , '');
define("TRACE_DATABASE_PASSWORD"                    , "");

/**
 * Trace Database Mongo
 */
define("TRACE_MONGO_DATABASE_HOST"                  , '');
define("TRACE_MONGO_DATABASE_NAME"                  , '');
define("TRACE_MONGO_DATABASE_USER"                  , '');
define("TRACE_MONGO_DATABASE_PASSWORD"              , "");

/**
 * Notifier
 */
define("NOTIFY_TABLE_NAME"                          , 'trace_notify');
define("NOTIFY_TABLE_KEY"                           , 'ID');
define("NOTIFY_PUSH_ONESIGNAL_APP_ID"               , '');
define("NOTIFY_PUSH_ONESIGNAL_API_KEY"              , '');

/**
 * Notifier Database Mysql
 */
define("NOTIFY_DATABASE_HOST"                            , '');
define("NOTIFY_DATABASE_NAME"                            , '');
define("NOTIFY_DATABASE_USER"                            , '');
define("NOTIFY_DATABASE_PASSWORD"                        , "");

/**
 * Notifier Database Mongo
 */
define("NOTIFY_MONGO_DATABASE_HOST"                          , '');
define("NOTIFY_MONGO_DATABASE_NAME"                          , '');
define("NOTIFY_MONGO_DATABASE_USER"                          , '');
define("NOTIFY_MONGO_DATABASE_PASSWORD"                      , "");


/**
 * Email SMTP
 */
define("A_SMTP_HOST"                                , '');
define("SMTP_AUTH"                                  , true);
define("A_SMTP_USER"                                , '');
define("A_SMTP_PASSWORD"                            , '');
define("A_SMTP_PORT"                                , '587');
define("A_SMTP_SECURE"                              , 'tls');

/**
 * Email Settings
 */
define("A_FROM_EMAIL"                               , '');
define("FROM_EMAIL"                               , '');
define("A_FROM_NAME"                                , '');
define("CC_FROM_EMAIL"                              , '');
define("CC_FROM_NAME"                               , '');
define("BCC_FROM_EMAIL"                             , '');
define("BCC_FROM_NAME"                              , '');

/**
 * Superadmin
 */
define("SUPERADMIN_USERNAME"                        , 'admin');
define("SUPERADMIN_PASSWORD"                        , "sgrillisgrilli");

define("FRAMEWORK_CSS"			                    , 'foundation');
define("FONT_ICON"			                        , 'fontawesome');
/**
 * Locale
 */
define("LANGUAGE_DEFAULT"                           , 'ITA');
define("LANGUAGE_DEFAULT_ID"                        , '1');
define("LANGUAGE_RESTRICTED_DEFAULT"                , 'ITA');
define("TIMEZONE"                                   , 'Europe/Rome');

/**
 * Repository Master
 */
define("MASTER_SITE"                                , 'www.blueocarina.net');
define("PRODUCTION_SITE"                            , '');
define("DEVELOPMENT_SITE"                           , '');

/**
 * Auth Apachee
 */
//define("AUTH_USERNAME"                              , 'admin');
//define("AUTH_PASSWORD"                              , "sgrillisgrilli");

/**
 * FTP
 */
define("FTP_USERNAME"                               , '');
define("FTP_PASSWORD"                               , "");
define("FTP_PATH"                                   , '/httpdocs');

/**
 * Debug
 */
define("DEBUG_MODE"                                 , false);
define("DEBUG_PROFILING"                            , false);
define("DEBUG_LOG"                                  , true);

/**
 * Site Settings
 */
//define("DISABLE_CACHE"                            , );
define("CACHE_LAST_VERSION"                         , '0');
define("CM_LOCAL_APP_NAME"                          , '');
define("APPID"                                      , '');
define("TRACE_VISITOR"                              , false);
define("TRACE_READ"                              , false);
define("ADMIN_THEME"			                    , 'admin');
define("COMPOSER_PATH"                              , "/vendor");

define("CDN_STATIC"                                 , true);
define("CM_SHOWFILES"                               , '');

/**
 * Server Settings
 */
define("PHP_EXT_MEMCACHE"                           , false);
define("PHP_EXT_APC"                                , false);
define("PHP_EXT_JSON"                               , true);
define("PHP_EXT_GD"                                 , true);
define("APACHE_MODULE_EXPIRES"                      , true);
define("MYSQLI_EXTENSIONS"                          , true);
define("MEMORY_LIMIT"                               , '192M');
define("ENABLE_MINIFY_CSS"                          , true);
define("ENABLE_MINIFY_JS"                           , true);
