ff.cms.get("/srv/new-er-service/cta-action", function(data) {if(jQuery(".cta-container").length) {jQuery(".cta-container").html(data["html"]);}}, {}, {"priority": "bottom"});


function owlCustomRefresh() {
    jQuery("#QL .elem-mobile").owlCarousel({
        margin: 10,
        autoWidth: true,
        navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
        items: 1,
        nav:false,
        dots: true
    });
}

jQuery(function() {
    if($('[data-equalizer]').length) {
        if($(window).width() > 639) {
            $(window).on('resize', function () {
                setTimeout(function () {
                    $('[data-equalizer]').foundation('_reflow');
                }, 1000);
            });
            setTimeout(function () {
                $('[data-equalizer]').foundation('_reflow');
            }, 1000);
        }
    }

    if(window.location.hash == "#pediatra24") {
        newQuestionClickNew(undefined,"60");
    }
    var linkCount = {};
    if(jQuery(".question-block-container .single-question").length) {
        jQuery(".question-block-container").owlCarousel({
            margin:15,
            nav:true,
            dots: false,
            autoWidth: true,
            navText: ["<i class='fa fa-long-arrow-left' aria-hidden='true'></i>","<i class='fa fa-long-arrow-right' aria-hidden='true'></i>"],
            responsive: {
                0: { items: 1 },
                320: { items: 2 },
                640: { items: 3 },
                1024: { items: 3 }
            }

        }).css("visibility", "visible").show();
    }
    if(jQuery(".user-feedback .single-feedback").length) {
        jQuery(".feedback-slider").owlCarousel({
            margin:15,
            nav:true,
            dots: false,
            autoWidth: true,
            navText: ["<i class='fa fa-long-arrow-left' aria-hidden='true'></i>","<i class='fa fa-long-arrow-right' aria-hidden='true'></i>"],
            responsive: {
                0: { items: 1 },
                900: { items: 2 },
                1350: { items: 3 }
            }

        }).css("visibility", "visible").show();
    }
    
    if(jQuery(".active-doctor-strip-container.elem-mobile .active-doctor-item").length) {
        jQuery(".active-doctor-strip-container.elem-mobile").owlCarousel({
            dots: true,
            startPosition: 1,
            autoWidth: true,
            center: true,
            margin: 10,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            items: 1
        });
    }



    if(jQuery("#QL .elem-mobile .vg-doctor-item-question").length) {
        owlCustomRefresh();
    }

    if(jQuery(".erheader-new .elem-mobile .header-block-container .slide").length) {
        var elem = jQuery(".erheader-new .elem-mobile .header-block-container .slider-container").owlCarousel({
            nav:false,
            dots: false,
            center: true,
            startPosition: 0,
            items: 1,
            mouseDrag : false,
            touchDrag:false
        }).addClass("loaded");

        jQuery(".go-to-more-info A").click(function() {
            elem.trigger('prev.owl.carousel');
        });
        jQuery(".go-to-doctor-info A").click(function() {
            elem.trigger('next.owl.carousel');
        });
        jQuery(".go-to-next-slide A").click(function() {
            elem.trigger('next.owl.carousel');
        });
        jQuery(".go-to-prev-slide A").click(function() {
            elem.trigger('prev.owl.carousel');
        });
    }

    if(jQuery(".last-question-container .vg-doctor-item-question").length) {
        jQuery(".question-carousel-cont").owlCarousel({
            nav:false,
            dots: false,
            autoWidth: true,
            margin: 20,
            center: true,
            startPosition: 1,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            responsive: {
                0: { items: 1 },
                600: { items: 1 },
                1500: { items: 3 }
            },
            loop: true
        });
    }

    if(jQuery('#catERSmall').length) {
        var questiontag2 = jQuery('#catERSmall').attr("data-default");
        var ms = $('#catERSmall').magicSuggest({
            placeholder: "Cerca per specializzazione",
            data: '/srv/tokeninput/question-tag-archivio',
            valueField: 'id',
            displayField: 'name',
            allowFreeEntries: false,
            autoSelect: true,
            value: questiontag2,
            maxSelection: 100
        });

        $(ms).on('selectionchange', function(e,m){
            var string = "";
            var date = "";
            var erList2 = this.getValue();
            if(questiontag2 != JSON.stringify(erList2)) {
                if (jQuery(".month-elem").length) {
                    if (jQuery(".month-elem").val().length) {
                        date = jQuery(".month-elem").val();
                    }

                    if (date.length) {
                        string = string + "/" + date.replace(/-/g, "/");
                    }
                }
                if (erList2.length) {
                    erList2.sort();
                    string = string + "&tag=" + erList2.join().replace(/,/g, "+");
                }
                console.log(string);
                console.log(date);
                console.log(erList2);
                history.pushState(null, null, "/esperto-risponde/archivio" + string);

                jQuery("#QL").prepend('<div class="sk-cube-grid"><div class="sk-cube sk-cube1"></div><div class="sk-cube sk-cube2"></div><div class="sk-cube sk-cube3"></div><div class="sk-cube sk-cube4"></div><div class="sk-cube sk-cube5"></div><div class="sk-cube sk-cube6"></div><div class="sk-cube sk-cube7"></div><div class="sk-cube sk-cube8"></div><div class="sk-cube sk-cube9"></div></div>');
                jQuery("#QL .vg-doctor-item-question").css("opacity", "0.4");
                ff.cms.get("/srv/er-archivio-filter", function (data) {
                    jQuery("#QL").html(data["html"]);
                    new Foundation.Equalizer($("#QL")).applyHeight();
                    if(jQuery("#QL .elem-mobile .vg-doctor-item-question").length) {
                        owlCustomRefresh();
                        $(window).resize( function (){
                            owlCustomRefresh();
                        });
                    }
                }, {"tag": erList2, "date": date});
            } else {
                questiontag2 = "done";
            }
        });

        if (jQuery(".month-elem").length) {
            jQuery(".month-elem").change(function () {
                var string = "";

                var date = "";
                if (jQuery(".month-elem").val().length) {
                    date = jQuery(".month-elem").val();
                }

                if (date.length) {
                    string = string + "/" + date.replace(/-/g, "/");
                }

                var erList2 = $('#catERSmall').magicSuggest().getValue();

                if (erList2.length) {
                    string = string + "&tag=" + erList2.join().replace(",", "+");
                }
                console.log(string);
                console.log(date);
                console.log(erList2);
                history.pushState(null, null, "/esperto-risponde/archivio" + "?" + string);

                jQuery("#QL").prepend('<div class="sk-cube-grid"><div class="sk-cube sk-cube1"></div><div class="sk-cube sk-cube2"></div><div class="sk-cube sk-cube3"></div><div class="sk-cube sk-cube4"></div><div class="sk-cube sk-cube5"></div><div class="sk-cube sk-cube6"></div><div class="sk-cube sk-cube7"></div><div class="sk-cube sk-cube8"></div><div class="sk-cube sk-cube9"></div></div>');
                jQuery("#QL .vg-doctor-item-question").css("opacity", "0.4");
                ff.cms.get("/srv/er-archivio-filter", function (data) {
                    jQuery("#QL").html(data["html"]);
                    new Foundation.Equalizer($("#QL")).applyHeight();
                    if(jQuery("#QL .elem-mobile .vg-doctor-item-question").length) {
                        owlCustomRefresh();
                        $(window).resize( function (){
                            owlCustomRefresh();
                        });
                    }
                }, {"tag": erList2, "date": date});

            });
        }
    }

    if(jQuery('#catERTag').length) {
        var questiontag = jQuery('#catERTag').attr("data-default");
        var ms = $('#catERTag').magicSuggest({
            placeholder: "Cerca per specializzazione",
            data: '/srv/tokeninput/question-tag-archivio',
            valueField: 'id',
            displayField: 'name',
            allowFreeEntries: false,
            autoSelect: true,
            value: questiontag,
            maxSelection: 100
        });

        $(ms).on('selectionchange', function(e,m){
            var string = "";
            var date = "";
            var erList = this.getValue();
            if(questiontag != JSON.stringify(erList)) {
                if (erList.length) {
                    erList.sort();
                    string = erList.join().replace(/,/g, "+");
                }

                history.pushState(null, null, "/esperto-risponde/tag/" + string);

                jQuery("#QL").prepend('<div class="sk-cube-grid"><div class="sk-cube sk-cube1"></div><div class="sk-cube sk-cube2"></div><div class="sk-cube sk-cube3"></div><div class="sk-cube sk-cube4"></div><div class="sk-cube sk-cube5"></div><div class="sk-cube sk-cube6"></div><div class="sk-cube sk-cube7"></div><div class="sk-cube sk-cube8"></div><div class="sk-cube sk-cube9"></div></div>');
                jQuery("#QL .vg-doctor-item-question").css("opacity", "0.4");
                ff.cms.get("/srv/er-archivio-filter", function (data) {
                    jQuery("#QL").html(data["html"]);
                    new Foundation.Equalizer($("#QL")).applyHeight();
                    if(jQuery("#QL .elem-mobile .vg-doctor-item-question").length) {
                        owlCustomRefresh();
                        $(window).resize( function (){
                            owlCustomRefresh();
                        });
                    }
                }, {"tag": erList, "date": date});
            } else {
                questiontag = "done";
            }
        });
    }

    if(jQuery("#L242 .doctor-follow-active-er").length) {
        var url = jQuery("#L242 .doctor-follow-active-er").attr("data-url");
        linkCount[url] = url;

        jQuery(document).on("click", ".doctor-follow-active-er", function() {
            if(ff.group!=="guests")
            {
                path = jQuery(this).attr("data-url");

                ff.cms.get("/srv/share/social-count", function (data) {
                    if (data["result"]) {
                        if (data["result"][path]["check-follow"]) {
                            jQuery(".doctor-follow-active-er[data-url='" + path + "']").addClass("doctor-followed");
                            jQuery(".doctor-follow-button").addClass("doctor-followed");
                            jQuery(".doctor-follow-button .large-container SPAN.follow-text").text("Stai seguendo");
                        } else {
                            jQuery(".doctor-follow-active-er[data-url='" + path + "']").removeClass("doctor-followed");
                            jQuery(".doctor-follow-button").removeClass("doctor-followed");
                            jQuery(".doctor-follow-button .large-container SPAN.follow-text").text("Segui");
                        }
                    }
                }, {"type": "anagraph", "operation": "save", "anagraph-url": path, "case": "click"});
            } else
            {
                if(!jQuery('.loginbox-container .login-required').length){
                    jQuery('.loginbox-container .close').parent().after('<div class=\'login-required text-center\'><div><i class=\'fa fa-ban\' aria-hidden=\'true\'></i><span>Questa operazione &egrave; riservata agli utenti registrati. Effettua il login.</span></div></div>');
                }
                jQuery('#L25 .login-button A').click();
            }
        });

        ff.cms.get("/srv/share/social-count", function (data) {
            if(data["result"]) {
                jQuery.each(data["result"], function(path, value) {
                    if(value["check-follow"])
                        jQuery(".doctor-follow-active-er[data-url='" + path + "']").addClass("doctor-followed");
                });
            }
            jQuery(".mol-interactors .item-doc-like").show();
        }, { "params": linkCount, "type": "anagraph", "operation": "get" }, {"priority": "bottom"});
    }

    if(jQuery("#L241").length) {
        ff.cms.get("/srv/block-register-er", function (data) {
            jQuery("#L241").html(data["html"]);
        }, {"path":window.location.pathname});
    }

    if(jQuery("#L153 .question-carousel-cont .vg-doctor-item-question").length) {
        jQuery(".question-carousel-cont").owlCarousel({
            nav:true,
            dots: true,
            autoWidth: true,
            center: true,
            startPosition: 1,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            items: 3,
            loop: true
        });
    }

    if(jQuery(".active-doctor-strip-container").length) {

        jQuery(".doctor-follow-active-er").each(function() {
            var url = jQuery(this).attr("data-url");
            linkCount[url] = url;
        });

        jQuery(document).on("click", ".doctor-follow-active-er", function() {
            if(ff.group!=="guests")
            {
                path = jQuery(this).attr("data-url");

                ff.cms.get("/srv/share/social-count", function (data) {
                    if (data["result"]) {
                        if (data["result"][path]["check-follow"])
                            jQuery(".doctor-follow-active-er[data-url='" + path + "']").addClass("doctor-followed");
                        else
                            jQuery(".doctor-follow-active-er[data-url='" + path + "']").removeClass("doctor-followed");
                    }
                }, {"type": "anagraph", "operation": "save", "anagraph-url": path, "case": "click"});
            } else
            {
                if(!jQuery('.loginbox-container .login-required').length){
                    jQuery('.loginbox-container .close').parent().after('<div class=\'login-required text-center\'><div><i class=\'fa fa-ban\' aria-hidden=\'true\'></i><span>Questa operazione &egrave; riservata agli utenti registrati. Effettua il login.</span></div></div>');
                }
                jQuery('#L25 .login-button A').click();
            }
        });

        ff.cms.get("/srv/share/social-count", function (data) {
            if(data["result"]) {
                jQuery.each(data["result"], function(path, value) {
                    if(value["check-follow"])
                        jQuery(".doctor-follow-active-er[data-url='" + path + "']").addClass("doctor-followed");
                });
            }
            jQuery(".mol-interactors .item-doc-like").show();
        }, { "params": linkCount, "type": "anagraph", "operation": "get" });
    }

    if(jQuery('#catER').length) {
        var questiontag = jQuery('#catER').attr("data-default");
        var ms = $('#catER').magicSuggest({
            placeholder: "Cerca per specializzazione",
            data: '/srv/tokeninput/question-tag-archivio',
            valueField: 'id',
            displayField: 'name',
            allowFreeEntries: false,
            autoSelect: true,
            value: questiontag,
            maxSelection: 100
        });

        $(ms).on('selectionchange', function(e,m){
            var string = "";
            var date = "";
            var erList = this.getValue();
            if(questiontag != JSON.stringify(erList)) {
                if (jQuery(".year-chooser-class").length && jQuery(".month-chooser-class").length) {
                    if (jQuery(".year-chooser-class").val().length) {
                        string = string + "/" + jQuery(".year-chooser-class").val();
                        date = jQuery(".year-chooser-class").val();

                        if (jQuery(".month-chooser-class").val().length) {
                            string = string + "/" + jQuery(".month-chooser-class").val();
                            date = date + "-" + jQuery(".month-chooser-class").val();
                        }
                    }

                }
                if (erList.length) {
                    erList.sort();
                    string = string + "?tag=" + erList.join().replace(/,/g, "+");
                }

                console.log(string);
                console.log(date);
                console.log(erList);
                history.pushState(null, null, "/esperto-risponde/archivio" + string);

                jQuery("#QL").prepend('<div class="sk-cube-grid"><div class="sk-cube sk-cube1"></div><div class="sk-cube sk-cube2"></div><div class="sk-cube sk-cube3"></div><div class="sk-cube sk-cube4"></div><div class="sk-cube sk-cube5"></div><div class="sk-cube sk-cube6"></div><div class="sk-cube sk-cube7"></div><div class="sk-cube sk-cube8"></div><div class="sk-cube sk-cube9"></div></div>');
                jQuery("#QL .vg-doctor-item-question").css("opacity", "0.4");
                ff.cms.get("/srv/er-archivio-filter", function (data) {
                    jQuery("#QL").html(data["html"]);
                    new Foundation.Equalizer($("#QL")).applyHeight();
                    if(jQuery("#QL .elem-mobile .vg-doctor-item-question").length) {
                        owlCustomRefresh();
                        $(window).resize( function (){
                            owlCustomRefresh();
                        });
                    }
                }, {"tag": erList, "date": date});
            } else {
                questiontag = "done";
            }
        });

        if (jQuery(".year-chooser-class").length && jQuery(".month-chooser-class").length) {
            jQuery(".year-chooser-class,.month-chooser-class").change(function () {
                var string = "";

                var erList = $('#catER').magicSuggest().getValue();

                var date = "";
                if (jQuery(".year-chooser-class").length && jQuery(".month-chooser-class").length) {
                    if (jQuery(".year-chooser-class").val().length) {
                        string = string + "/" + jQuery(".year-chooser-class").val();
                        date = jQuery(".year-chooser-class").val();

                        if (jQuery(".month-chooser-class").val().length) {
                            string = string + "/" + jQuery(".month-chooser-class").val();
                            date = date + "-" + jQuery(".month-chooser-class").val();
                        }
                    }

                }
                if (erList.length) {
                    erList.sort();
                    string = string + "?tag=" + erList.join().replace(/,/g, "+");
                }

                console.log(string);
                console.log(date);
                console.log(erList);
                history.pushState(null, null, "/esperto-risponde/archivio" + string);

                jQuery("#QL").prepend('<div class="sk-cube-grid"><div class="sk-cube sk-cube1"></div><div class="sk-cube sk-cube2"></div><div class="sk-cube sk-cube3"></div><div class="sk-cube sk-cube4"></div><div class="sk-cube sk-cube5"></div><div class="sk-cube sk-cube6"></div><div class="sk-cube sk-cube7"></div><div class="sk-cube sk-cube8"></div><div class="sk-cube sk-cube9"></div></div>');
                jQuery("#QL .vg-doctor-item-question").css("opacity", "0.4");
                ff.cms.get("/srv/er-archivio-filter", function (data) {
                    jQuery("#QL").html(data["html"]);
                    new Foundation.Equalizer($("#QL")).applyHeight();
                    if(jQuery("#QL .elem-mobile .vg-doctor-item-question").length) {
                        owlCustomRefresh();
                        $(window).resize( function (){
                            owlCustomRefresh();
                        });
                    }
                }, {"tag": erList, "date": date});

            });
        }
    }

    if(jQuery(".category-container.new-er-page .elem-mobile .cat-single-item").length) {
        jQuery(".category-container.new-er-page .elem-mobile .cat-slider-container").owlCarousel({
            nav:false,
            dots: true,
            dotsEach: 1,
            center: true,
            loop: true,
            autoWidth: true,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            items: 6,
            margin: 30
        }).removeClass("invisible");
    }
    if(jQuery(".category-container.new-er-page .elem-desktop .cat-single-item").length) {
        jQuery(".category-container.new-er-page .elem-desktop .cat-slider-container").owlCarousel({
            nav:false,
            dots: false,
            loop: false,
            autoWidth: true,
            navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>","<i class='fa fa-angle-right' aria-hidden='true'></i>"],
            items: 6,
            margin: 30
        }).removeClass("invisible");
    }
    jQuery(document).on("click",".question[data-fullclick]",function(e) {
            if(jQuery(this).attr("data-fullclick") && !jQuery(e.srcElement).is("a") && !jQuery(e.srcElement).closest("a").length)  {
                    window.location.href = jQuery(this).attr("data-fullclick");
                    return false;
            }
    }).css("cursor", "pointer");

    if(getUrlParameter('new-question') !== undefined) {
        setTimeout(function(){ newQuestionClickNew();}, 2000);
    }
    var path;

    jQuery(document).on("keyup", "#autocomplete_qeasearch", function(e) {
        if(e.keyCode == 13 && jQuery("#autocomplete_qeasearch").val()) {
            submitSearch(jQuery("#qeasearch").val()
                ? jQuery("#qeasearch").val()
                : jQuery("#autocomplete_qeasearch").val()
            );
        }
    });

    jQuery(document).on("keyup", ".elem-mobile .qea-search INPUT", function(e) {
        if(e.keyCode == 13 && jQuery(".elem-mobile .qea-search INPUT").val()) {
            submitERSearch(jQuery(".elem-mobile .qea-search"));
        }
    });
    
    jQuery(document).on("keyup", ".elem-desktop .qea-search INPUT", function(e) {
        if(e.keyCode == 13 && jQuery(".elem-desktop .qea-search INPUT").val()) {
            submitERSearch(jQuery(".elem-desktop .qea-search"));
        }
    });
    
    if(jQuery("#L49").length && jQuery("#L150").length && $( window ).width()< 640) {
        var adv = jQuery("#L49")[0].outerHTML;
        jQuery("#L49").remove();
        jQuery(adv).insertAfter("#L150");
    }

    if(jQuery("#L150").length)
    {
        path = window.location.pathname;

        if(ff.isMobile()) {
            if(jQuery(".question-text").height()> 250) {
                jQuery("#L150 .question-detail-container").addClass("crop-text");

                $('#L150 .question-detail-container').after().click(function() {
                    jQuery("#L150 .question-detail-container").removeClass("crop-text");
                });
            }
        }
        
        if(jQuery("#L150 .group2 .doctor-avatar A").length)
        {
            var anagraphURL = jQuery("#L150 .group2 .doctor-avatar A").attr("href");
            jQuery("#L150 .group2").attr("anagraph-url", anagraphURL);

            ff.cms.get("/srv/share/social-count", function( data ) {
                if(data["result"])
                {
                    if(data["result"][anagraphURL]["check-follow"]) {
                        jQuery(".doctor-follow").addClass("followed");
                    } else {
                        jQuery(".doctor-follow").removeClass("followed");
                    }
                }
                jQuery("DIV.doctor-follow").show();
            }, { "type": "anagraph", "operation": "get", "anagraph-url": anagraphURL });
        }

    }
    jQuery(document).on("focus",".qea-search", function() {
        jQuery(this).parents(".group").find(".tooltip-content").slideDown().addClass("focus");
        
    });
    jQuery(document).on("focusout",".qea-search", function() {
        jQuery(this).parents(".group").find(".tooltip-content").slideUp().removeClass("focus");
    });

    console.log(jQuery(".doctor-teleconsulto").length);
    if(jQuery(".er-homepage-teleconsulto .doctor-teleconsulto").length) {
        var navSetting = true;
        if(jQuery(".er-homepage-teleconsulto .doctor-teleconsulto .slider-container .doctor-premium-item").length < 2) {
            navSetting = false;
        }

        console.log("ciao");

        jQuery(".er-homepage-teleconsulto .doctor-teleconsulto .slider-container").owlCarousel({
            loop: true,
            autoplay: true,
            autoplaySpeed: 600,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            responsiveClass: true,
            nav: navSetting,
            dots: false,
            center: true,
            autoWidth: true,
            navText: ["<i class='fa fa-long-arrow-left' aria-hidden='true'></i>","<i class='fa fa-long-arrow-right' aria-hidden='true'></i>"],
            responsive: {
                0: { items: 1 },
                320: { items: 2 },
                640: { items: 3 },
                1024: { items: 3 }
            },
            margin: 10
        }).css("height", "auto");
    }

});

function submitSearch(string) {
    jQuery("#autocomplete_qeasearch").autocomplete( "close" );
    window.location.href = "/search/" + ff.slug(string) + "/domande-e-risposte";
}

function submitERSearch(string) {
    var searchKey = string.find("INPUT").val();
    if(searchKey.length) { 
        ff.cms.get("/srv/save-er-search", function( data ) {
            window.location.href = "/esperto-risponde/search/" + ff.slug(data["search"]);
        }, { "search-key": searchKey });
    }
}

function addToSelectEr(string, id) {
    if(id == "#catER" && !jQuery(id).is(":visible")) {
        id = "#catERSmall";
    }
    var result = [];
    if(string.length) {
        if($(id).magicSuggest().getValue().length) {
            result = $(id).magicSuggest().getValue();
        }
        result.push(string);
        $(id).magicSuggest().setValue(result);
    }
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function doctorRequestTeleconsulto() {
    ff.cms.get("/srv/consulto-premium/doctor-request", function(data) {
        if(data["html"].length) {
            openNewModal(data["html"]);
        }
    }, {});
}

function sendRequestTeleconsulto() {
    var datiUtente = {};
    var error = false;
    var errorEmail = false;
    var errorCond = false;
    var container = jQuery(".teleconsulto-popoup-container");

    container.find(".missing").removeClass("missing");
    container.find(".error-cont").remove();
    container.css({"opacity":"0.5","pointer-events":"none"});

    var nome = container.find("INPUT[name='doctor-name']").val();
    var email = container.find("INPUT[name='doctor-email']").val();
    var telefono = container.find("INPUT[name='doctor-tel']").val();

    if(nome.length) {
        datiUtente.nome =  nome;
    } else {
        error = true;
        container.find("INPUT[name='doctor-name']").addClass("missing");
    }

    if(!email.length) {
        error = true;
        container.find("INPUT[name='doctor-email']").addClass("missing");
    } else if(!profileIsValidEmailAddress(email)) {
        errorEmail = true;
        container.find("INPUT[name='doctor-email']").addClass("missing");
    } else {
        datiUtente.email =  email;
    }

    if(telefono.length) {
        datiUtente.telefono =  telefono;
    } else {
        error = true;
        container.find("INPUT[name='doctor-tel']").addClass("missing");
    }

    if(!container.find("#conditioncheckbox").is(":checked") || !container.find("#conditioncheckboxPrivacy").is(":checked")) {
        errorCond = true;
        container.find(".accept-condition LABEL").addClass("missing");
    }

    if(errorCond) {
        container.css({"opacity":"1","pointer-events":"auto"});
        container.find(".title-container").append("<div class='error-cont'><div data-alert class='callout alert'>È necessaria l'accettazione dei termini d'uso e l'informativa Privacy</div></div>" );
    } else if(errorEmail) {
        container.css({"opacity":"1","pointer-events":"auto"});
        container.find(".title-container").append("<div class='error-cont'><div data-alert class='callout alert'>Il campo mail inserito non è valido</div></div>" );
    } else if(error) {
        container.css({"opacity":"1","pointer-events":"auto"});
        container.find(".title-container").append("<div class='error-cont'><div data-alert class='callout alert'>Compila i campi richiesti</div></div>" );
    } else {
        ff.cms.get("/srv/send-email-teleconsulto", function (data) {
            if(data) {
                if(data["result"] != "success") {
                    container.css({"opacity":"1","pointer-events":"auto"});
                    container.find(".callout.alert").remove();
                    container.find(".title-container").append('<div class="error-cont"><div data-alert class="callout alert">Impossibile inviare il messaggio</div></div>');
                } else {
                    container.css({"opacity":"1","pointer-events":"auto"});
                    container.find(".title-container").remove();
                    container.find(".field-contact-container").html('<div class="row"><div class="columns small-12"><div class="success-form-text text-center"><img src="/themes/site/images/check-circle-2-green.svg" /><b class="success-title">' + data["titolo"] + '</b><br/><br/>' + data["message"] + '</div></div></div>');
                }
            }
        }, { params: datiUtente });
    }
}

function profileIsValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};
