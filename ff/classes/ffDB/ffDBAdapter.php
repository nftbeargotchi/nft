<?php
/**
 * @ignore
 * @package FormsFramework
 */

/**
 * @ignore
 * @package FormsFramework
 */
interface ffDBAdapter
{
    const TYPE_DB			= 0;
    const TYPE_TABLE			= 1;
    const TYPE_COLUMNS		= 2;
    const TYPE_INDEXES		= 3;
    const TYPE_QUERY		= 4;
    
    public function fieldGet();
    public function fieldSet();
    public function recordNext();
    public function recordPrev();
    public function recordGet();
    public function recordDelete();
    public function recordUpdate();
    public function recordInsert();
    public function recordsetGet();

    public function queryStructure($eType, $sName, $lazy, $create_globals);
    public function getDBSource($sName, $eType);
    
    public function connect();
}
