<?php
/**
 * VGallery: CMS based on FormsFramework
 * Copyright (C) 2004-2015 Alessandro Stucchi <wolfgan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package VGallery
 * @subpackage core
 * @author Alessandro Stucchi <wolfgan@gmail.com>
 * @copyright Copyright (c) 2004, Alessandro Stucchi
 * @license http://opensource.org/licenses/gpl-3.0.html
 * @link https://github.com/wolfgan43/vgallery
 */

use Facebook\Facebook;

class authApiSocial
{
    public function __construct()
    {

    }
    public function facebook()
    {
        if (Auth::isLogged()) {
            $response["type"] = 'html';
            $response["js"] = 'setTimeout(function(){ window.opener.location.reload();window.close();  }, 500);';
        } else {
            $fb = new Facebook([
                'app_id' => "",
                'app_secret' => "",
                'default_graph_version' => 'v2.2',
            ]);


            $helper = $fb->getRedirectLoginHelper();

            $_SESSION['FBRLH_state'] = $_GET['state'];
            if (isset($_GET['state'])) {
                $helper->getPersistentDataHandler()->set('state', $_GET['state']);
            }

            $permissions = ['email'];
            $loginUrl = $helper->getLoginUrl("http" . ($_SERVER["HTTPS"] ? "s" : "") . "://" . $_SERVER["HTTP_HOST"] . "/api/auth/social/facebookresponse", $permissions);

            $response["redirect"] = $loginUrl . "&display=popup";
        }
        return $response;
    }

    public function facebookresponse()
    {
        $fb = new Facebook([
            'app_id' => "1103256527149308",
            'app_secret' => "a035f7534ed3d2c83068041191ff2f14",
            'default_graph_version' => 'v2.2',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        $_SESSION['FBRLH_state'] = $_GET['state'];
        if (isset($_GET['state'])) {
            $helper->getPersistentDataHandler()->set('state', $_GET['state']);
        }

        try {
            $accessToken = $helper->getAccessToken();
        } catch (Facebook\Exceptions\FacebookResponseException $e) {
            $config["social"]["error"] = $e->getMessage();
            $response = Auth::widget("error", $config);
            $response["status"] = 400;
            $response["type"] = 'html';
            $response["js"] = 'setTimeout(function(){ window.close();  }, 1500);';
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            $config["social"]["error"] = $e->getMessage();
            $response = Auth::widget("error", $config);
            $response["status"] = 400;
            $response["type"] = 'html';
            $response["js"] = 'setTimeout(function(){ window.close();  }, 1500);';
        };

        if (!isset($accessToken)) {
            if ($helper->getError()) {
                $config["social"]["error"] = $helper->getErrorDescription();
                $response = Auth::widget("error", $config);
                $response["status"] = 401;
                $response["type"] = 'html';
                $response["js"] = 'setTimeout(function(){ window.close();  }, 1500);';
            } else {
                $config["social"]["error"] = "Bad Request";
                $response = Auth::widget("error", $config);
                $response["status"] = 400;
                $response["type"] = 'html';
                $response["js"] = 'setTimeout(function(){ window.close();  }, 1500);';
            }
        } else {
            $fbResponse = $fb->get('/me?fields=id,first_name,last_name,picture,email', $accessToken);
            $arrInfo = $fbResponse->getGraphUser();

            $arrAnagraph["name"] = $arrInfo->getFirstName();
            $arrAnagraph["surname"] = $arrInfo->getLastName();
            $arrAnagraph["email"] = $arrInfo->getField('email');
        }

        if (is_array($arrAnagraph) && count($arrAnagraph)) {
            $anagraphObject = Anagraph::getInstance();
            $arrAnagraphList = $anagraphObject->read(
                array(
                    "anagraph.status"
                    , "anagraph.ID"
                    , "anagraph.ID_user"
                ), array(
                    "anagraph.email" => $arrAnagraph["email"]
                    , "anagraph.ID_domain" => 1
                )
            );

            if (is_array($arrAnagraphList) && count($arrAnagraphList)) {
                $ID_user = $arrAnagraphList[0]["ID_user"];
                $token = getTokenLocalDB($ID_user);

                if (!$arrAnagraphList[0]["status"]) {
                    updateUserInfo($ID_user, "activate", 1);
                }

                saveLocalSocialToken($accessToken->getValue(), $ID_user, "facebook");
                Auth::getInstance("session")->create($ID_user);
                $response = Auth::widget("welcome");
                $response["type"] = 'html';
                $response["html"] = '<div class="welcome-box-social">' . $response["html"] . '</div>';
                if($_SERVER["HTTP_REFERER"] == "/" || strpos($_SERVER["HTTP_REFERER"], "/login")
                ) {
                    $response["js"] = 'if(window.opener  == null) { window.location.href=\'/area-utente\'} else {setTimeout(function(){ window.opener.location.href=\'' . WWW_LINK_ABSOLUTE . '/area-utente\'; window.close();   }, 1500); }';
                } else {
                    $response["js"] = 'if(window.opener  == null) { window.location.href=\'' . $_SERVER["HTTP_REFERER"] .'\'} else {setTimeout(function(){ window.opener.location.reload(); window.close();   }, 1500); }';
                }
                $response["css"] = '.welcome-box-subtitle,.welcome-box-title{font-family:Raleway,sans-serif}body{background:-webkit-linear-gradient(45deg,#e74c3c 0,#df2472 100%)!important}.welcome-box-social{max-width:400px;text-align:center;margin:40px auto;min-height:calc(100vh - 80px);background-color:#fff;padding:30px}.welcome-box-title{font-size:22px;margin-bottom:15px}.welcome-box-subtitle{font-size:18px;margin-bottom:2rem;text-align:center}.welcome-box-avatar{text-align:center}.welcome-box-avatar IMG{width:200px;height:200px;margin:0 auto}';

                Auth::doEvent("on_registration_social_done", $ID_user, array("ID_user" => $ID_user, "anagraph" => $arrAnagraph));
            } else {
                $socialRegistration = $this->socialregisteruser($arrAnagraph, "facebook");

                if($socialRegistration["ID_user"] > 0) {
                    Auth::getInstance("session")->create($socialRegistration["ID_user"]);
                    $response = Auth::widget("welcome");
                    $response["type"] = 'html';
                    $response["html"] = '<div class="welcome-box-social">' . $response["html"] . '</div>';

                    if($_SERVER["HTTP_REFERER"] == "/" || strpos($_SERVER["HTTP_REFERER"], "/login")) {
                        $response["js"] = 'if(window.opener  == null) { window.location.href=\'/area-utente\'} else {setTimeout(function(){ window.opener.location.href=\'' . WWW_LINK_ABSOLUTE . '/area-utente\'; window.close();   }, 1500); }';
                    } else {
                        $response["js"] = 'if(window.opener  == null) { window.location.href=\'' . $_SERVER["HTTP_REFERER"] .'\'} else {setTimeout(function(){ window.opener.location.reload(); window.close();   }, 1500); }';
                    }
                    $response["css"] = '.welcome-box-subtitle,.welcome-box-title{font-family:Raleway,sans-serif}body{background:-webkit-linear-gradient(45deg,#e74c3c 0,#df2472 100%)!important}.welcome-box-social{max-width:400px;text-align:center;margin:40px auto;min-height:calc(100vh - 80px);background-color:#fff;padding:30px}.welcome-box-title{font-size:22px;margin-bottom:15px}.welcome-box-subtitle{font-size:18px;margin-bottom:2rem;text-align:center}.welcome-box-avatar{text-align:center}.welcome-box-avatar IMG{width:200px;height:200px;margin:0 auto}';

                    Auth::doEvent("on_registration_social_done", $ID_user, array("ID_user" => $ID_user, "anagraph" => $arrAnagraph));
                } else {
                    $config["social"]["error"] = "Error during registration";
                    $response = Auth::widget("error", $config);
                    $response["status"] = 400;
                    $response["type"] = 'html';
                    $response["js"] = 'setTimeout(function(){ window.close();  }, 1500);';
                }

            }
        }
        return $response;
    }

    function google()
    {
        if (Auth::isLogged()) {
            $response["type"] = 'html';
            $response["js"] = 'setTimeout(function(){ window.opener.location.reload();window.close();  }, 500);';
        } else {
            $gClient = new Google_Client();
            $gClient->setApplicationName('');
            $gClient->setClientId("");
            $gClient->setClientSecret("");
            $gClient->setRedirectUri(WWW_LINK_ABSOLUTE . "/api/auth/social/googleresponse");
            $gClient->addScope(Google_Service_Oauth2::USERINFO_PROFILE);
            $gClient->addScope(Google_Service_Oauth2::USERINFO_EMAIL);
            $gClient->setAccessType('offline');        // offline access
            $gClient->setIncludeGrantedScopes(true);   // incremental auth

            $objOAuthService = new Google_Service_Oauth2($gClient);

            if (isset($_GET['code'])) {
                $gClient->authenticate($_GET['code']);
                $_SESSION['access_token'] = $gClient->getAccessToken();
                $loginUrl = WWW_LINK_ABSOLUTE . "/api/auth/social/googleresponse";
            } else {
                $loginUrl = $gClient->createAuthUrl();
            }

            $response["redirect"] = $loginUrl;
        }

        return $response;
    }

    public function googleresponse()
    {
        $gClient = new Google_Client();
        $gClient->setApplicationName('Google Login Paginemediche');
        $gClient->setClientId("1018601441894-hgjjv2f0d5cocifsqhh08e25b7nib2cc.apps.googleusercontent.com");
        $gClient->setClientSecret("L-X5vojwXGS9hCEw0TAogl62");
        $gClient->setRedirectUri(WWW_LINK_ABSOLUTE . "/api/auth/social/googleresponse");
        $gClient->addScope(Google_Service_Oauth2::USERINFO_PROFILE);
        $gClient->addScope(Google_Service_Oauth2::USERINFO_EMAIL);
        $gClient->setAccessType('offline');        // offline access
        $gClient->setIncludeGrantedScopes(true);   // incremental auth

        $objOAuthService = new Google_Service_Oauth2($gClient);

        if (isset($_GET['code'])) {
            $gClient->authenticate($_GET['code']);
            $_SESSION['access_token'] = $gClient->getAccessToken();
        }
        if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
            $gClient->setAccessToken($_SESSION['access_token']);
        }

        if ($gClient->getAccessToken()) {
            $arrToken = $gClient->getAccessToken();

            $userData = $objOAuthService->userinfo->get();

            if (!empty($userData)) {
                $arrAnagraph["name"] = $userData->givenName;
                $arrAnagraph["surname"] = $userData->familyName;
                $arrAnagraph["email"] = $userData->email;

                switch ($userData->gender) {
                    case "male":
                        $arrAnagraph["sesso"] = "M";
                        break;
                    case "female":
                        $arrAnagraph["sesso"] = "F";
                        break;
                    default:
                        $arrAnagraph["sesso"] = "";
                }

            }
        } else {
            $config["social"]["error"] = "Error during registration";
            $response = Auth::widget("error", $config);
            $response["status"] = 400;
            $response["type"] = 'html';
            $response["js"] = 'setTimeout(function(){ window.close();  }, 1500);';
        }


        if (is_array($arrAnagraph) && count($arrAnagraph)) {
            $anagraphObject = Anagraph::getInstance();
            $arrAnagraphList = $anagraphObject->read(
                array(
                    "anagraph.status"
                    , "anagraph.ID"
                    , "anagraph.ID_user"
                ), array(
                    "anagraph.email" => $arrAnagraph["email"]
                    , "anagraph.ID_domain" => 1
                )
            );

            if (is_array($arrAnagraphList) && count($arrAnagraphList)) {
                $ID_user = $arrAnagraphList[0]["ID_user"];
                $token = getTokenLocalDB($ID_user);

                if (!$arrAnagraphList[0]["status"]) {
                    updateUserInfo($ID_user, "activate", 1);
                }

                saveLocalSocialToken($arrToken["access_token"], $ID_user, "google");

                Auth::getInstance("session")->create($ID_user);
                $response = Auth::widget("welcome");
                $response["type"] = 'html';
                $response["html"] = '<div class="welcome-box-social">' . $response["html"] . '</div>';
                if($_SERVER["HTTP_REFERER"] == "/" || strpos($_SERVER["HTTP_REFERER"], "/login")) {
                    $response["js"] = 'if(window.opener  == null) { window.location.href=\'/area-utente\'} else {setTimeout(function(){ window.opener.location.href=\'' . WWW_LINK_ABSOLUTE . '/area-utente\'; window.close();   }, 1500); }';
                } else {
                    $response["js"] = 'if(window.opener  == null) { window.location.href=\'' . $_SERVER["HTTP_REFERER"] .'\'} else {setTimeout(function(){ window.opener.location.reload(); window.close();   }, 1500); }';
                }
                $response["css"] = '.welcome-box-subtitle,.welcome-box-title{font-family:Raleway,sans-serif}body{background:-webkit-linear-gradient(45deg,#e74c3c 0,#df2472 100%)!important}.welcome-box-social{max-width:400px;text-align:center;margin:40px auto;min-height:calc(100vh - 80px);background-color:#fff;padding:30px}.welcome-box-title{font-size:22px;margin-bottom:15px}.welcome-box-subtitle{font-size:18px;margin-bottom:2rem;text-align:center}.welcome-box-avatar{text-align:center}.welcome-box-avatar IMG{width:200px;height:200px;margin:0 auto}';
            } else {
                $socialRegistration = $this->socialregisteruser($arrAnagraph, "google");

                if($socialRegistration["ID_user"] > 0) {
                    Auth::getInstance("session")->create($socialRegistration["ID_user"]);
                    $response = Auth::widget("welcome");
                    $response["type"] = 'html';
                    $response["html"] = '<div class="welcome-box-social">' . $response["html"] . '</div>';
                    if($_SERVER["HTTP_REFERER"] == "/" || strpos($_SERVER["HTTP_REFERER"], "/login")) {
                        $response["js"] = 'if(window.opener  == null) { window.location.href=\'/area-utente\'} else {setTimeout(function(){ window.opener.location.href=\'' . WWW_LINK_ABSOLUTE . '/area-utente\'; window.close();   }, 1500); }';
                    } else {
                        $response["js"] = 'if(window.opener  == null) { window.location.href=\'' . $_SERVER["HTTP_REFERER"] .'\'} else {setTimeout(function(){ window.opener.location.reload(); window.close();   }, 1500); }';
                    }
                    $response["css"] = '.welcome-box-subtitle,.welcome-box-title{font-family:Raleway,sans-serif}body{background:-webkit-linear-gradient(45deg,#e74c3c 0,#df2472 100%)!important}.welcome-box-social{max-width:400px;text-align:center;margin:40px auto;min-height:calc(100vh - 80px);background-color:#fff;padding:30px}.welcome-box-title{font-size:22px;margin-bottom:15px}.welcome-box-subtitle{font-size:18px;margin-bottom:2rem;text-align:center}.welcome-box-avatar{text-align:center}.welcome-box-avatar IMG{width:200px;height:200px;margin:0 auto}';
                } else {
                    $config["social"]["error"] = "Error during registration";
                    $response = Auth::widget("error", $config);
                    $response["status"] = 400;
                    $response["type"] = 'html';
                    $response["js"] = 'setTimeout(function(){ window.close();  }, 1500);';
                }
            }
        }
        return $response;
    }

    public function socialregisteruser($arrInfo, $referer = "") {
        $ID_user = 0;
        $arrInfo["referer"] = $referer;
        $resultRegister = registerUser($arrInfo);

        if($resultRegister["result"] == "success") {
            $ID_user = $resultRegister["ID_user"];
            updateUserInfo($resultRegister["ID_user"], "activate", 1);
        }

        return array("ID_user" => $ID_user, "token" => $resultRegister["token"]);
    }
}


