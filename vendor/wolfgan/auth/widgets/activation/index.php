<?php
/**
 *   VGallery: CMS based on FormsFramework
Copyright (C) 2004-2015 Alessandro Stucchi <wolfgan@gmail.com>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

 * @package VGallery
 * @subpackage core
 * @author Alessandro Stucchi <wolfgan@gmail.com>
 * @copyright Copyright (c) 2004, Alessandro Stucchi
 * @license http://opensource.org/licenses/gpl-3.0.html
 * @link https://github.com/wolfgan43/vgallery
 */

    $config_default = array(

    );
    $extension = pathinfo($_SERVER['SERVER_NAME'], PATHINFO_EXTENSION);
    $config = array_replace_recursive($config_default, (array) $config);

    $request = Auth::getRequest();
    if(isset($request["rawdata"]["email"]) && $request["rawdata"]["email"] && !filter_var($request["rawdata"]["email"], FILTER_VALIDATE_EMAIL)) {
        Cms::redirect($_SERVER["PATH_INFO"]);
    }

    if($_SERVER["REQUEST_METHOD"] == "POST") {
        $type = "utente";
        cmCache_writeLog(array("created" => time(), "host" => $_SERVER, "username" => $_POST["username"], "POST" => $_POST), "activation");
        if(isset($_POST["t"])) {
            $response = null;
            $secret = "activation" . "-" . $_POST["username"] . '-' . $_POST["t"];
            $sid = Auth::getInstance("token")->create($secret);

            $return = checkSid($_POST["username"], $sid, "activation", $type);
            if ($return["result"] == "success") {
                $token = getTokenLocalDB($return["user"]["ID"]);
                if (strlen($token)) {
                    $auth = Auth::getInstance("session")->create($return["user"]["ID"]);
                    updateUserInfo($return["user"]["ID"], "activate", 1);

                    $response = array(
                        "status" => "0"
                    , "redirect" => true
                    , "error" => ""
                    );
                    $response = array_replace($response, (array)call_user_func_array($config["callback"]["activation"], array($response)));
                    if ($_REQUEST["redirect"]) {
                        $response["redirect"] = $_REQUEST["redirect"];
                    }
                }
            }
            if (!$response) {
                $response = array(
                    "status" => "400"
                    , "error" => "Error during activation"
                );
            }
        } else {
             $response = Auth::request($_POST["username"], array("scopes" => "activation", "type" => $_POST["type"], "referral" => $_POST["referer"]));

             if(strlen($response["code"])) {
                $secret = "activation" . "-" . $_POST["username"] . '-' . $response["code"];
                $sid = Auth::getInstance("token")->create($secret);
                $return = setSid($_POST["username"], $sid, $response["code"], "activation", $type);

                if ($return["result"] == "success") {
                    $response["code"] = "";
                    $response["status"] = "0";
                    $response["t"] = Auth::getInstance("token")->create();
                } else {
                    $response["status"] = "400";
                    $response["error"] = $return["message"];

                }

            }
        }
        Api::send($response);
    } else {
        $path = Auth::_getDiskPath("tpl") . ($config["tpl_path"]
                ? $config["tpl_path"]
                : "/activation"
            );
        $html_name = "/index.html";
        $css_name = "/style.css";
        $script_name = "/script.js";

        if(!is_file($file)) {
            $file = __DIR__ . $html_name;
        }

        $filename = (is_file($path . $html_name)
            ? $path . $html_name
            : __DIR__ . $html_name
        );
        $tpl = ffTemplate::factory(ffCommon_dirname($filename));
        $tpl->load_file(basename($filename), "main");

        if(isset($_REQUEST["email"]) && strlen($_REQUEST["email"])) {
            cmCache_writeLog(array("created" => time(), "host" => $_SERVER, "username" => $_REQUEST["email"]), "activation");

            $params = array("scopes" => "activation");
            $params["type"] = "utente";
            if(isset($_REQUEST["referer"])) {
                $params["referral"] = $_REQUEST["referer"];
            }
            $response = Auth::request($_REQUEST["email"], $params);

            if(strlen($response["code"])) {
                $secret = "activation" . "-" . $_REQUEST["email"] . '-' . $response["code"];
                $sid = Auth::getInstance("token")->create($secret);
                $return = setSid($_REQUEST["email"], $sid, $response["code"], "activation", $params["type"]);
                if ($return["result"] !== "success") {
                    if (isset($_REQUEST["error"]) && strlen($_REQUEST["error"])) {
                        $tpl->set_var("error", '<div class="callout alert">' . htmlspecialchars($_REQUEST["error"]) . '</div>');
                    }
                }
            } else {
                $tpl->set_var("error", '<div class="callout alert">' . htmlspecialchars($response["error"]) . '</div>');
            }

            $tpl->set_var("email_conferma", htmlspecialchars($_REQUEST["email"]));
            $tpl->set_var("email_class", "");
            $tpl->set_var("email_attr", " disabled");

            $tpl->set_var("bearer_code", $response["t"]);
            $tpl->parse("SezBearerContainer", false);
        } else {
            $tpl->set_var("email_class", "hide-code-string");
        }

        $token = Auth::password();
        $tpl->set_var("csrf_token", $token);

        $tpl->set_var("activation_url", $config["api"]["activation"]);

        if($config["domain"]) {
            $tpl->parse("SezDomain", false);
        } else {
            $tpl->set_var("domain_name", $_SERVER["HTTP_HOST"]);
            $tpl->parse("SezDomainHidden", false);
        }

        if(isset($_REQUEST["redirect"]) && strlen($_REQUEST["redirect"])) {
            $tpl->set_var("ret_url", "'" . $_REQUEST["redirect"] . "'");
        } else {
            $tpl->set_var("ret_url", "'/area-utente'");
        }

        $html = $tpl->rpparse("main", false);

        $css = file_get_contents(ffMedia::getFileOptimized(is_file($path . $css_name)
            ? $path . $css_name
            : __DIR__ . $css_name
        ));

        $js = file_get_contents(ffMedia::getFileOptimized(is_file($path . $script_name)
            ? $path . $script_name
            : __DIR__ . $script_name
        ));

        $output = array(
            "html"  => $html
            , "css" => $css
            , "js"  => $js
        );

        if(Auth::isXHR()) {
            Api::send($output);
        }
    }

return $output;
