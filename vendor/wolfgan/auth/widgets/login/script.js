/*
 * VGallery: CMS based on FormsFramework
 * Copyright (C) 2004-2015 Alessandro Stucchi <wolfgan@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  @package VGallery
 *  @subpackage core
 *  @author Alessandro Stucchi <wolfgan@gmail.com>
 *  @copyright Copyright (c) 2004, Alessandro Stucchi
 *  @license http://opensource.org/licenses/gpl-3.0.html
 *  @link https://github.com/wolfgan43/vgallery
 */

if(!hCore) var hCore = {
    auth : {}
};

jQuery.extend(true, hCore.auth, {
    login: function (url, selector, elem, redirect, type) {
        var selectorID = (selector
            ? "#" + selector
            : document
        );

        var domain = jQuery(selectorID).find("INPUT[name='domain']").val() || window.location.host;
        var username = jQuery(selectorID).find("INPUT[name='username']").val() || undefined;
        var password = jQuery(selectorID).find("INPUT[name='password']").val() || undefined;
        var token = jQuery(selectorID).find("INPUT[name='csrf']").val() || "";
        var stayConnect = jQuery(selectorID).find("INPUT[name='stayconnected-new']").is(':checked') || false;
        var redirectDef = redirect || ff.getURLParameter("redirect");


        if(redirectDef == 'null' || redirectDef == '') {
            var lastPage = window.location.pathname;

            if(lastPage.indexOf("/login") >= 0) {
                redirectDef = "/user";
            } else if(window.location.pathname != '/') {
                redirectDef = "";
            } else {
                redirectDef = "/user";
            }
        }

        redirectDef = decodeURIComponent(redirectDef);

        if (!url)
            url = '/login';

        if (!jQuery(selectorID + " .error-container").length) {
            jQuery(selectorID).prepend('<div class="error-container" />');
        }

        $.ajax({
            url: url,
            headers: {
                "domain": domain
                , "csrf": token
                , "refresh": stayConnect
            },
            method: 'POST',
            dataType: 'json',
            data: {
                "username": username
                , "password": password
                , "redirect": redirectDef
                , "type": type
            }
        })
            .done(function (data) {
                if (data.status == "0") {
                    if(redirectDef.indexOf("showSerenaResult") > 0) {
                        console.log("Fine chatbot infermedica - guest login");
                        if (typeof (ga) !== 'undefined') {
                            ga(function () {
                                var tracker = ga.getAll()[0];
                                if (tracker) {
                                    tracker.send('event', "serena", "Fine chatbot infermedica - guest login", window.location.href);
                                }
                            });
                        }
                    }
                    if (data.welcome) {
                        if (data.welcome.css.length) {
                            var style = document.createElement("style");
                            style.innerHTML = data.welcome.css;
                            document.head.appendChild(style);
                        }
                        if (data.welcome.js.length) {
                            var script = document.createElement("script");
                            script.innerHTML = data.welcome.js;
                            document.head.appendChild(script);
                        }
                        if (data.welcome.html.length) {
                            if(typeof data.redirect !== "undefined" && data.redirect !== "") {
                                if(redirectDef) {
                                    if(redirectDef.indexOf("#") > 0) {
                                        setTimeout(function() {
                                            window.location.replace(redirectDef);
                                            window.location.reload();
                                        }, 1000);
                                    } else {
                                        window.location.href = redirectDef;
                                    }


                                } else {
                                    window.location.reload();
                                };
                            } else {
                                window.location.reload();
                            }

                            jQuery(".login-page-doctor.doctor-action,.action-end-container").remove();
                            jQuery(selectorID + " .login-inner-container").html(data.welcome.html);
                        }
                    } else {
                        if(typeof data.redirect !== "undefined" && data.redirect !== "") {
                            window.location.href = data.redirect;
                        } else {
                            if(redirectDef) {
                                window.location.href = redirectDef;
                            } else {
                                window.location.reload();
                            }
                        }
                    }
                } else {
                    jQuery(elem).find(".disabled").css({
                        'opacity': '',
                        'pointer-events': ''
                    }).removeClass("disabled");
                    if (jQuery(selectorID + " .error-container").length) {
                        jQuery(selectorID + " .error-container").html('<div class="callout alert">' + data.error + '</div>');
                    }
                }
            })
            .fail(function (data) {
                jQuery(elem).find(".disabled").css({
                    'opacity': '',
                    'pointer-events': ''
                }).removeClass("disabled");
                if (jQuery(selectorID + " .error-container").length) {
                    jQuery(selectorID + " .error-container").html('<div class="callout alert">' + 'Service Temporary not Available. Try Later.' + '</div>');
                }
            });

        return false;
    },
    social: function (title, url) {
        if(0 && url == "/api/auth/social/facebook") {
            FB.login(function(response) {
              if (response.status === 'connected') {
                window.location.href= url;
              } else {
                FB.login();
              }
            });
        } else {
            var social_window = undefined;
            social_window = window.open(
                url
                , title
                , "menubar=no, status=no, height=500, width= 500"
            );
        }
    },
    reloadPageBySocialLogin : function(ret_url) {
        var lastPage = window.opener.location.pathname;
        if(ret_url) {
            if(ret_url.indexOf("/login") >= 0 || ret_url == '/') {
                window.opener.location = "/user";
            } else {
                window.opener.location = ret_url;
            }
        } else {
            if(lastPage.indexOf("/login") >= 0 || lastPage == '/') {
                window.opener.location = "/user";
            } else {
                window.opener.location.reload();
            }
        }
    
        window.close(); 
    },
    submitProcessKey : function(e, button) {
        if (null == e)
            e = window.event;
        if (e.keyCode === 13)  {
            document.getElementById(button).focus();
            document.getElementById(button).click();
            return false;
        }
    }
});