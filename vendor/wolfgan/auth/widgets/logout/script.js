if(!hCore) var hCore = {
    auth : {}
};

jQuery.extend(true, hCore.auth, {
    logout: function (url, selector, elem) {
        var selectorID = (selector
                ? "#" + selector
                : document
        );

        if (!url)
            url = '/login';
        var redirect = ff.getURLParameter("redirect");

        if(redirect == 'null') {
            if(window.location.pathname == '/login' || window.location.pathname == '/login/') {
                redirect = "";
            } else {
                redirect = "/";
            }
        }

        $.ajax({
            url: url,
            headers: {},
            method: 'POST',
            dataType: 'json',
            data: {}
        }).done(function(data) {
            if (data.status === "0") {
                if(redirect.length) {
                    window.location.href = redirect
                } else {
                    window.location.href = "/";
                }
            } else {
                jQuery(elem).find(".disabled").css({
                    'opacity': '',
                    'pointer-events': ''
                }).removeClass("disabled");
                if (jQuery(selectorID + " .error-container").length) {
                    jQuery(selectorID + " .error-container").html('<div class="callout alert">' + data.error + '</div>');
                }
            }
        })
        .fail(function(data) {
            if (jQuery(selectorID + " .error-container").length) {
                jQuery(selectorID + " .error-container").html('<div class="callout alert">' + data.error + '</div>');
            }
        });

        return false;
    }
});