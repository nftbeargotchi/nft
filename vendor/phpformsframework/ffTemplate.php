<?php
/**
* @package Forms PHP Framework
* @category Common Functions Class
* @desc ffTemplate.php - Forms Framework Template Engine
* @author Samuele Diella <samuele.diella@gmail.com>
* @copyright Copyright &copy; 2004-2016, Samuele Diella
* @license https://opensource.org/licenses/LGPL-3.0
* @link http://www.formsphpframework.com
* @version v2, alpha
* @since v1, alpha 1
*/


if (!defined("FF_LOCALE")) {
    define("FF_LOCALE", "ITA");
}
if (!defined("FF_ENABLE_MULTILANG")) {
    define("FF_ENABLE_MULTILANG", true);
}
if (!defined("FF_ENABLE_MEM_TPL_CACHING")) {
    define("FF_ENABLE_MEM_TPL_CACHING", false);
}
if (!defined("FF_TEMPLATE_ENABLE_TPL_JS")) {
    define("FF_TEMPLATE_ENABLE_TPL_JS", false);
}

/**
* @desc ffTemplate è la classe preposta alla gestione dei template
* @author Samuele Diella <samuele.diella@gmail.com>
* @version v4, alpha
* @since v1, alpha 1
*/

class ffTemplate
{
    const REGEXP                            = '/\{([\w\:\=\-\|\.]+)\}/U';
    const APPLET                            = '/\{\[(.+)\]\}/U';
    const COMMENTHTML                       = '/\{\{([\w\[\]\:\=\-\|\.]+)\}\}/U';
    const LANG                              = FF_LOCALE;

    public $root_element						= "main";

    public $BeginTag							= "Begin";
    public $EndTag								= "End";

    public $debug_msg							= false;
    public $display_unparsed_sect				= false;
    public $doublevar_to_commenthtml 			= FF_TEMPLATE_ENABLE_TPL_JS;

    public $DBlocks 							= array();			// initial data: files and blocks
    public $ParsedBlocks 						= array();		// result data and variables
    public $DVars 								= array();
    public $DApplets							= null;
    public $DBlockVars 						= array();

    public $template_root;
    public $sTemplate;

    public $minify								= false; /* can be: false, strip, strong_strip, minify
                                                  NB: minify require /library/minify (set CM_CSSCACHE_MINIFIER and CM_JSCACHE_MINIFIER too) */
    public $compress							= false;

    // FF enabled settings (u must have FF and use ::factory()
    public $force_mb_encoding					= "UTF-8"; // false or UTF-8 (require FF)

    public $events 						= null;
    protected static $_events				= null;

    // MultiLang SETTINGS
    public $MultiLang							= FF_ENABLE_MULTILANG; // enable support (require class ffDB_Sql)

    // PRIVATES
    private $useFormsFramework				= false;
    private $use_cache		                = FF_ENABLE_MEM_TPL_CACHING;

    // COMMON CHECKS
    public function __set($name, $value)
    {
        if ($this->useFormsFramework) {
            ffErrorHandler::raise("property \"$name\" not found on class " . __CLASS__, E_USER_ERROR, $this, get_defined_vars());
        } else {
            die("property \"$name\" not found on class " . __CLASS__);
        }
    }

    public function __get($name)
    {
        if ($this->useFormsFramework) {
            ffErrorHandler::raise("property \"$name\" not found on class " . __CLASS__, E_USER_ERROR, $this, get_defined_vars());
        } else {
            die("property \"$name\" not found on class " . __CLASS__);
        }
    }

    public function __isset($name)
    {
        if ($this->useFormsFramework) {
            ffErrorHandler::raise("property \"$name\" not found on class " . __CLASS__, E_USER_ERROR, $this, get_defined_vars());
        } else {
            die("property \"$name\" not found on class " . __CLASS__);
        }
    }

    public function __unset($name)
    {
        if ($this->useFormsFramework) {
            ffErrorHandler::raise("property \"$name\" not found on class " . __CLASS__, E_USER_ERROR, $this, get_defined_vars());
        } else {
            die("property \"$name\" not found on class " . __CLASS__);
        }
    }

    public function __call($name, $arguments)
    {
        if ($this->useFormsFramework) {
            ffErrorHandler::raise("function \"$name\" not found on class " . __CLASS__, E_USER_ERROR, $this, get_defined_vars());
        } else {
            die("function \"$name\" not found on class " . __CLASS__);
        }
    }

    /*static public function __callStatic ($name, $arguments)
    {
        if ($this->useFormsFramework)
            ffErrorHandler::raise("function \"$name\" not found on class " . get_class($this), E_USER_ERROR, $this, get_defined_vars());
        else
            die("function \"$name\" not found on class " . get_class($this));
    }*/

    // STATIC EVENTS MANAGEMENT
    public static function addEvent($event_name, $func_name, $priority = null, $index = 0, $break_when = null, $break_value = null)
    {
        if (!class_exists("ffCommon", false)) {
            die(__CLASS__ . ": " . __FUNCTION__ . " method require Forms Framework");
        }

        self::initEvents();
        self::$_events->addEvent($event_name, $func_name, $priority, $index, $break_when, $break_value);
    }

    private static function doEvent($event_name, $event_params = array())
    {
        self::initEvents();
        return self::$_events->doEvent($event_name, $event_params);
    }

    private static function initEvents()
    {
        if (self::$_events === null) {
            self::$_events = new ffEvents();
        }
    }

    /**
     * This method istantiate a ffTemplate instance based on dir path. When using this
     * function, the resulting object will deeply use Forms Framework.
     *
     * @param string $template_root
     * @return ffTemplate
     */
    public static function factory($template_root)
    {
        $tmp = new ffTemplate($template_root);
        if (class_exists("ffCommon", false)) {
            $res = self::doEvent("on_factory", array($template_root));

            $tmp->useFormsFramework = true;
            $tmp->events = new ffEvents();

            $res = self::doEvent("on_factory_done", array($tmp));
        }
        return $tmp;
    }

    // CONSTRUCTOR
    public function __construct($template_root)
    {
        $this->template_root = $template_root;
    }

    public function load_file($filename, $root_element = null)
    {
        if ($root_element !== null) {
            $this->root_element = $root_element;
        }

        $this->sTemplate = $filename;
        if (substr($filename, 0, 1) != "/") {
            $filename = "/" . $filename;
        }
        $template_path = $this->template_root . $filename;

        if ($this->useFormsFramework && $this->use_cache) {
            $cache = ffCache::getInstance();
            $res = $cache->get($template_path, "/ff/template");
        }

        if (!$res) {
            $this->DBlocks[$this->root_element] = @file_get_contents($template_path);
            if ($this->DBlocks[$this->root_element] !== false) {
                if ($this->useFormsFramework && $this->force_mb_encoding !== false) {
                    $this->DBlocks[$this->root_element] = ffCommon_charset_encode($this->DBlocks[$this->root_element], $this->force_mb_encoding);
                }

                $this->getDVars();
                $nName = $this->NextDBlockName($this->root_element);
                while ($nName != "") {
                    $this->SetBlock($this->root_element, $nName);
                    $this->blockVars($nName);
                    $nName = $this->NextDBlockName($this->root_element);
                }
            } else {
                if ($this->useFormsFramework) {
                    ffErrorHandler::raise("Unable to find the template", E_USER_ERROR, null, get_defined_vars());
                } else {
                    die("<br><b><u><font color=\"red\">Unable to find the template</font></u></b><br>");
                }
            }

            if ($this->useFormsFramework && $this->use_cache) {
                $cache->set($template_path, array("DBlocks" => $this->DBlocks, "DVars" => $this->DVars, "DBlockVars" => $this->DBlockVars), "/ff/template");
            }
        } else {
            $this->DBlocks = $res["DBlocks"];
            $this->DVars = $res["DVars"];
            $this->DBlockVars = $res["DBlockVars"];
        }

        if ($this->useFormsFramework) {
            $res = self::$_events->doEvent("on_loaded_data", array($this));
        }
    }

    public function load_content($content, $root_element = null)
    {
        if ($root_element !== null) {
            $this->root_element = $root_element;
        }

        $nName = "";

        $this->DBlocks[$this->root_element] = $content;
        $this->getDVars();
        $nName = $this->NextDBlockName($this->root_element);
        while ($nName != "") {
            $this->SetBlock($this->root_element, $nName);
            $nName = $this->NextDBlockName($this->root_element);
        }

        $res = self::$_events->doEvent("on_loaded_data", array($this));
    }

    public function getDVars()
    {
        if ($this->doublevar_to_commenthtml) {
            $this->DBlocks[$this->root_element] = preg_replace('/\{\{([\w\[\]\:\=\-\|\.]+)\}\}/U', "<!--{\{$1\}\}-->", $this->DBlocks[$this->root_element]);// str_replace(array("{{", "}}"), array("<!--", "-->"), $this->DBlocks[$this->root_element]);
        }

        $matches = null;
        $rc = preg_match_all(ffTemplate::REGEXP, $this->DBlocks[$this->root_element], $matches);
        if ($rc && $matches) {
            $this->DVars = array_flip($matches[1]);
        }
    }

    public function getDApplets()
    {
        if (!$this->DApplets) {
            $matches = null;
            $rc = preg_match_all(ffTemplate::APPLET, $this->DBlocks[$this->root_element], $matches);
            if ($rc && $matches) {
                $applets = $matches[1];
                if (is_array($applets) && count($applets)) {
                    foreach ($applets as $applet) {
                        if (strpos($applet, "{") !== false) {
                            $matches = null;
                            $rc = preg_match_all(ffTemplate::REGEXP, $applet, $matches);
                            if ($rc && $matches) {
                                $applet = str_replace($matches[0], array_intersect_key($this->ParsedBlocks, array_flip($matches[1])), $applet);
                            }
                        }

                        $this->setApplet($applet);
                    }
                }
            }

            $matches = null;
            $rc = preg_match_all(ffTemplate::APPLET, implode(" ", $this->ParsedBlocks), $matches);
            if ($rc && $matches) {
                $applets = $matches[1];
                if (is_array($applets) && count($applets)) {
                    foreach ($applets as $applet) {
                        $this->setApplet($applet);
                    }
                }
            }
        }

        return $this->DApplets;
    }

    private function setApplet($applet)
    {
        $arrApplet = explode(":", $applet, 2);
        $appletid = "[" . $applet . "]";
        $this->DApplets[$appletid] = array();
        $this->DApplets[$appletid]["name"] = $arrApplet[0];

        parse_str(str_replace(":", "&", $arrApplet[1]), $this->DApplets[$appletid]["params"]);
    }

    public function NextDBlockName($sTemplateName)
    {
        $sTemplate = $this->DBlocks[$sTemplateName];
        $BTag = strpos($sTemplate, "<!--" . $this->BeginTag);
        if ($BTag === false) {
            return "";
        } else {
            $ETag = strpos($sTemplate, "-->", $BTag);
            $sName = substr($sTemplate, $BTag + 9, $ETag - ($BTag + 9));
            if (strpos($sTemplate, "<!--" . $this->EndTag . $sName . "-->") > 0) {
                return $sName;
            } else {
                return "";
            }
        }
    }


    public function SetBlock($sTplName, $sBlockName)
    {
        if (!isset($this->DBlocks[$sBlockName])) {
            $this->DBlocks[$sBlockName] = $this->getBlock($this->DBlocks[$sTplName], $sBlockName);
        }

        $this->DBlocks[$sTplName] = $this->replaceBlock($this->DBlocks[$sTplName], $sBlockName);

        $nName = $this->NextDBlockName($sBlockName);
        while ($nName != "") {
            $this->SetBlock($sBlockName, $nName);
            $nName = $this->NextDBlockName($sBlockName);
        }
    }

    public function getBlock($sTemplate, $sName)
    {
        $alpha = strlen($sName) + 12;

        $BBlock = strpos($sTemplate, "<!--" . $this->BeginTag . $sName . "-->");
        $EBlock = strpos($sTemplate, "<!--" . $this->EndTag . $sName . "-->");

        if ($BBlock === false || $EBlock === false) {
            return "";
        } else {
            return substr($sTemplate, $BBlock + $alpha, $EBlock - $BBlock - $alpha);
        }
    }


    public function replaceBlock($sTemplate, $sName)
    {
        $BBlock = strpos($sTemplate, "<!--" . $this->BeginTag . $sName . "-->");
        $EBlock = strpos($sTemplate, "<!--" . $this->EndTag . $sName . "-->");

        if ($BBlock === false || $EBlock === false) {
            return $sTemplate;
        } else {
            return substr($sTemplate, 0, $BBlock) . "{" . $sName . "}" . substr($sTemplate, $EBlock + strlen("<!--End" . $sName . "-->"));
        }
    }

    public function GetVar($sName)
    {
        return $this->DBlocks[$sName];
    }

    public function set_var($sName, $sValue)
    {
        $this->ParsedBlocks[$sName] = $sValue;
        if (isset($this->DVars[$sName]) || isset($this->DBlocks[$sName])) {
            return true;
        } else {
            return false;
        }
    }

    public function isset_var($sName)
    {
        if (isset($this->DVars[$sName]) || isset($this->DBlocks[$sName])) {
            return true;
        } else {
            return false;
        }
    }

    public function set_regexp_var($sPattern, $sValue)
    {
        $rc = false;
        $tmp = array_keys($this->ParsedBlocks);
        foreach ($tmp as $key => $value) {
            if (preg_match($sPattern, $value)) {
                $rc = true;
                $this->ParsedBlocks[$value] = $sValue;
            }
        }
        return $rc;
    }

    public function parse_regexp($sPattern, $sValue)
    {
        $rc = false;
        $tmp = array_keys($this->DBlocks);
        foreach ($tmp as $key => $value) {
            if (preg_match($sPattern, $value)) {
                $rc = true;
                $this->parse($value, $sValue);
            }
        }
        return $rc;
    }

    public function print_var($sName)
    {
        echo $this->ParsedBlocks[$sName];
    }

    public function parse($sTplName, $bRepeat, $bBefore = false)
    {
        if (isset($this->DBlocks[$sTplName])) {
            if ($bRepeat && isset($this->ParsedBlocks[$sTplName])) {
                if ($bBefore) {
                    $this->ParsedBlocks[$sTplName] = $this->ProceedTpl($sTplName) . $this->ParsedBlocks[$sTplName];
                } else {
                    $this->ParsedBlocks[$sTplName] .= $this->ProceedTpl($sTplName);
                }
            } else {
                $this->ParsedBlocks[$sTplName] = $this->ProceedTpl($sTplName);
            }

            return true;
        } elseif ($this->debug_msg) {
            echo "<br><b>Block with name <u><font color=\"red\">$sTplName</font></u> does't exist</b><br>";
        }

        return false;
    }

    public function pparse($block_name, $is_repeat)
    {
        $ret = $this->rpparse($block_name, $is_repeat);

        if ($this->compress) {
            ffTemplate::http_compress($ret);
        } else {
            echo $ret;
        }
    }

    public function rpparse($block_name, $is_repeat)
    {
        $this->parse($block_name, $is_repeat);
        return $this->getBlockContent($block_name);
    }

    public function getBlockContent($block_name, $minify = null)
    {
        $minify = ($minify === null ? $this->minify : $minify);

        if ($minify === false) {
            return $this->entities_replace($this->ParsedBlocks[$block_name]);
        } elseif ($minify === "strip") {
            return $this->entities_replace(preg_replace("/\n\s*/", "\n", $this->ParsedBlocks[$block_name], -1, $count));
        } elseif ($minify === "strong_strip") {
            return $this->entities_replace(preg_replace(
                array(
                            '/\>[^\S ]+/s',  // strip whitespaces after tags, except space
                            '/[^\S ]+\</s',  // strip whitespaces before tags, except space
                            '/(\s)+/s'       // shorten multiple whitespace sequences
                        ),
                array(
                            '>',
                            '<',
                            '\\1'
                        ),
                $this->ParsedBlocks[$block_name],
                -1,
                $count
                    ));
        } elseif ($minify === "minify") {
            if (!class_exists("Minify_HTML")) {
                require FF_DISK_PATH . '/library/minify/min/lib/Minify/HTML.php';
            }
            if (!class_exists("CSSmin")) {
                require FF_DISK_PATH . '/library/minify/min/lib/CSSmin.php';
            }
            if (!class_exists("JSMin")) {
                require FF_DISK_PATH . '/library/minify/min/lib/JSMin.php';
            }

            return $this->entities_replace(str_replace(
                chr(10),
                " ",
                Minify_HTML::minify(
                $this->ParsedBlocks[$block_name],
                array(
                            "cssMinifier" => "CSSmin::_minify"
                            , "jsMinifier" => "JSMin::minify"
                            , "jsCleanComments" => true
                        )
            )
                    ));
        } elseif ($minify === "yui") {
            if (!class_exists("Minify_HTML")) {
                require FF_DISK_PATH . '/library/minify/min/lib/Minify/HTML.php';
            }
            if (!class_exists("Minify_YUICompressor")) {
                require(FF_DISK_PATH . "/library/gminify/YUICompressor.php");
            }
            Minify_YUICompressor::$jarFile = FF_DISK_PATH . "/library/gminify/yuicompressor-2.4.8.jar";
            if (!file_exists(CM_JSCACHE_DIR)) {
                @mkdir(CM_JSCACHE_DIR, 0777, true);
            }
            Minify_YUICompressor::$tempDir = CM_JSCACHE_DIR;
            return $this->entities_replace(str_replace(
                chr(10),
                " ",
                Minify_HTML::minify(
                $this->ParsedBlocks[$block_name],
                array(
                            "cssMinifier" => "Minify_YUICompressor::minifyCss"
                            , "jsMinifier" => "Minify_YUICompressor::minifyJs"
                        )
            )
                    ));
        } else {
            if ($this->useFormsFramework) {
                ffErrorHandler::raise("Unknown minify method", E_USER_ERROR, $this, get_defined_vars());
            } else {
                die("Unknown minify method");
            }
        }
    }

    public static function http_compress($data, $output_result = true, $method = null, $level = 9)
    {
        if ($method === null) {
            $encodings = array_flip(explode(",", $_SERVER["HTTP_ACCEPT_ENCODING"]));
            if (isset($encodings["gzip"])) { // better gzip
                $method = "gzip";
            } elseif (isset($encodings["deflate"])) {
                $method = "deflate";
            }
        }

        if ($method == "deflate") {
            if ($output_result) {
                header("Content-Encoding: deflate");
                echo gzdeflate($data, $level);
            /*gzcompress($this->tpl[0]->rpparse("main", false), 9);
            gzencode($this->tpl[0]->rpparse("main", false), 9, FORCE_DEFLATE);
            gzencode($this->tpl[0]->rpparse("main", false), 9, FORCE_GZIP);*/
            } else {
                return array(
                    "method" => "deflate"
                    , "data" => gzdeflate($data, $level)
                );
            }
        } elseif ($method == "gzip") {
            if ($output_result) {
                header("Content-Encoding: gzip");
                echo gzencode($data, $level);
            } else {
                return array(
                    "method" => "gzip"
                    , "data" => gzencode($data, $level)
                );
            }
        } else {
            if ($output_result) {
                echo $data;
            } else {
                return array(
                    "method" => null
                    , "data" => $data
                );
            }
        }
    }

    public function blockVars($sTplName)
    {
        if (isset($this->DBlockVars[$sTplName])) {
            return $this->DBlockVars[$sTplName];
        }

        $sTpl = $this->DBlocks[$sTplName];

        $matches = array();
        $rc = preg_match_all(ffTemplate::REGEXP, $sTpl, $matches);
        if ($rc) {
            $vars = $matches[1];

            // --- AUTOMATIC LANGUAGE LOOKUP FOR INTERNATIONALIZATION
            if ($this->useFormsFramework) {
                foreach ($vars as $nName) {
                    if (substr($nName, 0, 1) == "_") {
                        if ($this->MultiLang) {
                            $this->set_var($nName, $this->get_word_by_code(substr($nName, 1)));
                        } else {
                            $this->set_var($nName, "{" . substr($nName, 1) . "}");
                        }
                    }
                }
                reset($vars);
            }

            $this->DBlockVars[$sTplName] = $vars;

            return $vars;
        } else {
            return false;
        }
    }

    public function ProceedTpl($sTplName)
    {
        $vars = $this->blockVars($sTplName);
        $sTpl = $this->DBlocks[$sTplName];

        if ($vars) {
            $search_for = array();
            $replace_with = array();

            reset($vars);
            foreach ($vars as $key => $value) {
                $tmp = $this->ParsedBlocks[$value];
                if (is_object($tmp)) {
                    if ($this->useFormsFramework) {
                        ffErrorHandler::raise("bad value into template", E_USER_ERROR, $this, get_defined_vars());
                    } else {
                        die("bad value into template");
                    }
                }

                $search_for[] = "{" . $value . "}";
                if (isset($this->ParsedBlocks[$value])) {
                    $replace_with[] = $this->ParsedBlocks[$value];
                } elseif (isset($this->DBlocks[$value]) && $this->display_unparsed_sect) {
                    $replace_with[] = $this->DBlocks[$value];
                } else {
                    $replace_with[] = "";
                }
            }
            $sTpl = str_replace($search_for, $replace_with, $sTpl);
        }
        return $sTpl;
    }

    public function PrintAll()
    {
        $res = "<table border=\"1\" width=\"100%\">";
        $res .= "<tr bgcolor=\"#C0C0C0\" align=\"center\"><td>Key</td><td>Value</td></tr>";
        $res .= "<tr bgcolor=\"#FFE0E0\"><td colspan=\"2\" align=\"center\">ParsedBlocks</td></tr>";
        reset($this->ParsedBlocks);
        foreach ($this->ParsedBlocks as $key => $value) {
            $res .= "<tr><td><pre>" . ffCommon_specialchars($key) . "</pre></td>";
            $res .= "<td><pre>" . ffCommon_specialchars($value) . "</pre></td></tr>";
        }
        $res .= "<tr bgcolor=\"#E0FFE0\"><td colspan=\"2\" align=\"center\">DBlocks</td></tr>";
        reset($this->DBlocks);

        foreach ($this->DBlocks as $key => $value) {
            $res .= "<tr><td><pre>" . ffCommon_specialchars($key) . "</pre></td>";
            $res .= "<td><pre>" . ffCommon_specialchars($value) . "</pre></td></tr>";
        }
        $res .= "</table>";
        return $res;
    }

    public function entities_replace($text)
    {
        return str_replace(array("{\\","\\}"), array("{","}"), $text);
    }

    public function get_word_by_code($code, $language = self::LANG)
    {
        if ($this->useFormsFramework) {
            $res = $this->events->doEvent("on_get_word_by_code", array($code, $language, $this));
            $rc = end($res);
            if ($rc === null) {
                return ffTranslator::get_word_by_code($code, $language);
            }
            //return ffTemplate::_get_word_by_code($code, $language);
            else {
                return $rc;
            }
        } else {
            return "{" . $code . "}";
        }
    }

    public static function _get_word_by_code($code, $language = self::LANG)
    {
        return ffTranslator::get_word_by_code($code, $language);
    }
}
