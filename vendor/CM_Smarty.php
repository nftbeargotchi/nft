<?php
/**
 * Created by PhpStorm.
 * User: crumma
 * Date: 30/10/2017
 * Time: 10:11
 */

//require_once (FF_DISK_PATH . "/vendor/smarty/smarty/libs/Smarty.class.php");

class CM_Smarty extends Smarty
{
    public function __construct()
    {
        parent::__construct();

        //$this->Smarty();

        $this->template_dir = FF_DISK_PATH . '/cache/smarty/templates';
        $this->compile_dir  = FF_DISK_PATH . '/cache/smarty/templates_c';
        $this->config_dir   = FF_DISK_PATH . '/cache/smarty/configs';
        $this->cache_dir    = FF_DISK_PATH . '/cache/smarty/cache';

        $this->caching = false;

        $this->cache_lifetime = 10;
    }
}
