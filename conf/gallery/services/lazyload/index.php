<?php
// $db_gallery : access db object
// $globals : globals settings
// $actual_srv = params defined by system

if (isset($actual_srv["enable"]) && strlen($actual_srv["enable"])) {
    $cm->oPage->tplAddJs("lazysizes", "lazysizes.min.js", "/themes/site/javascript/lazysizes");
    //$cm->oPage->tplAddJs("lazybackground", "lazybackground.js", "/themes/site/javascript/lazysizes");
}