<?php
    // $db_gallery : access db object
    // $globals : globals settings
    // $actual_srv = params defined by system

    if ($actual_srv["enable"] && strlen($actual_srv["app_id"])) {
        $js = '_linkedin_partner_id = "3140778";
                window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
                window._linkedin_data_partner_ids.push(_linkedin_partner_id);
                (function(){var s = document.getElementsByTagName("script")[0];
                var b = document.createElement("script");
                b.type = "text/javascript";b.async = true;
                b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
                s.parentNode.insertBefore(b, s);})();';
        $oPage->tplAddJs("linkedinPixel", null, null, false, false, $js, false, "bottom");
        $globals->fixed_pre["body"][] = '<noscript><img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=' . $actual_srv["app_id"] . '&fmt=gif" /></noscript>';
    }
