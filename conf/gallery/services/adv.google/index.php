<?php
    // $db_gallery : access db object
    // $globals : globals settings
    // $actual_srv = params defined by system

    if ($actual_srv["enable"]) {
        if ($actual_srv["code"]) {
            $js_content = "
		        var e = document.createElement('script');
		        e.type = 'text/javascript'; 
		        e.rel = 'preconnect';
		        e.src = '//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js';
		        document.head.appendChild(e);";

        $js_content .= "
            (adsbygoogle = window.adsbygoogle || []).push({
                google_ad_client: '" . $actual_srv["code"] . "'
                , enable_page_level_ads: true
              });";
        $oPage->tplAddJs("adsbygoogle", null, null, false, $oPage->isXHR(), $js_content, true, "bottom");
        } else {
        $oPage->tplAddJs("adsbygoogle", "adsbygoogle.js", "https://pagead2.googlesyndication.com/pagead/js", false, $oPage->isXHR(), null, true, "bottom", true);

        }
    }
