<?php
    // $db_gallery : access db object
    // $globals : globals settings
    // $actual_srv = params defined by system

    if ($actual_srv["enable"] && strlen($actual_srv["app_id"])) {
        if(strpos($globals->user_path, "/medici-online/") !== 0) {
            $js = 'window.fbAsyncInit = function() {
                FB.init({
                  appId            : \'' . $actual_srv["app_id"] . '\',
                  autoLogAppEvents : true,
                  xfbml            : true,
                  version          : \'v3.2\'
                });
              };
            
              (function(d, s, id){
                 var js, fjs = d.getElementsByTagName(s)[0];
                 if (d.getElementById(id)) {return;}
                 js = d.createElement(s); js.id = id;
                 js.src = "https://connect.facebook.net/en_US/sdk.js";
                 fjs.parentNode.insertBefore(js, fjs);
               }(document, \'script\', \'facebook-jssdk\'));';

            $oPage->tplAddJs("facebookSDK", null, null, false, false, $js, false, "bottom");
        }
    }
