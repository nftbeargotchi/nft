<?php
    // $db_gallery : access db object
    // $globals : globals settings
    // $actual_srv = params defined by system
switch ($globals->user_path) {
    case "/new-login-test":
    case "/libretto-sanitario-coronavirus":
    case "/libretto-sanitario":
    case "/personal-area-noemi":
    case "/registrazione-slvsp":
    case "/thank-you-slvsp":
    case "/thank-you-slvsp-diario":
        break;
    default:
        $js = 'var pCookie; var mustFadeIn = false;
                
                function startCookiesTech() {}
                var showPrv=function() {
                    setTimeout(function () {
                        startCookiesTech();
                        window.cookieconsent.initialise({
                            onInitialise: function (status) {
                            },
                            onStatusChange: function (st) {
                                console.log(st);
                                startCookies();
                            },
                            palette: {
                                "popup": {
                                    "background": "#000"
                                },
                                "button": {
                                    "background": "#f1d600"
                                }
                            },
                            onHandle: function (i) {
                                var oObjC = [{ key: "", descr: "Tecnici (necessari)", descrest: "I cookie necessari aiutano a contribuire a rendere fruibile un sito web abilitando le funzioni di base come la navigazione della pagina e l\'accesso alle aree protette del sito. Il sito web non può funzionare correttamente senza questi cookie." }, { key: "mkt", descr: "Marketing", descrest:"I cookie per il marketing vengono utilizzati per monitorare i visitatori nei siti web. L\'intento è quello di visualizzare annunci pertinenti e coinvolgenti per il singolo utente e quindi quelli di maggior valore per gli editori e gli inserzionisti terzi." } ];
                                this.prepareDetailStatus(oObjC,\'Conferma\');
                            },
                            theme: "edgeless",
                            position: "bottom-left",
                            //revokable: 0,
                            content: {
                                "message": "<h3 class=\'cc-title\' style=\'color:#e74c3c\'>Questo sito utilizza Cookie</h3>Questo sito e alcune terze parti usano cookie o tecnologie simili per funzionalità tecniche e, con il tuo consenso, anche per altre finalità descritte nella Cookie Policy, quali misurare le performance, analizzare audience e migliorare i prodotti e servizi. Puoi liberamente prestare, rifiutare o revocare il tuo consenso in qualsiasi momento, personalizzando le tue preferenze. Cliccando sul pulsante \"Accetta tutti i cookie\" acconsenti all\'uso di tali tecnologie per tutte le finalità indicate. Ignorando questo banner proseguirai con solo cookie tecnici.  ",
                                "deny": "Solo Cookie Tecnici",
                                "allow": "Gestisci Preferenze <i class=\"fa fa-chevron-right\"></i>",
                                "dismiss": "Accetta tutti i Cookie",
                                "link": "maggiori info",
                                "href": "' . WWW_LINK_ABSOLUTE . "/privacy-policy" . '"
                            },
                            type: "opt-in"
                        }, function (popup) {
                            pCookie = popup;
                            if (mustFadeIn) pCookie.fadeIn();
                        }, function (err) {
                            console.error(err);
                        })
                    },1500)
                }
                showPrv();
                
                function startCookies() {
                    var okMkt = document.cookie.indexOf("cookieconsent_status_details=all") != -1 || document.cookie.indexOf("cookieconsent_status_details=mkt") != -1;
                    if (okMkt) {
                        if(typeof gtag !== \'undefined\') {
                            gtag (\'js\' , new Date ( ) ) ;
                            gtag (\'config\', \'AW-744344688\') ;
                        } 
                        if(typeof fbq !== \'undefined\') {
                            fbq(\'init\', \'531575800360122\');
                            fbq(\'track\', \'PageView\');
                        }
                    }
                    if(typeof(setOtherCookie) !== \'undefined\' ) {
                        setOtherCookie();
                    }
                }

            
                function resetCookies() {
                    pCookie.clearStatus();
                    location.reload();
            
                }
        ';
        $oPage->tplAddCss("cookiebar", "cookieconsent.min.css", DOMAIN_VISITAMI . "/styles");
        $oPage->tplAddJs("jquery.cookieBar", "cookieconsent.js", DOMAIN_VISITAMI . "/Scripts", false, false, null, false, "top");
        $oPage->tplAddJs("cookieBar", null, null, false, false, $js, false, "bottom");
}
