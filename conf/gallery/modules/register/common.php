<?php
function MD_register_on_check_after($component, $action)
{
    $cm = cm::getInstance();
    $db = ffDB_Sql::factory();

    if ($action) {
        $cm->doEvent("md_on_register_check_after", array($component, $action));

        switch ($action) {
            case "insert":
                if (isset($component->form_fields["billcf"]) && isset($component->form_fields["billpiva"])) {
                    if (!$component->form_fields["billcf"]->getValue() && !$component->form_fields["billpiva"]->getValue()) {
                        return ffTemplate::_get_word_by_code("piva_or_cf_need");
                    }
                }
    
                if (!$component->user_vars["disable_account_registration"]) {
                    if (isset($component->form_fields["username"])) {
                        if (strlen($component->form_fields["username"]->getValue())) {
                            $sSQL = "SELECT ID FROM anagraph WHERE username = " . $db->toSql($component->form_fields["username"]->value);
                            $db->query($sSQL);
                            if ($db->numRows() > 0) {
                                return ffTemplate::_get_word_by_code("username_not_unic_value");
                            }
                        }
                    }
                    if (strlen($component->form_fields["email"]->getValue())) {
                        $sSQL = "SELECT ID FROM anagraph WHERE email = " . $db->toSql($component->form_fields["email"]->value);
                        $db->query($sSQL);
                        if ($db->numRows() > 0) {
                            return ffTemplate::_get_word_by_code("email_not_unic_value");
                        }
                    }
                }
                break;
            case "update":
                if (!$component->user_vars["disable_account_registration"]) {
                    if (isset($component->form_fields["username"])) {
                        if (strlen($component->form_fields["username"]->getValue())) {
                            $sSQL = "SELECT ID FROM anagraph WHERE username = " . $db->toSql($component->form_fields["username"]->value) . " AND ID <> " . $db->toSql($component->key_fields["register-ID"]->value);
                            $db->query($sSQL);
                            if ($db->numRows() > 0) {
                                return ffTemplate::_get_word_by_code("username_not_unic_value");
                            }
                        }
                    }

                    if (strlen($component->form_fields["email"]->getValue())) {
                        $sSQL = "SELECT ID FROM anagraph WHERE email = " . $db->toSql($component->form_fields["email"]->value) . " AND ID <> " . $db->toSql($component->key_fields["register-ID"]->value);
                        $db->query($sSQL);
                        if ($db->numRows() > 0) {
                            return ffTemplate::_get_word_by_code("email_not_unic_value");
                        }
                    }
                }
               break;
            default:
        }
    }
    return null;
}

function MD_register_clone($ID_register, $register_new_name = "")
{
    $db = ffDB_Sql::factory();

    $addit_name = ffTemplate::_get_word_by_code("register_clone");
    $addit_smart_url = ffCommon_url_rewrite($addit_name);

    if (strlen($register_new_name)) {
        $register_new_smart_url = ffCommon_url_rewrite($register_new_name);
        $sSQL = "SELECT module_register.*
                    , module_register.name AS display_name
                FROM module_register
                WHERE module_register.name LIKE " . $db->toSql($register_new_smart_url . "%") . "
                ORDER BY LENGTH(module_register.name) DESC";
        $db->query($sSQL);
        if ($db->nextRecord()) {
            if (strpos($db->getField("name", "Text", true), "-" . $addit_smart_url) !== false) {
                $tmpCountCloneSmartUrl = explode("-" . $addit_smart_url, ffCommon_url_rewrite($db->getField("name", "Text", true)));
                $tmpCountCloneName = explode(" " . $addit_name, $db->getField("display_name", "Text", true));

                $countClone = $db->numRows();

                $register_new_smart_url = $tmpCountCloneSmartUrl[0] . "-" . $addit_smart_url . $countClone;
                $register_new_name = $tmpCountCloneName[0] . " " . $addit_name . $countClone;
            } else {
                $register_new_smart_url = $register_new_smart_url . "-" . $addit_smart_url;
                $register_new_name = $register_new_name . " " . $addit_name;
            }
        }
    }

    $sSQL = "SELECT module_register.*
                 , module_register.name AS display_name
            FROM module_register
            WHERE module_register.ID = " . $db->toSql($ID_register, "Number");
    $db->query($sSQL);
    if ($db->nextRecord()) {
        $register_old_name = $db->getField("display_name", "Text", true);
        $register_old_smart_url = ffCommon_url_rewrite($db->getField("name", "Text", true));

        if ($register_new_smart_url == $register_old_smart_url) {
            $register_new_smart_url = $register_new_smart_url . "-" . $addit_smart_url;
            $register_new_name = $register_new_name . " " . $addit_name;
        } elseif (strpos($register_old_smart_url, $register_new_smart_url . "-" . $addit_smart_url) === 0) {
            $countClone = str_replace($register_new_smart_url . "-" . $addit_smart_url, "", $register_old_smart_url);

            if (!$countClone) {
                $countClone = 2;
            } else {
                $countClone = ((int) $countClone) + 1;
            }

            $register_new_smart_url = $register_new_smart_url . "-" . $addit_smart_url . $countClone;
            $register_new_name = $register_new_name . " " . $addit_name . $countClone;
        }

        if (!strlen($register_new_smart_url)) {
            if (strpos($register_old_smart_url, "-" . $addit_smart_url) !== false) {
                $tmpCountCloneSmartUrl = explode("-" . $addit_smart_url, $register_old_smart_url);
                $tmpCountCloneName = explode(" " . $addit_name, $register_old_name);

                if (!$tmpCountCloneSmartUrl[1]) {
                    $countClone = 2;
                } else {
                    $countClone = ((int) $tmpCountCloneSmartUrl[1]) + 1;
                }

                $register_new_smart_url = $tmpCountCloneSmartUrl[0] . "-" . $addit_smart_url . $countClone;
                $register_new_name = $tmpCountCloneName[0] . " " . $addit_name . $countClone;
            } else {
                $register_new_smart_url = $register_old_smart_url . "-" . $addit_smart_url;
                $register_new_name = $register_old_name . " " . $addit_name;
            }
        }

        $sSQL = "INSERT INTO module_register
                (
                    `ID`
                    , `name`
                    , `ID_anagraph_type`
                    , `ID_email`
                    , `ID_email_activation`
                    , `activation`
                    , `default`
                    , `disable_account_registration`
                    , `display_view_mode`
                    , `enable_bill_data`
                    , `enable_default_tip`
                    , `enable_ecommerce_data`
                    , `enable_general_data`
                    , `enable_manage_account`
                    , `enable_newsletter`
                    , `enable_privacy`
                    , `enable_require_note`
                    , `enable_setting_data`
                    , `enable_user_menu`
                    , `fixed_post_content`
                    , `fixed_pre_content`
                    , `force_redirect`
                    , `generate_password`
                    , `primary_gid`
                    , `public`
                    , `show_title`
                    , `simple_registration`
                    , `default_placeholder`
                    , `default_show_label`
                )
                SELECT 
                    null
                    , " . $db->toSql($register_new_smart_url) . "
                    , `ID_anagraph_type`
                    , `ID_email`
                    , `ID_email_activation`
                    , `activation`
                    , `default`
                    , `disable_account_registration`
                    , `display_view_mode`
                    , `enable_bill_data`
                    , `enable_default_tip`
                    , `enable_ecommerce_data`
                    , `enable_general_data`
                    , `enable_manage_account`
                    , `enable_newsletter`
                    , `enable_privacy`
                    , `enable_require_note`
                    , `enable_setting_data`
                    , `enable_user_menu`
                    , `fixed_post_content`
                    , `fixed_pre_content`
                    , `force_redirect`
                    , `generate_password`
                    , `primary_gid`
                    , `public`
                    , `show_title`
                    , `simple_registration`
                    , `default_placeholder`
                    , `default_show_label`
                FROM module_register
                WHERE module_register.ID = " . $db->toSql($ID_register, "Number");
        $db->execute($sSQL);
        $ID_new_register = $db->getInsertID(true);
        if ($ID_new_register > 0) {
            $sSQL = "SELECT module_register_fields.*
                    FROM module_register_fields
                    WHERE module_register_fields.ID_module = " . $db->toSql($ID_register, "Number") . "
                    ORDER BY module_register_fields.ID";
            $db->query($sSQL);
            if ($db->nextRecord()) {
                do {
                    $arrRegisterField[] = $db->getField("ID", "Number", true);
                } while ($db->nextRecord());
            }

            if (is_array($arrRegisterField) && count($arrRegisterField)) {
                $arrRegisterFieldSelectionValue = array();
                foreach ($arrRegisterField as $ID_register_field) {
                    $sSQL = "INSERT INTO module_register_fields
                            (
                                `ID`
                                , `ID_module`
                                , `name`
                                , `ID_check_control`
                                , `ID_extended_type`
                                , `ID_form_fields_group`
                                , `ID_selection`
                                , `disable_select_one`
                                , `enable_in_grid`
                                , `enable_in_menu`
                                , `enable_tip`
                                , `hide`
                                , `hide_register`
                                , `unic_value`
                                , `writable`
                                , `custom_placeholder`
                                , `default_grid`
                                , `grid_md`
                                , `grid_sm`
                                , `grid_xs`
                                , `placeholder`
                                , `label_grid_md`
                                , `label_grid_sm`
                                , `label_grid_xs`
                                , `label_default_grid`
                                , `hide_label`
                                , `enable_in_document`
                                , `enable_in_mail`
                                , `order`
                            )
                            SELECT 
                                null
                                , " . $db->toSql($ID_new_register, "Number") . "
                                , `name`
                                , `ID_check_control`
                                , `ID_extended_type`
                                , `ID_form_fields_group`
                                , `ID_selection`
                                , `disable_select_one`
                                , `enable_in_grid`
                                , `enable_in_menu`
                                , `enable_tip`
                                , `hide`
                                , `hide_register`
                                , `unic_value`
                                , `writable`
                                , `custom_placeholder`
                                , `default_grid`
                                , `grid_md`
                                , `grid_sm`
                                , `grid_xs`
                                , `placeholder`
                                , `label_grid_md`
                                , `label_grid_sm`
                                , `label_grid_xs`
                                , `label_default_grid`
                                , `hide_label`
                                , `enable_in_document`
                                , `enable_in_mail`
                                , `order`
                            FROM module_register_fields
                            WHERE module_register_fields.ID = " . $db->toSql($ID_register_field, "Number");
                    $db->execute($sSQL);

                    $arrRegisterField[$ID_register_field] = $db->getInsertID(true);

                    if (check_function("MD_form_on_done_action")) {
                        $arrRegisterFieldSelectionValue = array_replace($arrRegisterFieldSelectionValue, MD_form_clone_field_selection_value($ID_register_field, $arrRegisterField[$ID_register_field]));
                    }
                }
            }
            if (check_function("MD_form_on_done_action")) {
                MD_form_clone_field_selection($arrRegisterField, $register_new_smart_url);
            }
        }
    }

    return array("ID" => $ID_new_register
        , "name" => $register_new_smart_url
        , "display_name" => $register_new_name);
}

function MD_register_delete($ID_register)
{
    $db = ffDB_Sql::factory();

    $sSQL = "DELETE FROM module_form_fields_selection_value
                WHERE module_form_fields_selection_value.ID_form_fields IN (SELECT module_register_fields.ID
                                                                            FROM module_register_fields
                                                                            WHERE module_register_fields.ID_module = " . $db->toSql($ID_register, "Number") .
            ")";
    $db->execute($sSQL);

    $sSQL = "DELETE FROM module_register_fields
                WHERE module_register_fields.ID_module = " . $db->toSql($ID_register, "Number");
    $db->execute($sSQL);
}
