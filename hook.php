<?php
define("VG_SITE_RESTRICTED", "/restricted");

Auth::addEvent("on_before_send_request", function (&$params) {
    switch ($_SERVER["PATH_INFO"]) {
        case "/recover":
            $params["title"] = "Richiesta recupero password. Codice verifica: " . $params["fields"]["code"];
            $params["template"] = "/themes/site/contents/email/auth/recover_code.html";
            $params["fields"]["complete_name"] = $params["email"];
            $email = $params["email"];
            break;
        case "/activation":
        default:
            $params["title"] = "Il nostro benvenuto su Grand Chef! Codice attivazione: " . $params["fields"]["code"];
            $params["template"] = "/themes/site/contents/email/auth/activation_code.html";
            $params["fields"]["complete_name"] = $params["fields"]["username"];
            $email = $params["email"];
            break;
    }

    if (strlen($email)) {
        $search = array(
            "anagraph.email" => $email
        );
        if (strlen($params["fields"]["type"])) {
            switch ($params["fields"]["type"]) {
                case "utente":
                    $search["anagraph.ID_type"] = 2;
                    break;
                case "dottore":
                    $search["anagraph.ID_type"] = 1;
                    break;
                default:

            }
        }
        $anagraphObject = Anagraph::getInstance();
        $arrAnagraphList = $anagraphObject->read(
            array(
                "anagraph.ID"
            , "anagraph.ID_role"
            , "anagraph_person.name"
            , "anagraph_person.surname"
            ), $search
        );
        if (is_array($arrAnagraphList) && count($arrAnagraphList)) {
            if (strlen($arrAnagraphList["0"]["person"]["name"]) || strlen($arrAnagraphList["0"]["person"]["surname"])) {

                $params["fields"]["complete_name"] = $arrAnagraphList["0"]["person"]["name"] . " " . $arrAnagraphList["0"]["person"]["surname"];
                $params["fields"]["name"] = $arrAnagraphList["0"]["person"]["name"];
                $params["fields"]["surname"] = $arrAnagraphList["0"]["person"]["surname"];


            }
        }


    }
});

switch ($_SERVER["HTTP_HOST"]) {
    case "[DOMAIN]": // DA IMPOSTARE
        if (!defined("DOMAIN_PROD")) define("DOMAIN_PROD", true);
        if (!defined("WWW_LINK")) define("WWW_LINK", "www.[DOMAIN].[EXTENSION]");
        if (!defined("WWW_LINK_ABSOLUTE")) define("WWW_LINK_ABSOLUTE", "https://www.[DOMAIN].[EXTENSION]");
        break;
    case "[DEV_DOMAIN]": // DA IMPOSTARE
        if (!defined("DOMAIN_PROD")) define("DOMAIN_PROD", false);
        if (!defined("WWW_LINK")) define("WWW_LINK", "dev.[DOMAIN].[EXTENSION]");
        if (!defined("WWW_LINK_ABSOLUTE")) define("WWW_LINK_ABSOLUTE", "https://dev.[DOMAIN].[EXTENSION]");
        break;
}
function updateUserInfo($ID_user, $type, $value = null)
{
    $result = "error";

    if ($ID_user) {
        $db = ffDB_Sql::factory();

        $anagraphObject = Anagraph::getInstance();
        $arrAnagraphList = $anagraphObject->read(
            array(
                "anagraph.ID"
            , "anagraph.username"
            , "anagraph_person.name"
            , "anagraph_person.surname"
            ),
            array(
                "anagraph.ID_user" => $ID_user
            )
        );
        $ID_anagraph = $arrAnagraphList[0]["ID"];
        $name = $arrAnagraphList[0]["person"]["name"];
        $surname = $arrAnagraphList[0]["person"]["surname"];
        switch ($type) {
            case "activate":

                $anagraphObject->update(/*OK*/
                    array(
                        "anagraph.status" => 1
                    ),
                    array(
                        "anagraph.ID_user" => $ID_user
                    )
                );
                $sSQL = "UPDATE access_users SET status = 1 WHERE ID = " . $db->toSql($ID_user, "Number");
                $db->execute($sSQL);
                break;
            case "address":
                if ($value) {
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph_place.billaddress" => $value
                        ),
                        array(
                            "anagraph_place.ID_anagraph" => $ID_anagraph
                        )
                    );
                }
                break;
            case "avatar":
                if ($value) {
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph.avatar" => $value
                        ),
                        array(
                            "anagraph.ID_user" => $ID_user
                        )
                    );
                }
                break;
            case "cap":
                if ($value) {
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph_place.billcap" => $value
                        ),
                        array(
                            "anagraph_place.ID_anagraph" => $ID_anagraph
                        )
                    );
                }
                break;
            case "cf":
                if ($value) {
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph_person.cf" => $value
                        ),
                        array(
                            "anagraph_person.ID_anagraph" => $ID_anagraph
                        )
                    );
                }
                break;
            case "city":
                if ($value) {
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph_place.billtown" => $value
                        ),
                        array(
                            "anagraph_place.ID_anagraph" => $ID_anagraph
                        )
                    );
                }
                break;
            case "dataNascita":
                if ($value) {
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph_person.birthday" => $value
                        ),
                        array(
                            "anagraph_person.ID_anagraph" => $ID_anagraph
                        )
                    );
                }
                break;
            case "degree":
                if ($value) {
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph.ID_role" => $value
                        ),
                        array(
                            "anagraph.ID_user" => $ID_user
                        )
                    );
                }
                break;
            case "last_update":
                $anagraphObject->update(/*OK*/
                    array(
                        "anagraph.last_update" => time()
                    ),
                    array(
                        "anagraph.ID_user" => $ID_user
                    )
                );
                break;
            case "name":
                if ($value) {
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph.name" => $value . " " . $surname
                        ),
                        array(
                            "anagraph.ID" => $ID_anagraph
                        )
                    );
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph_person.name" => $value
                        ),
                        array(
                            "anagraph_person.ID_anagraph" => $ID_anagraph
                        )
                    );
                }
                break;
            case "province":
                if ($value) {
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph_place.billprovince" => $value
                        ),
                        array(
                            "anagraph_place.ID_anagraph" => $ID_anagraph
                        )
                    );
                }
                break;
            case "referer":
                if ($value) {
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph.referer" => $value
                        ),
                        array(
                            "anagraph.ID_user" => $ID_user
                        )
                    );
                }
                break;
            case "sesso":
                if ($value) {
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph_person.gender" => $value
                        ),
                        array(
                            "anagraph_person.ID_anagraph" => $ID_anagraph
                        )
                    );
                }
                break;
            case "surname":
                if ($value) {
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph.name" => $name . " " . $value
                        ),
                        array(
                            "anagraph.ID" => $ID_anagraph
                        )
                    );

                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph_person.surname" => $value
                        ),
                        array(
                            "anagraph_person.ID_anagraph" => $ID_anagraph
                        )
                    );
                }
                break;
            case "tags":
                if ($value) {
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph.tags" => $value
                        ),
                        array(
                            "anagraph.ID_user" => $ID_user
                        )
                    );
                }
                break;
            case "tel":
                if ($value) {
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph.tel" => $value
                        ),
                        array(
                            "anagraph.ID_user" => $ID_user
                        )
                    );
                }
                break;
            default:
                die("CAMPO NON PREVISTO");

        }
        $result = "success";
    }
    return $result;
}


function updateUserInfoLocal($type, $value = null, $token = null)
{
    $result = "error";
    if (!$token) {
        if (Auth::isLogged()) {
            if (strlen(Auth::get()["login_token"])) {
                $token = Auth::get()["login_token"];
            }
        }
    }

    if ($token) {
        $id_user = getUserByTokenLocal($token);

        if ($id_user) {
            $result = "success";
            $anagraphObject = Anagraph::getInstance();
            $arrAnagraphList = $anagraphObject->read(
                array(
                    "anagraph.ID"
                , "anagraph.username"
                ),
                array(
                    "anagraph.ID_user" => $id_user
                )
            );
            $ID_anagraph = $arrAnagraphList[0]["ID"];
            switch ($type) {
                case "activate":
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph.status" => 1
                        ),
                        array(
                            "anagraph.ID_user" => $id_user
                        )
                    );
                    break;
                case "address":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "anagraph_place.billaddress" => $value
                            ),
                            array(
                                "anagraph_place.ID_anagraph" => $ID_anagraph
                            )
                        );
                    }
                    break;
                case "avatar":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "anagraph.avatar" => $value
                            ),
                            array(
                                "anagraph.ID_user" => $id_user
                            )
                        );
                    }
                    break;
                case "cap":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "anagraph_place.billcap" => $value
                            ),
                            array(
                                "anagraph_place.ID_anagraph" => $ID_anagraph
                            )
                        );
                    }
                    break;
                case "cf":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "anagraph_person.cf" => $value
                            ),
                            array(
                                "anagraph_person.ID_anagraph" => $ID_anagraph
                            )
                        );
                    }
                    break;
                case "city":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "anagraph_place.billtown" => $value
                            ),
                            array(
                                "anagraph_place.ID_anagraph" => $ID_anagraph
                            )
                        );
                    }
                    break;
                case "dataNascita":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "anagraph_person.birthday" => $value
                            ),
                            array(
                                "anagraph_person.ID_anagraph" => $ID_anagraph
                            )
                        );
                    }
                    break;
                case "dataNascitaLocal":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "mol.doctors.data_nascita" => $value
                            ),
                            array(
                                "mol.doctors.ID_anagraph" => $ID_anagraph
                            )
                        );
                    }
                    break;
                case "degree":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "anagraph.ID_role" => $value
                            ),
                            array(
                                "anagraph.ID_user" => $id_user
                            )
                        );
                    }
                    break;
                case "last_update":
                    $anagraphObject->update(/*OK*/
                        array(
                            "anagraph.last_update" => time()
                        ),
                        array(
                            "anagraph.ID_user" => $id_user
                        )
                    );
                    break;
                case "name":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "anagraph_person.name" => $value
                            ),
                            array(
                                "anagraph_person.ID_anagraph" => $ID_anagraph
                            )
                        );
                    }
                    break;
                case "province":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "anagraph_place.billprovince" => $value
                            ),
                            array(
                                "anagraph_place.ID_anagraph" => $ID_anagraph
                            )
                        );
                    }
                    break;
                case "sesso":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "anagraph_person.gender" => $value
                            ),
                            array(
                                "anagraph_person.ID_anagraph" => $ID_anagraph
                            )
                        );
                    }
                    break;
                case "sessoLocal":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "mol.doctors.sesso" => $value
                            ),
                            array(
                                "mol.doctors.ID_anagraph" => $ID_anagraph
                            )
                        );
                    }
                    break;
                case "surname":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "anagraph_person.surname" => $value
                            ),
                            array(
                                "anagraph_person.ID_anagraph" => $ID_anagraph
                            )
                        );
                    }
                    break;
                case "tags":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "anagraph.tags" => $value
                            ),
                            array(
                                "anagraph.ID_user" => $id_user
                            )
                        );
                    }
                    break;
                case "tel":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "anagraph.tel" => $value
                            ),
                            array(
                                "anagraph.ID_user" => $id_user
                            )
                        );
                    }
                    break;
                case "username":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "anagraph.username" => $value
                            ),
                            array(
                                "anagraph.ID_user" => $id_user
                            )
                        );
                    }
                    break;
                case "activity":
                case "bachelor_year":
                case "cell":
                case "cell_premium":
                case "collabora":
                case "complete":
                case "curriculum_mol":
                case "data_premium":
                case "espertoin":
                case "expert":
                case "facebook":
                case "google_plus":
                case "last_request":
                case "linkedin":
                case "mail_er":
                case "medico_online":
                case "medsurf_dec":
                case "mol_cell":
                case "mol_city":
                case "mol_province":
                case "mol_tel":
                case "n_iscrizione_albo":
                case "nome_pubmed":
                case "premium_notify_time":
                case "premium_sponsor":
                case "pro_subject":
                case "provincia_iscrizione_albo":
                case "question_tags":
                case "question_tags_premium":
                case "short_descrption_mol":
                case "twitter":
                case "verificato":
                case "website":
                    if ($value) {
                        $anagraphObject->update(/*OK*/
                            array(
                                "mol.doctors." . $type => $value
                            ),
                            array(
                                "mol.doctors.ID_anagraph" => $ID_anagraph
                            )
                        );
                    }
                    break;
                case "addCategories":
                    if ($value) {
                        $anagraphObject->write(/*OK*/
                            array(
                                "anagraph_categories.ID_anagraph" => $ID_anagraph
                            , "anagraph_categories.ID_categories" => $value
                            ),
                            array(
                                "anagraph_categories.ID_categories" => $value
                            ),
                            array(
                                "anagraph_categories.ID_anagraph" => $ID_anagraph
                            , "anagraph_categories.ID_categories" => $value
                            )
                        );
                    }
                    break;
                case "removeCategories":
                    if ($value) {
                        $anagraphObject->delete(/*OK*/
                            array(
                                "anagraph_categories.ID_anagraph" => $ID_anagraph
                            , "anagraph_categories.ID_categories" => $value
                            )
                        );
                    }
                    break;

                case "addNewsletter":
                    if ($value) {
                        $anagraphObject->write(
                            array(
                                "anagraph_newsletter.ID_anagraph" => $ID_anagraph
                            , "anagraph_newsletter.name" => $value
                            ),
                            array(
                                "anagraph_newsletter.name" => $value
                            ),
                            array(
                                "anagraph_newsletter.ID_anagraph" => $ID_anagraph
                            , "anagraph_newsletter.name" => $value
                            )
                        );
                    }
                    break;
                default:
                    die("CAMPO NON PREVISTO");

            }
        }
    }
    return $result;
}

function getTagList($type = "")
{
    $db = ffDB_Sql::factory();
    $arrTagListDef = array();

    $sSQL = "SELECT smart_url, ID, name, common_text, categories, permalink 
                FROM search_tags
                WHERE 1";
    $db->query($sSQL);
    if ($db->nextRecord()) {
        do {
            $text = $db->getField("name", "Text", true);
            switch ($type) {
                case "name":
                    $key = $text;
                    break;
                case "smart_url":
                    $key = $db->getField("smart_url", "Text", true);
                    break;
                case "common_text":
                    $key = $db->getField("common_text", "Text", true);
                    break;
                case "permalink":
                    $key = $db->getField("permalink", "Text", true);
                    break;
                case "url_rewrite":
                    $key = ffCommon_url_rewrite($db->getField("name", "Text", true));
                    break;
                case "ID":
                default:
                    $key = $db->getField("ID", "Number", true);
                    break;
            }

            $arrTagListDef[$key] = array(
                "ID" => $db->getField("ID", "Number", true)
            , "name" => $text
            , "smart_url" => $db->getField("smart_url", "Text", true)
            , "common_text" => $db->getField("common_text", "Text", true)
            , "permalink" => $db->getField("permalink", "Text", true)
            , "categories" => $db->getField("categories", "Text", true)
            );
        } while ($db->nextRecord());
    }
    return $arrTagListDef;
}

function saveLocalSocialToken($token, $user_id, $type = "live", $expire = 0)
{
    $db = ffDB_Sql::factory();

    if (strlen($user_id) && strlen($token)) {
        $sSQL = "INSERT INTO cm_mod_security_token
                    (
                        ID
                        , ID_user
                        , token
                        , type
                        , expire
                    ) VALUES
                    (
                        null
                        , " . $db->toSql($user_id, "Number") . "
                        , " . $db->toSql($token, "Text") . "
                        , " . $db->toSql($type, "Text") . "
                        , " . $db->toSql($expire, "Text") . " 
                    )";
        $db->execute($sSQL);
    }
}

function saveLocalToken($token, $user_id, $type = "live")
{
    $db = ffDB_Sql::factory();

    if (strlen($user_id) && strlen($token)) {
        $sSQL = "SELECT * 
                    FROM cm_mod_security_token 
                    WHERE ID_user  = " . $db->toSql($user_id, "Number") . "
                        AND token = " . $db->toSql($token, "Text") . "
                        AND type = " . $db->toSql($type, "Text") . "
                        AND expire = 0";
        $db->query($sSQL);
        if (!$db->nextRecord()) {
            $sSQL = "INSERT INTO cm_mod_security_token
                        (
                            ID
                            , ID_user
                            , token
                            , type
                            , expire
                        ) VALUES
                        (
                            null
                            , " . $db->toSql($user_id, "Number") . "
                            , " . $db->toSql($token, "Text") . "
                            , " . $db->toSql($type, "Text") . "
                            , 0
                        )";
            $db->execute($sSQL);
        }
    }
}

function getTokenLocalDB($user_id)
{
    $db = ffDB_Sql::factory();

    $token_result = null;

    if (is_array($user_id) && count($user_id)) {
        $sSQL = "SELECT * 
                    FROM cm_mod_security_token 
                    WHERE ID_user IN (" . $db->toSql(implode(",", $user_id), "Text", false) . ") 
                        AND type = 'live' 
                        AND expire = 0";
        $db->query($sSQL);
        if ($db->nextRecord()) {
            do {
                $token_result[$db->getField("ID_user", "Number", true)] = $db->getField("token", "Text", true);
            } while ($db->nextRecord());
        }
    } elseif (strlen($user_id)) {
        $sSQL = "SELECT * 
                    FROM cm_mod_security_token 
                    WHERE ID_user  = " . $db->toSql($user_id, "Number") . "
                        AND type = 'live' 
                        AND expire = 0";
        $db->query($sSQL);
        if ($db->nextRecord()) {
            $token_result = $db->getField("token", "Text", true);
        } else {
            $token_result = Auth::getInstance("token")->create();

            $sSQL = "INSERT INTO cm_mod_security_token
                (
                    ID
                    , ID_user
                    , token
                    , type
                    , expire
                ) VALUES
                (
                    null
                    , " . $db->toSql($user_id, "Number") . "
                    , " . $db->toSql($token_result, "Text") . "
                    , 'live'
                    , 0
                )";
            $db->execute($sSQL);
        }
    }
    return $token_result;
}


function getUserByTokenLocal($token)
{
    $db = ffDB_Sql::factory();

    $user_id = null;

    if (strlen($token)) {
        $sSQL = "SELECT * 
                    FROM cm_mod_security_token 
                    WHERE token  = " . $db->toSql($token, "Text") . "
                        AND type = 'live' 
                        AND expire = 0";
        $db->query($sSQL);
        if ($db->nextRecord()) {
            $user_id = $db->getField("ID_user", "Number", true);
        }
    }

    return $user_id;
}

function registerUser($arrInfo)
{
    $result = "error";
    $message = "";
    $token = "";
    $string_error = "";
    $arrError = array();

    if (is_array($arrInfo) && count($arrInfo)) {
        $db = ffDB_Sql::factory();
        $anagraphObject = Anagraph::getInstance();

        $avatar = "";
        $email = "";
        $password = "";
        $name = "";
        $surname = "";
        $sesso = "";
        $city = "";
        $billprovince = "";
        $billtown = "";

        if (strlen($arrInfo["email"])) {
            if (filter_var($arrInfo["email"], FILTER_VALIDATE_EMAIL)) {
                $arrAnagraphList = $anagraphObject->read(
                    array(
                        "anagraph.ID"
                    ),
                    array(
                        "anagraph.email" => $arrInfo["email"]
                        , "anagraph.ID_domain" => 1
                        , "anagraph.ID_type" => 2
                    )
                );
                if (is_array($arrAnagraphList) && count($arrAnagraphList)) {
                    $arrError[] = "INPUT[name='email']";
                    $string_error .= "<li>Hai già un account con questa email. <a href='/login' style='color: #009fe3;text-decoration: underline;'>Accedi qui</a></li>";
                } else {
                    $email = $arrInfo["email"];
                }
            } else {
                $arrError[] = "INPUT[name='email']";
                $string_error .= "<li><a onclick=\"goToElem('INPUT[name=\'email\']');\">L'email inserita non è stata scritta correttamente</a></li>";
            }
        } else {
            $arrError[] = "INPUT[name='email']";
            $string_error .= "<li><a onclick=\"goToElem('INPUT[name=\'email\']');\">Il campo \"Email\" non è stato compilato</a></li>";
        }

        if (isset($arrInfo["password"])) {
            if (strlen($arrInfo["password"])) {
                if (checkPassword($arrInfo["password"])) {
                    $arrError[] = "INPUT[name='confirm-password']";
                    $arrError[] = "INPUT[name='password']";
                    $string_error .= "<li><a onclick=\"goToElem('.single-field INPUT[name=\'password\']');\">" . ffTemplate::_get_word_by_code("error_register_popup_check_password") . "</a></li>";
                } else {
                    if ($arrInfo["password"] != $arrInfo["confirmPassword"]) {
                        $arrError[] = "INPUT[name='confirm-password']";
                        $arrError[] = "INPUT[name='password']";
                        $string_error .= "<li><a onclick=\"goToElem('.single-field INPUT[name=\'password\']');\">Il campo \"Password\" e il campo \"Conferma password\" non corrispondono</a></li>";
                    } else {
                        $password = $arrInfo["password"];
                    }
                }
            } else {
                $arrError[] = "INPUT[name='confirm-password']";
                $arrError[] = "INPUT[name='password']";
                $string_error .= "<li><a onclick=\"goToElem('.single-field INPUT[name=\'password\']');\">Il campo \"Password\" non è stato compilato</a></li>";
            }
        } else {
            $password = "PasswordSocial" . date("Y", time()) . "!!";
        }

        if (isset($arrInfo["name"])) {
            if (!strlen($arrInfo["name"])) {
                $arrError[] = "INPUT[name='user-name']";
                $string_error .= "<li><a onclick=\"goToElem('.single-field INPUT[name=\'user-name\']');\">Il campo \"Nome\" non è stato compilato</a></li>";
            } else {
                $name = $arrInfo["name"];
            }
        } else {
            $name = "";
        }

        if (isset($arrInfo["surname"])) {
            if (!strlen($arrInfo["surname"])) {
                $arrError[] = "INPUT[name='user-surname']";
                $string_error .= "<li><a onclick=\"goToElem('.single-field INPUT[name=\'user-surname\']');\">Il campo \"Cognome\" non è stato compilato</a></li>";
            } else {
                $surname = $arrInfo["surname"];
            }
        } else {
            $surname = "";
        }

        if (isset($arrInfo["sesso"])) {
            if (!strlen($arrInfo["sesso"])) {
                $arrError[] = ".sesso-container-small";
                $string_error .= "<li><a onclick=\"goToElem('.sesso-radio-container-small');\">Il campo \"Sesso\" non è stato compilato</a></li>";
            } else {
                $sesso = $arrInfo["sesso"];
            }
        }

        if (isset($arrInfo["city"])) {
            if (!strlen($arrInfo["city"])) {
                $arrError[] = "INPUT[name='citta']";
                $string_error .= "<li><a onclick=\"goToElem('.sesso-radio-container-small');\">Il campo \"Citt&agrave;\" non è stato compilato</a></li>";
            } else {
                $city = $arrInfo["city"];
            }
        }

        if (isset($arrInfo["provinciaResidenza"])) {
            if (!($arrInfo["provinciaResidenza"][0] > 0)) {
                $arrError[] = "#provincia-residenza";
                $string_error .= "<li><a onclick=\"goToElem('#provincia-residenza');\">Il campo \"Provincia\" non è stato compilato</a></li>";
            } else {
                $billprovince = $arrInfo["provinciaResidenza"][0];
            }
        }
        if (isset($arrInfo["cittaResidenza"])) {
            if (!($arrInfo["cittaResidenza"][0] > 0)) {
                $arrError[] = "#citta-residenza";
                $string_error .= "<li><a onclick=\"goToElem('#citta-residenza');\">Il campo \"Città\" non è stato compilato</a></li>";
            } else {
                $billtown = $arrInfo["cittaResidenza"][0];
            }
        }

        if (isset($arrInfo["birthday"])) {
            if (!strlen($arrInfo["birthday"])) {
                $arrError[] = "INPUT[name='birthday']";
                $string_error .= "<li><a onclick=\"goToElem('.birthday-container');\">Il campo \"Data di nascita\" non è stato compilato</a></li>";
            } else {
                $arrBirthday = explode("/", $arrInfo["birthday"]);
                $birthday = $arrBirthday[2] . "-" . $arrBirthday[1] . "-" . $arrBirthday[0];
            }
        }

        if (isset($arrInfo["newsletter"])) {
            $newsletter = $arrInfo["newsletter"];
        }
        $arrUsername = explode("@", $email);

        $username = $arrUsername[0] . " " . substr($arrUsername[1], 0, strpos($arrUsername[1], "."));
        $username = ucwords(str_replace(array(".", "-", "_"), array(" "), $username));

        $code_activation = randomPasswordRegister();

        if (!function_exists("ffCommon_url_rewrite")) {
            require_once(FF_DISK_PATH . "/ff/common.php");
        }

        if (strlen($string_error)) {
            $string_error = '<div class=\'inner-container\'><ul>' . $string_error . '</ul></div>';
        } else {
            $result = "success";
            $arrUser = $anagraphObject->insert(
                array(
                    "access.users.acl" => 3
                , "access.users.ID_domain" => 1
                , "access.users.acl_primary" => "user"
                , "access.users.expire" => 0
                , "access.users.status" => 0
                , "access.users.username" => $username
                , "access.users.username_slug" => ffCommon_url_rewrite($username)
                , "access.users.email" => $email
                , "access.users.tel" => ""
                , "access.users.password" => $password
                , "access.users.avatar" => $avatar
                , "access.users.created" => time()
                , "access.users.last_update" => time()
                , "access.users.last_login" => 0
                , "access.users.ID_lang" => LANGUAGE_INSET_ID
                , "access.users.SID" => $code_activation
                , "access.users.SID_expire" => time() + (12 * 3600)
                , "access.users.SID_device" => 0
                , "access.users.SID_ip" => ""
                , "access.users.SID_question" => ""
                , "access.users.SID_answer" => ""
                , "access.users.verified_email" => 0
                , "access.users.verified_tel" => 0
                )
            );
            $ID_user = $arrUser[0]["user"];

            $arrAnagraphList = $anagraphObject->read(
                array(
                    "anagraph.ID_user"
                ),
                array(
                    "anagraph.ID_user" => array('$gt' => $ID_user)
                ),
                array(
                    "anagraph.ID_user" => -1
                ),
                1
            );


            if (is_array($arrAnagraphList) && count($arrAnagraphList)) {
                $ID_user_new = $arrAnagraphList[0]["ID_user"] + 1;

                $anagraphObject->update(
                    array(
                        "access.users.ID" => $ID_user_new
                    ),
                    array(
                        "access.users.ID" => $ID_user
                    )
                );

                $ID_user = $ID_user_new;
            }

            $anagraphObject->insert(
                array(
                    "access.user_groups.ID_user" => $ID_user
                , "access.user_groups.ID_group" => 3
                )
            );

            switch ($sesso) {
                case "Uomo":
                    $sesso_db = "M";
                    break;
                case "Donna":
                    $sesso_db = "F";
                    break;
            }

            $arrAnagraph = $anagraphObject->insert(
                array(
                    "anagraph.ID_domain" => 1
                , "anagraph.ID_type" => 2
                , "anagraph.ID_lang" => 1
                , "anagraph.ID_user" => $ID_user
                , "anagraph.avatar" => $avatar
                , "anagraph.name" => $name . " " . $surname
                , "anagraph.email" => $email
                , "anagraph.tel" => ""
                , "anagraph.tags" => ""
                , "anagraph.status" => 0
                , "anagraph.username" => $username
                , "anagraph.created" => time()
                , "anagraph.last_update" => time()
                , "anagraph.referer" => $arrInfo["referer"]
                , "anagraph.valid_email" => 0
                , "anagraph.valid_tel" => 0
                , "anagraph.password_alg" => ""
                , "anagraph_person.name" => $name
                , "anagraph_person.surname" => $surname
                , "anagraph_person.cf" => ""
                , "anagraph_person.birthday" => $birthday
                , "anagraph_person.gender" => $sesso_db
                , "anagraph_place.billaddress" => ""
                , "anagraph_place.billcap" => ""
                , "anagraph_place.billprovince" => $billprovince
                , "anagraph_place.billtown" => $billtown
                )
            );

            $ID_anagraph = $arrAnagraph[0]["anagraph"];

            if ($newsletter) {
                $arrAnagraphList = $anagraphObject->insert(
                    array(
                        "anagraph_newsletter.ID_anagraph" => $ID_anagraph
                    , "anagraph_newsletter.name" => "newsletter_pubblica"
                    )
                );
            }
        }
    }

    return array("result" => $result, "message" => $string_error, "arrError" => $arrError, "email" => $email, "ID_user" => $ID_user);
}



function randomPasswordRegister()
{
    $alphabet = '1234567890';
    $pass = array(); //remember to declare $pass as an array
    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
    for ($i = 0; $i < 5; $i++) {
        $n = rand(0, $alphaLength);
        $pass[] = $alphabet[$n];
    }
    return implode($pass); //turn the array into a string
}

function getUserInfo($token = null)
{
    $result = "error";
    $message = "";
    $accessInfo = null;

    if (!$token) {
        if (Auth::isLogged()) {
            if (strlen(Auth::get()["login_token"])) {
                $token = Auth::get()["login_token"];
            }
        }
    }

    if ($token) {
        $timeout = 12; //5
        $headers = array(
            "domain: " . PM_CLIENT_DOMAIN
        , "client-id: " . PM_CLIENT_ID
        , "client-secret: " . PM_CLIENT_SECRET
        , "t: " . $token
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, API_AUTH_LINK_ABSOLUTE . "/user/accessInfo");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = json_decode(curl_exec($ch));
        curl_close($ch);

        $message = $data->error;

        if ($data->status === 0) {
            $result = "success";
            $accessInfo = $data->data;
        }
    }


    return array("result" => $result, "message" => $message, "accessInfo" => $accessInfo);
}

function userLogin($username, $password, $model = null)
{
    $ch = curl_init();
    $timeout = 20; //5
    $headers = array(
        "domain: " . PM_CLIENT_DOMAIN
    , "client-id: " . PM_CLIENT_ID
    , "client-secret: " . PM_CLIENT_SECRET
    );
    if ($model) {
        $headers[] = "model: " . $model;
    }
    $post_fields = array(
        "username" => $username
    , "password" => $password
    );

    curl_setopt($ch, CURLOPT_URL, API_AUTH_LINK_ABSOLUTE . "/user/login");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    $data = json_decode(curl_exec($ch));
    curl_close($ch);

    $loginInfo = (array)$data->data;

    return $loginInfo;
}

function updateAccessInfo($ID_user, $type, $value)
{
    $db = ffDB_Sql::factory();
    $result = "error";
    $message = "";
    switch ($type) {
        case "email":
            $sSQL = "UPDATE access_users SET 
                            username = " . $db->toSql($value, "Text") . "
                            , username_slug = " . $db->toSql(ffCommon_url_rewrite($value), "Text") . "
                            , email = " . $db->toSql($value, "Text") . "
                        WHERE ID = " . $db->toSql($ID_user, "Number");
            $db->execute($sSQL);
            $sSQL = "UPDATE anagraph SET 
                            username = " . $db->toSql($value, "Text") . "
                            , email = " . $db->toSql($value, "Text") . "
                        WHERE ID_user = " . $db->toSql($ID_user, "Number");
            $db->execute($sSQL);
            $result = "success";
            break;
        case "password":
            $sSQL = "UPDATE access_users SET 
                            password = PASSWORD(" . $db->toSql($value, "Text") . ")
                        WHERE ID = " . $db->toSql($ID_user, "Text");
            $db->execute($sSQL);
            $result = "success";
            break;
        default:
            $message = "Campo non mappato";;

    }

    return array("result" => $result, "message" => $message);
}

function refreshUserData($username)
{
    if (check_function("refresh_cache")) {
        refresh_cache_by_path("/cache/paginemediche.it/ita/private/shard/" . $username . "/login-login/index_XHR.html");
        refresh_cache_by_path("/cache/pro.paginemediche.it/ita/private/shard/" . $username . "/login-login/index_XHR.html");
    }
}

function deleteUserUreg($token = null)
{
    $result = "error";
    $message = "";
    $accessInfo = null;

    if ($token) {
        $timeout = 12; //5
        $headers = array(
            "domain: " . PM_CLIENT_DOMAIN
        , "client-id: " . PM_CLIENT_ID
        , "client-secret: " . PM_CLIENT_SECRET
        , "t: " . $token
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, API_AUTH_LINK_ABSOLUTE . "/user/clearProfile");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = json_decode(curl_exec($ch));
        curl_close($ch);

        $message = $data->error;

        if ($data->status === 0) {
            $result = "success";
        }
    }


    return array("result" => $result, "message" => $message);
}

function checkSid($username, $sid, $scope, $model = null)
{
    $result = "error";
    $ID_user = 0;
    $db = ffDB_Sql::factory();
    $sSQL = "SELECT ID_user
                FROM access_sid 
                  INNER JOIN access_users ON access_users.ID = access_sid.ID_user
                WHERE access_users.email = " . $db->toSql($username, "Text") . " 
                    AND access_users.SID_expire > " . $db->toSql(time(), "Number") . " 
                    AND access_users.SID = " . $db->toSql($sid, "Text") . "  
                    AND access_sid.type = " . $db->toSql($scope, "Text");
    $db->query($sSQL);
    if ($db->nextRecord()) {
        $result = "success";
        $ID_user = $db->getField("ID_user", "Number", true);
        $sSQL = "DELETE FROM access_sid WHERE ID_user = " . $db->toSql($ID_user, "Number") . " AND type = " . $db->toSql($scope, "Text");
        $db->execute($sSQL);
    }

    return array("result" => $result, "user" => array("ID" => $ID_user));
}

function getSid($username, $scope, $model = null)
{
    $sid = "";
    $db = ffDB_Sql::factory();
    $sSQL = "SELECT access_sid.token
                FROM access_sid 
                  INNER JOIN access_users ON access_users.ID = access_sid.ID_user
                WHERE access_users.email = " . $db->toSql($username, "Text") . " 
                    AND access_users.SID_expire > " . $db->toSql(time(), "Number") . " 
                    AND access_sid.type = " . $db->toSql($scope, "Text");
    $db->query($sSQL);
    if ($db->nextRecord()) {
        $sid = $db->getField("token", "Text", true);

    }

    return $sid;
}

function setSid($username, $sid, $code, $scope, $model = null)
{
    $ID_user = 0;
    $db = ffDB_Sql::factory();
    $sSQL = "SELECT ID
                FROM access_users 
                WHERE access_users.email = " . $db->toSql($username, "Text");
    $db->query($sSQL);
    if ($db->nextRecord()) {
        $ID_user = $db->getField("ID", "Number", true);

        $sSQL = "DELETE FROM access_sid WHERE ID_user = " . $db->toSql($ID_user, "Number") . " AND type = " . $db->toSql($scope, "Text");
        $db->execute($sSQL);

        $sSQL = "UPDATE access_users SET
                        access_users.SID_expire = " . $db->toSql(time() + (60 * 60), "Number") . " 
                        , access_users.SID = " . $db->toSql($sid, "Text") . "
                    WHERE ID = " . $db->toSql($ID_user, "Number");
        $db->execute($sSQL);

        $sSQL = "INSERT INTO access_sid 
                    (
                        ID
                        , ID_user
                        , type
                        , token
                        , expire
                    ) VALUES
                    (
                        null
                        , " . $db->toSql($ID_user, "Number") . " 
                        , " . $db->toSql($scope, "Text") . "
                        , " . $db->toSql($code, "Text") . "
                        , " . $db->toSql(time() + (60 * 60), "Number") . "
                    )";
        $db->execute($sSQL);
    }
    return array("result" => "success", "message" => "");
}

function getAttachIcon($attach_name, $attach_extension)
{
    switch ($attach_extension) {
        case "zip":
            $icon = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px"
                             height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve" style="margin-left: -2px;">
                        <g>
                            <g>
                                <polygon fill="#fff" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                                        21.5,23.499 2.5,23.499 2.5,0.499 15.5,0.499 21.5,6.499 			"/>
                                   
                                    <line fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="12.5" y1="18.5" x2="12.5" y2="13.5"/>
                                <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M15.5,18.499V13.5
                                    h1.25c0.69,0,1.25,0.56,1.25,1.25S17.44,16,16.75,16H15.5"/>
                                <polyline fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="7,13.5 
                                    9.5,13.5 7,18.5 9.5,18.5 		"/>
                                <g>
                                     <polyline fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                                        15.5,0.499 15.5,6.499 21.5,6.499 			"/>
                                </g>
                            </g>
                        </g>
                    </svg>';
            break;
        case "pdf":
            $icon = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px"
                             height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve" style="margin-left: -2px;">
                        <g>
                            <polyline fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="15.5,0.5 
                                15.5,6.5 21.5,6.5 	"/>
                            <g>
                                <polygon fill="#fff" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="21.5,23.5 
                                    2.5,23.5 2.5,0.5 15.5,0.5 21.5,6.5 		"/>
                                <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M11.5,18.5v-5h1
                                    c1.152,0,2,1.068,2,2.5s-0.848,2.5-2,2.5H11.5z"/>
                                <polyline fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                                    16.5,18.5 16.5,13.5 19,13.5 		"/>
                                
                                    <line fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="16.5" y1="15.5" x2="18" y2="15.5"/>
                                <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M6.5,18.499V13.5
                                    h1.25C8.44,13.5,9,14.059,9,14.75C9,15.439,8.44,16,7.75,16H6.5"/>
                            </g>
                        </g>
                    </svg>';
            break;
        case "rar":
            $icon = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px"
                             height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                        <g>
                            <g>
                                <polygon fill="#fff" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                                    21.5,23.499 2.5,23.499 2.5,0.499 15.5,0.499 21.5,6.499 		"/>
                                <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M5.5,18.499V13.5
                                    h1.25C7.44,13.5,8,14.06,8,14.75S7.44,16,6.75,16H5.5"/>
                                <polyline fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                                    13.5,18.5 12,13.5 10.5,18.5 		"/>
                                
                                    <line fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="10.8" y1="17.5" x2="13.2" y2="17.5"/>
                                
                                    <line fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="6.741" y1="16" x2="8" y2="18.5"/>
                                <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M15.5,18.499V13.5
                                    h1.25c0.69,0,1.25,0.56,1.25,1.25S17.44,16,16.75,16H15.5"/>
                                
                                    <line fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" x1="16.741" y1="16" x2="18" y2="18.5"/>
                                <polyline fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                                    15.5,0.499 15.5,6.499 21.5,6.499 		"/>
                            </g>
                        </g>
                    </svg>';
            break;
        case "doc":
        case "docx":
            $icon = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px"
                             height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                        <g>
                            <g>
                                <g>
                                    <polygon fill="#fff" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                                        21.5,23.5 2.5,23.5 2.5,0.5 15.5,0.5 21.5,6.5 			"/>
                                    <polyline fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                                        15.5,0.5 15.5,6.5 21.5,6.5 			"/>
                                </g>
                                <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M18.207,14.738
                                    c-0.16-0.743-0.521-1.239-1.297-1.239c-1.201,0-1.41,1.036-1.41,2.501c0,1.464,0.209,2.5,1.41,2.5
                                    c0.775,0,1.137-0.496,1.297-1.241"/>
                                <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M13.5,16
                                    c0,1.462-0.252,2.5-1.5,2.5c-1.249,0-1.5-1.038-1.5-2.5c0-1.465,0.251-2.5,1.5-2.5C13.248,13.5,13.5,14.535,13.5,16z"/>
                                <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M5.5,18.5v-5h1
                                    c1.152,0,2,1.068,2,2.5s-0.848,2.5-2,2.5H5.5z"/>
                            </g>
                        </g>
                    </svg>';
            break;
        case "jpg":
        case "jpeg":
            $icon = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px"
                             height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                        <g>
                            <g>
                                <g>
                                    <g>
                                        <g>
                                            <g>
                                                <polygon fill="#fff" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                                                    19.5,23.5 0.5,23.5 0.5,0.5 13.5,0.5 19.5,6.5 						"/>
                                                <polyline fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                                                    13.5,0.5 13.5,6.5 19.5,6.5 						"/>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                                <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M16.359,14.5
                                    c-0.168-0.746-0.547-1-1.359-1c-1.258,0-1.5,1.035-1.5,2.5c0,1.462,0.242,2.5,1.5,2.5c1.048,0,1.42-0.878,1.5-2H15"/>
                                <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M9.5,18.499V13.5
                                    h1.25c0.69,0,1.25,0.56,1.25,1.25S11.44,16,10.75,16H9.5"/>
                                <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M3.5,17
                                    c0,0.829,0.672,1.5,1.5,1.5s1.5-0.671,1.5-1.5v-3.5"/>
                            </g>
                        </g>
                    </svg>';
            break;
        case "png":
            $icon = '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="24px"
                             height="24px" viewBox="0 0 24 24" enable-background="new 0 0 24 24" xml:space="preserve">
                        <g>
                            <g>
                                <g>
                                    <g>
                                        <g>
                                            <g>
                                                <polygon fill="#fff" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                                                    19.5,23.5 0.5,23.5 0.5,0.5 13.5,0.5 19.5,6.5 						"/>
                                                <polyline fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="
                                                    13.5,0.5 13.5,6.5 19.5,6.5 						"/>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                                <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M3.5,18.5v-5h1.25
                                    C5.44,13.5,6,14.06,6,14.75S5.44,16,4.75,16H3.5"/>
                                <polyline fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" points="8.5,18.5 
                                    8.5,13.5 11.5,18.5 11.5,13.5 		"/>
                                <path fill="none" stroke="#000000" stroke-linecap="round" stroke-linejoin="round" stroke-miterlimit="10" d="M16.359,14.5
                                    c-0.168-0.746-0.547-1-1.359-1c-1.258,0-1.5,1.035-1.5,2.5c0,1.462,0.242,2.5,1.5,2.5c1.048,0,1.42-0.878,1.5-2H15"/>
                            </g>
                        </g>
                    </svg>';
            break;
        default:
            $icon = "";
    }

    return $icon;
}

function strposa($haystack, $needles = array(), $offset = 0)
{
    $chr = array();
    foreach ($needles as $needle) {
        $res = strpos($haystack, $needle, $offset);
        if ($res !== false) {
            $chr[$needle] = $res;
        }
    }
    if (empty($chr)) {
        return false;
    } else {
        return min($chr);
    }
}

function sendSMSTwilio($numer, $text)
{
    require_once(FF_DISK_PATH . "/library/twilio/Twilio/autoload.php");
    try {
        $client = new Twilio\Rest\Client(MOD_SHARE_TWILIO_SID, MOD_SHARE_TWILIO_TOKEN);
        $client->messages->create(
            $numer,
            array(
                'from' => 'PM it',
                'body' => $text
            )
        );
    } catch (Exception $e) {

    }
}

function saveRemoteImage($url_file, $nome_file, $relative_path)
{
    set_time_limit(300);
    //Percorso file remoto
    $remotefile = pathinfo($url_file);
    if (!strlen($remotefile["extension"])) {
        $remotefile["extension"] = "png";
    }
    //Cartella locale in cui copiare il file
    $cartella = FF_DISK_PATH . "/uploads" . $relative_path; // cartella dove mettere immagini

    if (!is_dir($cartella)) {
        mkdir($cartella, 0777, true);
    }

    $file = file($url_file);
    file_put_contents($cartella . $nome_file . "." . $remotefile["extension"], $file);

    return ($relative_path . $nome_file . "." . $remotefile["extension"]);
}
